(function($) {
    //=== Добавляем типы анимаций ===
    $.fn.addTouch = function() {
        return this.each(function(i, el) {
            $(el).bind('touchstart touchmove touchend touchcancel',function() {
                var touches = event['changedTouches'],
                    first = touches[0],
                    type = '';
                switch(event.type) {
                    case 'touchstart':
                        type = 'mousedown';
                        break;
                    case 'touchmove':
                        type = 'mousemove';
                        event.preventDefault();
                        break;
                    case 'touchend':
                        type = 'mouseup';
                        break;
                    case 'touchcancel':
                        type = 'mouseup';
                        break;
                    default:
                        return;
                }
                var simulatedEvent = document.createEvent('MouseEvent');
                simulatedEvent.initMouseEvent(type, true, true, window, 1, first.screenX, first.screenY, first.clientX, first.clientY, false, false, false, false, 0/*left*/, null);
                first.target.dispatchEvent(simulatedEvent);
            });
        });
    };


    //=================================== Пошёл мой код ==========================================

    var selected_page_index = 0,    //--- Хранит страницу, которая сейчас показывается ----
        pages = [],                 //--- Храним объекты всех страниц ---
        animated = false,
        paging_speed = 400;         //--- Время анимации переключени страниц ---

    var img_pot_page = ['images/solid-circle.png', 'images/empty-circle.png'];

	var onePage = function(section, index) {

        //--- Определение переменных, чтобы не высчитывать их в дальнейшем

        var no_fixed =      section.is('[data-no-fixed]'),              //--- Проверяем, страница будет подниматься или будет лежать под предыдущей страницей ---
		    bi =            $('.bi:first', section),                    //--- Фоновое изображение
            wbi =           bi.width(),
            hbi =           bi.height(),
            pbi =           bi.parent(),                                //--- Блок, в котором находится фоновое изображение ---
            one_track =     !(index && index != sections.length - 1),   //--- Находится в центре всего контента или скраю ----
            py =            bi.attr('data-parallaks') || 0,             //--- Присутствие эффекта параллакса ---
		    nfc =           $('.content-section-text:first', section).css({'top': '100%'}),  //--- Контент, который двигается за предыдущим блоком или следующим ---
            for_zoom =      $('[data-zoom]', section),            //--- Элементы, масштабируемые при изменении размера экрана ---
            zoom_params =   for_zoom.length ? for_zoom.attr('data-zoom').split('|') : false;
        var section_height = section.height();
        var this_list = false, this_item, thobj = this;

        if (section.is('[data-item]')) {
            var n = section.attr('data-item'), new_list, buttons_container;
            if (!lists_items[n]  || false) {
                new_list = $('<div>').addClass('list-items-pages').appendTo($('#page-content'));
                buttons_container = $('<div>').addClass('cont-items-pages').appendTo(new_list);
            }

            lists_items[n] = lists_items[n] ? lists_items[n] : {'listjq': buttons_container, 'items': [], 'listbl': new_list};
            this_list = lists_items[n];
            this_item = $('<img>').attr('src', img_pot_page[!lists_items[n]['items'].length ? 0 : 1]).appendTo(this_list['listjq']).text(this_list.items.length).mousedown(function() {
                !animated ? thobj.showSection() : false;
            }).addTouch();
            lists_items[n]['items'].push(this_item);
        }

        var position;

        var endMovePage = function(e) {
            var touch = (e.originalEvent.touches ? e.originalEvent.touches[0] : false) || (e.originalEvent.changedTouches ? e.originalEvent.changedTouches[0] : false) || e;
            var range_y = touch.clientY - position;
            var new_index = index - Math.abs(range_y) / range_y;
            if (new_index != index) {
                if (range_y && Math.abs(range_y) > 10 && (pages[new_index] || false)) {
                    pages[new_index].showSection();
                } else {
                    showed = Math.abs(range_y);
                    if (pages[new_index]) {
                        selected_page_index = new_index;
                        thobj.showSection();
                    }
                }
                prev_preopen = false;
            }
            section.on('touchstart mousedown', saveStartParams);
            $(window).off('touchmove mousemove', MovePage).off('touchend touchcancel mouseup', endMovePage);
            e.stopPropagation();
            e.preventDefault();
        };

        var prev_preopen = false;

        var MovePage = function(e) {
            var touch = (e.originalEvent.touches ? e.originalEvent.touches[0] : false) || (e.originalEvent.changedTouches ? e.originalEvent.changedTouches[0] : false) || e;
            var range_y = touch.clientY - position;
            if (Math.abs(range_y) || showed) {
                var ind = index - Math.abs(range_y) / range_y;
                var new_page = pages[ind] || false;
                if (new_page) {
                    range_y = Math.abs(range_y) > section_height ? section_height * Math.abs(range_y)/range_y : range_y;
                    new_page.showSection(range_y);
                    thobj.hideSection(range_y);
                    if (prev_preopen !== false && prev_preopen !== ind) {
                        pages[prev_preopen].hideSection(range_y > 0 ? -section_height : section_height);
                        pages[prev_preopen].hideSection();
                    }
                    prev_preopen = ind;
                }
            }
            e.stopPropagation();
            e.preventDefault();
        };

        var saveStartParams = function(e) {
            position = ((e.originalEvent.touches ? e.originalEvent.touches[0] : false) || (e.originalEvent.changedTouches ? e.originalEvent.changedTouches[0] : false) || e).clientY;
            section.off('touchstart mousedown', saveStartParams);
            $(window).on('touchmove mousemove', MovePage).on('touchend touchcancel mouseup', endMovePage);
        };

        section.on('touchstart mousedown', saveStartParams);
        no_fixed ? section.css({top: '100%'}) : false;
        zoom_params = zoom_params ? {'kzoom': zoom_params[0] / zoom_params[1], 'max-width': zoom_params[0], 'max-height': zoom_params[1]} : false;

		var iniZoomed = function() {
            //=== Масштабируем фоновое изображение ===
            if (bi.length) {
                bi.css({'width': '', 'height': '', 'margin-left': '', 'margin-top': ''});   //--- Сбрасываем изображение ---
                var prl = !one_track ? Math.abs(py) : 0,
                    n_height = pbi.height() * (1 + prl / 100),
                    kw = pbi.width() / wbi;
                if (hbi * kw > n_height) {
                    bi.width('100%').css({'margin-top': - (kw * hbi - n_height) / 2 + 'px'});
                } else {
                    var percents = n_height / hbi / kw * 100;
                    bi.width(percents + '%').css({'margin-left': -(percents - 100) / 2 + '%'});
                }
            }

            //=== Масштабирвание элементов, если таковы имеются

            if (zoom_params) {
                for_zoom.css({'width': '', 'height': '', 'font-size': ''});
                var w = for_zoom.width(), h = for_zoom.height();
                if (w != zoom_params['max-width'] || h != zoom_params['max-height']) {
                    zoom_params['max-width'] / w > zoom_params['max-height'] / h ? for_zoom.height(w / zoom_params['kzoom']) : for_zoom.width(w = h * zoom_params['kzoom']);
                    for_zoom.css({'font-size': w / zoom_params['max-width'] * 100 + '%'});
                }
            }
            section_height = section.height();
		};


		this.reiniZoom = iniZoomed;
		selected_page_index == index ? iniZoomed() : false;

        this.getListItems = function() {
            return this_list ? this_list['listbl'] : false;
        };

        var showed = 0;

        var startMoveSlides = function(funcs) {
            for (var k = 0; k < funcs.length; k++) {
                funcs[k]();
            }
            iniZoomed();
        };

        var moveNewSlides = function(top) {      /* -100 <= top <= 100 */

        };

        var endShow = function() {
            showed = 0;
            animated = false;
            section.css({'z-index': ''});
        };


        //--- Показываем секцию страницы ---
		this.showSection = function(go_to) {
            go_to = go_to || false;
            if ((selected_page_index == index) || animated) return;
            var new_start_funcs = [],
                old_page = pages[selected_page_index],
                down = index > selected_page_index,
                ljq = this_list ? this_list['listbl'] : false;
            //========================================================================================================//
            //--- Добавляем функции для инициализации начала показа другого слайда ---
            if (!showed) {
                if (py) {
                    new_start_funcs.push(function() {
                        bi.css({top: (!one_track ? (down ? (py > 0 ? 0 : py) : (py < 0 ? 0 : -py)) : (down ? py : -py)) + '%'})
                    });
                }
                if (ljq && !ljq.is(old_page.getListItems())) {
                    new_start_funcs.push(function() {
                        ljq.appendTo(pageContent).css({'top': (down ? '' : '-') + '100%'});
                    });
                }
                new_start_funcs.push(function() {
                    section.css({'z-index': !down ? 2 : 0, top: (!down ? -100 : (!no_fixed ? 0 : 100)) +'%'}).appendTo(pageContent);
                    nfc.css({top: down ? '100%' : '0%'});
                });
                //--- Запуск инициализации ---
                startMoveSlides(new_start_funcs);
            }
            //--- / Добавляем функции для инициализации начала показа другого слайда ---
            //========================================================================================================//


            //--- Включаем анимацию, если надо переключить страницу ---
            if (!go_to) {
                if (this_list) {
                    for (var k = 0; k < this_list['items'].length; k++) {
                        var it = this_list['items'][k];
                        !it.is(this_item) ?  it.attr('src', img_pot_page[1]) : it.attr('src', img_pot_page[0])
                    }
                }
                animated = true;
                var sp = paging_speed * ((section_height - showed) / section_height);
                
                py ? bi.animate({'top': (!one_track ? -Math.abs(py / 2) : 0) + '%'}, sp) : false;
                ljq && !ljq.is(old_page.getListItems()) ? ljq.animate({'top': 0}, sp) : false;
                !down || no_fixed || (showed) ? section.animate({top: 0}, sp) : false;
                down ? nfc.animate({'top': 0}, sp) : false;
                
                selected_page_index = index;
                old_page.hideSection();
                setTimeout(endShow, sp);
            } else if (!animated) {
                showed = Math.abs(go_to);
                var goto = go_to - section_height*go_to/showed;
                py ? bi.css({'top': !one_track ? -showed * py / 200 : goto * py / 100}) : false;
                ljq && !ljq.is(old_page.getListItems()) ? ljq.css({'top': goto}) : false;
                go_to > 0 || no_fixed ? section.css({'top': goto}) : false;
                go_to < 0 ? nfc.css({'top': goto}) : false;
            }
		};




		this.hideSection = function(go_to) {
            go_to = go_to || false;
            var down = index > selected_page_index;
            var list_jq = this_list ? this_list['listbl'] : false;
            var new_page = pages[selected_page_index];

            if (!go_to) {
                var sp = paging_speed * ((section_height - showed) / section_height);
                list_jq && !list_jq.is(new_page.getListItems()) ?
                    list_jq.animate({'top': (down ? '' : '-') + '100%'}, sp, function() {
                        list_jq.detach();                        
                    }) : false;
                py ? bi.animate(py > 0 ?
                {'top': (down ? 0 : -py) + '%' } :
                {'top': (down ? py : (!one_track ? 0 : -py)) + '%' }, sp):
                    false;
                down ? nfc.animate({'top': '100%'}, sp) : false;
                no_fixed || !down ? section.animate({'top': (down ? '' : '-') + '100%'}, sp) : false;
                setTimeout(function() {
                    section.detach();
                    showed = 0;
                }, sp);
            } else if (!animated) {
                showed = Math.abs(go_to);
                var goto = go_to - section_height*go_to/showed;
                list_jq && !list_jq.is(pages[index-showed/go_to].getListItems()) ? list_jq.css({'top': go_to}) : false;
                py ? bi.css({'top': !one_track ? (go_to - section_height)*py/200 : go_to*py/100}) : false;
                goto > 0 || no_fixed ? section.css({'top': go_to}) : false;
                goto < 0 ? nfc.css({'top': go_to}) : false;
            }
		};
	};
	
	var page = $('#page');
	var pageContent = $('#page-content');
	var sch = 0;

    $('img').attr('draggable', 'false').get(0);

    var sections = $('.section-page');
    var lists_items = {};
    var endPreLoad = function() {
        sections.each(function (i) {
            var th = $(this);
            var aname = $('a[name]', th);
            pages.push(new onePage(th, i));
            i ? th.detach() : false;
            aname ? $('a[href^=#' + aname.attr('name') + ']').click(function (e) {
                e.preventDefault();
                pages[i].showSection();
                return false;
            }) : false;
        });


        //--- Инициализация кнопочек, ссылочек и т.д. для плавного изменения вида ---


        $('#to-next-page').mousedown(function () {
            var new_index = selected_page_index + 1;
            pages[new_index].showSection();
        }).addTouch();
        $(window).on('resize orientationchange', function () {
            pages[selected_page_index].reiniZoom();
        });


        //--- Добавляем обработчик при прокрутке в window ---

        var is_scroll = 0,                                          //--- Счётчик прокруток мышью ---
            scroll_start = 1;                                          //--- Предел, при котором происходит переключение страницы ---

        var ScrollSections = function (e) {
            e.preventDefault ? e.preventDefault() : false;
            if (animated) return;
            is_scroll += e.deltaY;
            if (scroll_start <= Math.abs(is_scroll)) {
                is_scroll = 0;
                var pl = pages.length - 1;
                var ni = selected_page_index - e.deltaY / Math.abs(e.deltaY);
                pages[ni > pl ? pl : (ni < 0 ? 0 : ni)].showSection();
            }
        };

        $(window).bind('mousewheel', ScrollSections).bind('keydown', function (e) {
            var new_index = false;
            if (animated) return;
            switch (e.which) {
                case 40:    // down
                    new_index = selected_page_index < pages.length - 1 ? (selected_page_index + 1) : false;
                    break;
                case 38:    // up
                    new_index = selected_page_index > 0 ? (selected_page_index - 1) : false;
                    break;
                case 35:    // end
                    new_index = pages.length - 1;
                    break;
                case 36:    // home
                    new_index = 0;
                    break;
            }
            new_index !== false ? pages[new_index].showSection() : false;
        });
        loading_element.fadeOut('slow', function() {
            loading_element.empty().remove();
        });
    };
    var imgs = $('img').each(function() {
        var im = $(this).attr('draggable', 'false').get(0);
        var img = new Image();
        $(img).load(function() {
            im.src = img.src;
            sch++;
            sch == imgs.length ? endPreLoad() : false;
        });
        img.src = im.src;
        im.src = '';
    });
    var loading_element = $('#loading-page').appendTo($('body:first'));
	$(function() {
		var body = $('body:first');
	})
})(jQuery)
