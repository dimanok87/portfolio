(function($) {
	$.fn.background = function(options) {
		options = options || {};
		var win = $(window);
		return $(this).each(function() {
			var _this = $(this);
			var iniBg = function() {
				var parent = _this.parent(),
					winWidth = parent.width(), 
					winHeight = parent.height();
				_this.css({
					width: 'auto',
					height: 'auto'
				});
		    	var widthK = winWidth / _this.width(),
        			heightK = winHeight / _this.height();
				if ((widthK > 1 && widthK < heightK) || (widthK < heightK)) {
					if (options["resize"]) {
						_this.css({
							height: '100%'
						});
					}
					_this.css({
						top: 0,
						left: -(_this.width() - winWidth) / 2
					});
				} else {
					if (options["resize"]) {
						_this.css({
							width: '100%'
						});
					}
					_this.css({
						left: 0,
						top: (_this.height() - winHeight) / 2
					});
				}
			};
			win.resize(iniBg);
			_this.width() ? iniBg() : _this.load(iniBg);
		});
	};
	
	$.fn.sliderMove = function(options) {
		return $(this).each(function() {
			var slider = $(this);
			var items = $("[data-item-slider]", slider);
			var activeItem = false;
			var showedDesc = false;
			var linzaSize = 0;
			var zoomed = false;
			var duration = 600;
			var zoomedPosition = [0, 0];
			items.each(function() {
				var item = $(this);
				var itemID = item.attr("data-item-slider");
				var descriptionItem = $('[data-description="' + itemID + '"]:first');
				item.click(function(e) {
					e.preventDefault();					
					if (!activeItem) {
						options.linza.show();
						linzaSize = [options.linza.width(), options.linza.height()];						
					}
					var top = options["items"][itemID]["background-center"][1] - linzaSize[1]/2;
					var left = options["items"][itemID]["background-center"][0] - options.background.width() / 2 - linzaSize[0]/2;
					top = Math.min(options.background.height() - options.padding*2 - linzaSize[1], Math.max(options.padding, top));
					options.linza.stop()[!activeItem ? "css" : "animate"]({
						marginTop: top,
						marginLeft: left
					}, duration);

					var leftBG = -(options["items"][itemID]["zoom-center"][0] - linzaSize[0]/2);
					var topBG = -(options["items"][itemID]["zoom-center"][1] - linzaSize[1]/2);
					
					if (!activeItem) {
						options.linza.css(
							"background-position", 
							leftBG + "px " + 
							topBG + "px"
						);
					} else {
						zoomed ? zoomed.stop() : false;
						zoomed = $({
								left: zoomedPosition[0],
								top: zoomedPosition[1]
							}).animate({
								left: leftBG,
								top: topBG
							}, {
								duration: duration,
								step: function() {
									zoomedPosition = [this.left, this.top];
									options.linza.css({
										backgroundPosition: this.left + "px " + this.top + "px"
									});
								}
							}, function() {
								zoomed = false;
							}
						);
					}
					/*
					zoomed = $({
						top: "",
						left: ""
					}).animate({
						top: toBlock.offset()["top"] - header.outerHeight(true)
					}, {
						duration: duration,
						step: function() {
							win.scrollTop(this.top);
       				    }
       				}, function() {
						zoomed = false;
					});
					*/
					
					
					showedDesc ? showedDesc.hide() : false;
					showedDesc = descriptionItem.show();
					activeItem ? activeItem.removeClass("active") : false;
					activeItem = item.addClass("active");
				});
			});
		});
	};
})(jQuery);


