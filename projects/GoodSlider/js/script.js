(function($) {
	$(function() {
		$(".slider-background").background();
		$(".slider-menu").sliderMove({			
			background: $("#background-slider"),
			linza: $("#linza"),
			padding: 8,
			items: {
				item1: {
					"background-center": [1160, 70],
					"zoom-center": [890, 250]
				},
				item3: {
					"background-center": [1090, 135],
					"zoom-center": [698, 392]
				},
				item2: {
					"background-center": [985, 180],
					"zoom-center": [450, 528]
				},
				item4: {
					"background-center": [1030, 275],
					"zoom-center": [580, 754]
				},
				item5: {
					"background-center": [1230, 320],
					"zoom-center": [970, 860]
				}
			}
		});
	})
})(jQuery);
