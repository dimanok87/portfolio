(function($) {
    $.fn.addTouch = function() {
        return this.each(function(i, el) {
            $(el).bind('touchstart touchmove touchend touchcancel',function() {
                var touches = event['changedTouches'],
                    first = touches[0],
                    type = '';
                switch(event.type) {
                    case 'touchstart':
                        type = 'mousedown';
                        break;
                    case 'touchmove':
                        type = 'mousemove';
                        event.preventDefault();
                        break;
                    case 'touchend':
                        type = 'mouseup';
                        break;
                    case 'touchcancel':
                        type = 'mouseup';
                        break;
                    default:
                        return;
                }
                var simulatedEvent = document.createEvent('MouseEvent');
                simulatedEvent.initMouseEvent(type, true, true, window, 1, first.screenX, first.screenY, first.clientX, first.clientY, false, false, false, false, 0/*left*/, null);
                first.target.dispatchEvent(simulatedEvent);
            });
        });
    };
    $.fn.slideMenu = function(options) {
        options = options || {};

        var showDelay = (options["showDelay"] || 0.5) * 1000;
        var hideDelay = (options["hideDelay"] || 0.5) * 1000;
        var showDuration = (options["showDuration"] || 0.5) * 1000;
        var hideDuration = (options["hideDuration"] || 0.5) * 1000;
        var attrNameSubMenu = options["attrId"] || "rel";
        var scrollerLine = $("<div>").addClass("scroller-line");
        var scrollerLineContainer = $("<div>").addClass("scroller-line-container");
        var scrollerRanger = $("<div>").addClass("scroller-ranger");
        var mask = $("<div>").css({
            position: "absolute",
            zIndex: 1000,
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
        });

        return $(this).each(function() {
            var _this = $(this);
            if (_this.data("slideMenu") || false) return;
            _this.data("slideMenu", true);
            var iniMenu = function() {
                var lnk = $(this);
                var parentBlock = options["parentBlock"] ? lnk.parents(options["parentBlock"]).first() : lnk;
                var submenu = $("#" + lnk.attr(attrNameSubMenu)).appendTo(parentBlock);
                var subMenuList = options["subMenuList"] ? $(options["subMenuList"], submenu) : submenu;
                var showTimeOut = false;
                var hideTimeOut = false;
                var slideUp = false;
                var slideDown = false;
                var opened = false;
                var heightInitialized = false;
                var subItems = options["subMenuItem"] ? $(options["subMenuItem"], submenu) : false;
                var subItemsLength = subItems ? subItems.length : false;
                var thScrollerLine = false;
                var thScrollerRanger = false;
                if (options['maxItemsShowed'] && subItemsLength > options['maxItemsShowed']) {
                    scrollerLine.clone().prependTo(submenu).append(
                        thScrollerLine = scrollerLineContainer.clone().append(
                            thScrollerRanger = scrollerRanger.clone()
                        )
                    );
                }
                
                
                var inDisplay = function() {
                    submenu.css({"left": 0});
                    var offsetSubMenu = submenu.offset()["left"];
                    var scrollLeft = $(window).scrollLeft();
                    var rightBorderPosition = offsetSubMenu + submenu.outerWidth();
                    var rangeRight = rightBorderPosition - $(window).width() - scrollLeft;
                    if (rangeRight > 0) {
                        submenu.css("left", -rangeRight);
                        return;
                    }
                    var rangeLeft = scrollLeft - offsetSubMenu;
                    if (rangeLeft > 0) {
                        submenu.css("left", rangeLeft);
                    }
                };
                var isTouched = true;
                var iniHeight = function() {
                    var height = 0;
                    var fullHeight = 0;
                    subItems.each(function(i) {
                        var _th = $(this);
                        var thHeight = _th.outerHeight(true);
                        if (i < options['maxItemsShowed']) {
                            height+= thHeight;
                        }
                        fullHeight+= thHeight;
                    });
                    subMenuList.height(height);
                    var wheeled = 0;
                    var percentsHeight = height / fullHeight * 100;
                    subMenuList.bind("mousewheel DOMMouseScroll", function(e) {
                        e.preventDefault();
                        var event = e.originalEvent;
                        var deltaY = event.deltaY || (event.wheelDelta ? -event.wheelDelta : (event.detail * 3));
                        var percentWheel = deltaY / fullHeight * 100;
                        wheeled = Math.min(Math.max(0, wheeled + percentWheel), 100 - percentsHeight);
                        thScrollerRanger.css("margin-top", thScrollerLine.height() / 100 * wheeled);
                        subItems.first().css("margin-top", - fullHeight / 100 * wheeled );
                        return false;
                    });
                    var startClientY = 0;
                    var downRanger = function(e) {
                        startClientY = e.clientY;
                        hideTimeOut = true;
                        $(window).bind({
                            mousemove: moveRanger,
                            mouseup: endMove
                        });
                        return false;
                    };
                    var moveRanger = function(e, isTouch) {
                        isTouch = isTouch || false;
                        mask.is(":hidden") ? $("body").append(mask) : false;
                        var rangeMove = e.clientY - startClientY;
                        if (isTouch) {
                            rangeMove = -rangeMove;
                        }
                        startClientY = e.clientY;
                        var percentWheel = rangeMove / height * 100;
                        wheeled = Math.min(Math.max(0, wheeled + percentWheel), 100 - percentsHeight);

                        thScrollerRanger.css("margin-top", thScrollerLine.height() / 100 * wheeled);
                        subItems.first().css("margin-top", - fullHeight / 100 * wheeled);
                    };
                    var endMove = function() {
                        $(window).unbind({
                            mousemove: moveRanger,
                            mouseup: endMove
                        });
                        hideTimeOut = false;
                        mask.detach();
                    };
                    thScrollerRanger.bind({
                        "mousedown": downRanger
                    }).addTouch();

                    var touchMove = function(e) {
                        e.preventDefault();
                        moveRanger(e, true);
                    };
                    var cancelMove = function(e) {
                        $(window).unbind({
                            mousemove: touchMove,
                            mouseup: cancelMove
                        });
                        hideTimeOut = false;
                        mask.detach();
                    };

                    var startTouchMenu = function(e) {
                        if (!isTouched) return;
                        hoverTimeout ? clearTimeout(hoverTimeout) : false;
                        hoverTimeout = false;
                        e.preventDefault();
                        startClientY = e.clientY;
                        hideTimeOut = true;
                        $(window).bind({
                            mouseup: cancelMove,
                            mousemove: touchMove
                        });
                    };
                    var hoverTimeout = false;
                    subMenuList.bind({
                        mousedown: startTouchMenu,
                        mouseenter: function() {
                            hoverTimeout = setTimeout(function() {
                                isTouched = false;
                            }, 10)
                        }
                    }).addTouch();
                    thScrollerRanger.height(percentsHeight + "%");
                    heightInitialized = true;
                };

                var showMenu = function() {
                    showTimeOut = false;
                    submenu.show().css("visibility", "hidden");
                    inDisplay();
                    !heightInitialized && options['maxItemsShowed'] && subItemsLength > options['maxItemsShowed'] ? iniHeight() : false;
                    submenu.hide().css("visibility", "");
                    opened = slideDown = true;
                    submenu.slideDown(showDuration, function() {
                        $(window).bind("mousedown touchstart", windowTapHide);
                        slideDown = false;
                        submenu.height(subMenuList.outerHeight(true));
                    });
                };

                var touch = false;
                var windowTapHide = function(e) {
                    if (!parentBlock.find(e.target).length && !parentBlock.is(e.target)) {
                        if (showTimeOut) {
                            clearTimeout(showTimeOut);
                            showTimeOut = false;
                            slideDown = false;
                        }
                        if (hideTimeOut) return;
                        isTouched = true;
                        opened = false;
                        hideTimeOut = false;
                        slideUp = true;
                        submenu.slideUp(hideDuration, function() {
                            slideUp = false;
                        });
                    }
                };
                //$(window).addTouch();
                parentBlock.bind({
                    mouseenter: function() {
                        if (hideTimeOut) {
                            clearTimeout(hideTimeOut);
                            hideTimeOut = false;
                            slideUp = false;
                        }
                        if (showTimeOut || slideUp || opened) return;
                        touch = false;
                        showTimeOut = setTimeout(function() {
                            showMenu();
                        }, showDelay);
                    },
                    mouseleave: function() {
                        if (showTimeOut) {
                            clearTimeout(showTimeOut);
                            showTimeOut = false;
                            slideDown = false;
                        }
                        if (hideTimeOut) return;
                        hideTimeOut = setTimeout(function() {
                            $(window).unbind("mousedown touchstart", windowTapHide);
                            isTouched = true;
                            opened = false;
                            hideTimeOut = false;
                            slideUp = true;
                            submenu.slideUp(hideDuration, function() {
                                slideUp = false;
                            });
                        }, hideDelay);
                    },
                    mousedown: function() {
                        if (showTimeOut) {
                            clearTimeout(showTimeOut);
                            showTimeOut = false;
                        }
                        if (hideTimeOut) {
                            clearTimeout(hideTimeOut);
                            hideTimeOut = false;
                            slideUp = false;
                        }
                        if (showTimeOut || slideUp || opened) return;
                        touch = true;
                        showMenu();
                    },
                    click: function() {
                        if (touch) {
                            touch = false;
                            return false;
                        }
                    }
                }).addTouch();
            };
            
            $(options["links"] || "a", _this).each(iniMenu);
        });
    };



    $(function() {
        $("#chromemenu").slideMenu({
            attrId: "rel",
            parentBlock: ".col",
            links: "a",
            showDelay: 0.3,
            hideDelay: 0.3,
            showDuration: 0.3,
            hideDuration: 0.3,
            subMenuList: "ul",
            subMenuItem: "li",
            maxItemsShowed: 10
        });
    })

})(jQuery);
