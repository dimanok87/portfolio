(function($) {
    //--- Кностанты с описанием - дальше скрипт можно даже не смотреть. Если что-то не срастается - спрашивай меня ----

    //-- Класс, который присваивается новой менюшке (общему блоку - строится в скрипте)
    var submenuBlockClass =         'submenu-block';

    //-- Класс, который присваивается контейнеру меню (в нём находится сама менюшка и подгружаемые меню, находится внутри submenuBlockClass, тоже в скрипте)
    var submenuContainerClass =     'submenu-container';

    //-- Атрибут, который отвечает за то, чтобы именно в этот блок добавлялась менюшка (добавляется скриптом)
    var submenuContainerAttr =      'data-subcontainer';

    //---------- То, что выше можно не трогать, если нет конфликтов классов ----------

    //-- Класс для индикатора загрузки меню ----
    var loaderClass =               'loader';

    //-- Селектор главного меню
    var mainMenuSelector =          '#catalog_menu';

    //-- Атрибут, в котором содержится url для подгрузки субменю --
    var ajaxUrlAttr =               'data-submenu';

    //--- Максимальное количество видимых пунктов в загружаемом меню ----
    var maxItems = 10;

    /* В строке 265 содержится информация, как активировать загрузку меню через аякс */


    //---- Это пример, как должно выглядеть меню. Ниже описано ------
    var subMenuExample =
        '<ul>' +
            '<li data-submenu="<url для выпадающего меню этого пункта>">' +
                '<a href="#">Пункт 1</a>' +
            '</li>' +
            '<li data-submenu="<url для выпадающего меню этого пункта>">' +
                '<a href="#">Пункт 2</a>' +
            '</li>' +
            '<li data-submenu="<url для выпадающего меню этого пункта>">' +
                '<a href="#">Пункт 3</a>' +
            '</li>' +
            '<li data-submenu="<url для выпадающего меню этого пункта>">' +
                '<a href="#">Пункт 4</a>' +
            '</li>' +
            '<li data-submenu="<url для выпадающего меню этого пункта>">' +
                '<a href="#">Пункт 5</a>' +
            '</li>' +
            '<li data-submenu="<url для выпадающего меню этого пункта>">' +
                '<a href="#">Пункт 6</a>' +
            '</li>' +
            '<li data-submenu="<url для выпадающего меню этого пункта>">' +
                '<a href="#">Пункт 7</a>' +
            '</li>' +
            '<li data-submenu="<url для выпадающего меню этого пункта>">' +
                '<a href="#">Пункт 8</a>' +
            '</li>' +
            '<li data-submenu="<url для выпадающего меню этого пункта>">' +
                '<a href="#">Пункт 9</a>' +
            '</li>' +
            '<li data-submenu="<url для выпадающего меню этого пункта>">' +
                '<a href="#">Пункт 10</a>' +
            '</li>' +
            '<li data-submenu="<url для выпадающего меню этого пункта>">' +
                '<a href="#">Пункт 11</a>' +
            '</li>' +
            '<li data-submenu="<url для выпадающего меню этого пункта>">' +
                '<a href="#">Пункт 12</a>' +
            '</li>' +
            '<li data-submenu="<url для выпадающего меню этого пункта>">' +
                '<a href="#">Пункт 13</a>' +
            '</li>' +
            '<li data-submenu="<url для выпадающего меню этого пункта>">' +
                '<a href="#">Пункт 14</a>' +
            '</li>' +
            '<li data-submenu="<url для выпадающего меню этого пункта>">' +
                '<a href="#">Пункт 15</a>' +
            '</li>' +
        '</ul>';


    //----- Плагин для инициализации скролла -----
    $.fn.iniScroll = function(options) {
        var height = 0;
        var fullHeight = 0;
        var menu = $(this);
        var subMenuList = $('ul', menu);
        var subItems = $('li', menu);
        var submenu = $('.' + submenuContainerClass + ':first', menu);
        if (options['maxItemsShowed'] >= subItems.length) return;
        var hideTimeOut;
        !options['item'].data('show') ? options['container'].prepend(menu) : false;

        subItems.each(function(i) {
            var _th = $(this);
            var thHeight = _th.outerHeight(true);
            if (i < options['maxItemsShowed']) {
                height+= thHeight;
            }
            fullHeight+= thHeight;
        });
        submenu.height(height);

        !options['item'].data('show') ? menu.detach() : false;

        var scrollerLine = $("<div>").addClass("scroller-line");
        var scrollerLineContainer = $("<div>").addClass("scroller-line-container");
        var scrollerRanger = $("<div>").addClass("scroller-ranger");
        var mask = $("<div>").css({
            position: "absolute",
            zIndex: 1000,
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
        });
        scrollerLine.prependTo(submenu).append(scrollerLineContainer.append(scrollerRanger));
        var wheeled = 0;
        var percentsHeight = height / fullHeight * 100;
        subMenuList.on("mousewheel DOMMouseScroll", function(e) {
            e.preventDefault();
            var event = e.originalEvent;
            var deltaY = event.deltaY || (event.wheelDelta ? -event.wheelDelta : (event.detail * 3));
            var percentWheel = deltaY / fullHeight * 100;
            wheeled = Math.min(Math.max(0, wheeled + percentWheel), 100 - percentsHeight);
            scrollerRanger.css("margin-top", scrollerLineContainer.height() / 100 * wheeled);
            subItems.first().css("margin-top", - fullHeight / 100 * wheeled );
            return false;
        });
        var startClientY = 0;
        var downRanger = function(e) {
            startClientY = e.clientY;
            hideTimeOut = true;
            $(window).bind({
                mousemove: moveRanger,
                mouseup: endMove
            });
            return false;
        };
        var moveRanger = function(e, isTouch) {
            isTouch = isTouch || false;
            mask.is(":hidden") ? $("body").append(mask) : false;
            var rangeMove = e.clientY - startClientY;
            if (isTouch) {
                rangeMove = -rangeMove;
            }
            startClientY = e.clientY;
            var percentWheel = rangeMove / height * 100;
            wheeled = Math.min(Math.max(0, wheeled + percentWheel), 100 - percentsHeight);

            scrollerRanger.css("margin-top", scrollerLineContainer.height() / 100 * wheeled);
            subItems.first().css("margin-top", - fullHeight / 100 * wheeled);
        };
        var endMove = function() {
            $(window).unbind({
                mousemove: moveRanger,
                mouseup: endMove
            });
            hideTimeOut = false;
            mask.detach();
        };
        scrollerRanger.bind({
            "mousedown": downRanger
        }).addTouch();

        var touchMove = function(e) {
            e.preventDefault();
            moveRanger(e, true);
        };
        var cancelMove = function(e) {
            $(window).unbind({
                mousemove: touchMove,
                mouseup: cancelMove
            });
            mask.detach();
        };

        var startTouchMenu = function(e) {
            if (!isTouched) return;
            hoverTimeout ? clearTimeout(hoverTimeout) : false;
            hoverTimeout = false;
            e.preventDefault();
            startClientY = e.clientY;
            $(window).bind({
                mouseup: cancelMove,
                mousemove: touchMove
            });
        };
        var hoverTimeout = false;
        subMenuList.bind({
            mousedown: startTouchMenu,
            mouseenter: function() {
                hoverTimeout = setTimeout(function() {
                    isTouched = false;
                }, 10)
            }
        }).addTouch();
        scrollerRanger.height(percentsHeight + "%");
    };





    $(function() {
        var mainMenu = $(mainMenuSelector);
        var submenuParent = '[' + submenuContainerAttr + ']';

        //---- Вставляем меню т раскрываем его ----
        var showMenu = function(menu, parent) {
            menu.appendTo(parent);
        };

        var onloadMenu = function(item) {
            item.data('menu').iniScroll({
                maxItemsShowed: maxItems,
                container: mainMenu,
                item: item
            });
            iniMenuItems(item.data('menu'));

            item.data('menu').on({
                'mouseenter': function() {
                    item.data('show', true);
                },
                'mouseleave': function() {
                    hideMenu(item);
                }
            });
        };


        //---- Загружаем и показываем меню ----
        var loadMenu = function(item, options) {

            options = options || {};
            var itemUrl = item.attr(ajaxUrlAttr);
            var container = item.parents(submenuParent + ':first');

            var menu = item.data('menu') || false;
            if (item.data('show')) return;

            var showblockMenu = function() {
                item.data('show', true);
                item.addClass('active');
                item.data('showTimeout', setTimeout(function() {
                    item.data('show') ?
                        showMenu(item.data('menu'), container) :
                        false;
                }, options['delayShowed'] || 0));
            };

            var blockMenu, containerMenu;
            if (!menu) {
                blockMenu = $('<div>').addClass(submenuBlockClass);
                containerMenu = $('<div>').addClass(submenuContainerClass).attr(submenuContainerAttr, '');
                blockMenu.append(containerMenu);
                containerMenu.append($('<div>').addClass(loaderClass));
                menu = item.data('menu', blockMenu);
                showblockMenu();
            } else {
                showblockMenu();
                return;
            }

            //---- При добавлении атрибутов data-submenu это убрать (эти строки имитируют аякс) --------
            setTimeout(function(){
                containerMenu.html($(subMenuExample));
                onloadMenu(item, container);
            }, 600);
            //---- /При добавлении атрибутов data-submenu это убрать --------


            //---- При добавлении атрибутов data-submenu это раскомментировать --------
            /*$.ajax({
                url: itemUrl,
                dataType: 'html',
                success: function(html) {
                    onloadMenu(item, $(html), container);
                }
            });*/
            //---- / При добавлении атрибутов data-submenu это раскомментировать --------
        };

        //--- Прячем меню ---
        var hideMenu = function(item) {
            clearTimeout(item.data('showTimeout'));
            item.data('show', false);
            setTimeout(function() {
                if (!item.data('show')) {
                    item.removeClass('active');
                    if (item.data('menu')) {
                        item.data('menu').detach();
                    }
                }
            }, 0);
        };

        //---- Функция инициализирует новые пункты меню для подгрузки submenu ----
        var iniMenuItems = function(menu, options) {
            $('[' + ajaxUrlAttr + ']', menu).on({
                'mouseenter': function(){
                    loadMenu($(this), options);
                },
                'mouseleave': function() {
                    hideMenu($(this));
                }
            });
        };

        //---- Инициализируем главное меню ----
        iniMenuItems(mainMenu, {delayShowed: 300});
    });



})(jQuery);