1) Подключить scrolling.js;
2) Инициализировать плагин скролла;

Для того, чтобы работало на сайте, надо инициализировать скролл после показа блока:

точнее - в script.js строка 300

    $('.ask_sign').click(function() {
        $(this).find('.ask').fadeIn(450);
    });

    var timer;

    $('.ask_sign').mouseenter(function() {
        $.data(this, 'timer', setTimeout($.proxy(function() {
            $(this).find('.ask').fadeIn(450);
        }, this), 500));
    });






Заменить на код:

    var showVariants = function(askBlock) {
        askBlock.fadeIn(450).iniScrollBlock({
            maxHeight: 150,
            scrolledContent: '.ask_wrap'
        });
    };
    $('.ask_sign').click(function() {
        showVariants($(this).find('.ask'));
    });

    var timer;

    $('.ask_sign').mouseenter(function() {
        $.data(this, 'timer', setTimeout($.proxy(function() {
            showVariants($(this).find('.ask'));
        }, this), 500));
    });
