(function($) {
    $.fn.iniScrollBlock = function(options) {

        options = options || {};

        return $(this).each(function() {
            // Базовый блок для скролла ---
            var _this = $(this);

            if (_this.data('scrollInited')) return true;

            /* Базовые элементы блока со кроллом  */

            var scrollerLine =
                $("<div>").addClass("fn-scroller-line");
            var scrollerLineBlock =
                $('<div>').addClass("fn-scroller-line-block").appendTo(scrollerLine);
            var scrollerLineContainer =
                $("<div>").addClass("scroller-line-container").appendTo(scrollerLineBlock);
            var scrollerRanger =
                $("<div>").addClass("scroller-ranger").appendTo(scrollerLineContainer);

            var wheeled = false;

            options = $.extend({
                maxHeight: 200,
                scrolledContent: '[data-scrolled]'
            }, options);

            // Контент базового блока ---
            var scrolled = $(options['scrolledContent'], _this);

            // Relative блок - вставляется в базовый блок

            var relativeBlock = $('<div>').css({
                position: 'relative',
                maxHeight: options['maxHeight'],
                overflow: 'hidden'
            });

            var fullHeight, percentsHeight, isTouched;

            var iniScrollElements = function() {
                _this.bind("mousewheel DOMMouseScroll", function(e) {
                    e.preventDefault();
                    var event = e.originalEvent;
                    var deltaY = event.deltaY || (event.wheelDelta ? -event.wheelDelta : (event.detail * 3));
                    var percentWheel = deltaY / fullHeight * 100;
                    wheeled = Math.min(Math.max(0, wheeled + percentWheel), 100 - percentsHeight);
                    scrollerRanger.css("margin-top", scrollerLine.height() / 100 * wheeled);
                    scrolled.css("margin-top", - fullHeight / 100 * wheeled );
                    return false;
                });


                var startClientY = 0;
                var downRanger = function(e) {
                    e.preventDefault();
                    startClientY = e.clientY;
                    $(window).bind({
                        mousemove: moveRanger,
                        mouseup: endMove
                    });
                    return false;
                };

                var moveRanger = function(e, isTouch) {
                    isTouch = isTouch || false;
                    var rangeMove = e.clientY - startClientY;
                    if (isTouch) {
                        rangeMove = -rangeMove;
                    }
                    startClientY = e.clientY;
                    var percentWheel = rangeMove / options['maxHeight'] * 100;
                    wheeled = Math.min(Math.max(0, wheeled + percentWheel), 100 - percentsHeight);

                    scrollerRanger.css("margin-top", scrollerLine.height() / 100 * wheeled);
                    scrolled.css("margin-top", - fullHeight / 100 * wheeled);
                };
                var endMove = function() {
                    $(window).unbind({
                        mousemove: moveRanger,
                        mouseup: endMove
                    });
                };

                scrollerRanger.bind({
                    "mousedown": downRanger
                }).addTouch();

                var touchMove = function(e) {
                    e.preventDefault();
                    moveRanger(e, true);
                };
                var cancelMove = function() {
                    $(window).unbind({
                        mousemove: touchMove,
                        mouseup: cancelMove
                    });
                };
            };

            var createScroll = function() {
                relativeBlock.append(scrolled, scrollerLine).appendTo(_this);
                fullHeight = scrolled.outerHeight();
                percentsHeight = options['maxHeight'] / fullHeight * 100;
                scrollerRanger.height(percentsHeight + '%');
                iniScrollElements();
            };

            if (scrolled.outerHeight() > options['maxHeight']) {
                createScroll();
            }
            _this.data('scrollInited', true);
        });
    };

    $(function() {
        $('.scrolled-block').iniScrollBlock({
            maxHeight: 150,
            scrolledContent: '.scrolled-content'
        });
    })
})(jQuery);