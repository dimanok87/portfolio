var cellData;
(function($) {
    cellData = function(newData) {
        var data = newData || [];
        this.setData = function(newData) {
            data = newData;
        };
        this.getHTMLData = function() {
            var htmlDataBlock = $('<table>').addClass('html-cell-data').attr({
                cellpadding: 0,
                cellspacing: 0,
                border: 0
            });
            for (var i = 0; i < data.length; i++) {
                var tr = $('<tr>').appendTo(htmlDataBlock),
                    nameTd = $('<td>').
                        addClass('html-cell-name').
                        text(data[i].title).
                        appendTo(tr),
                    valueTd = $('<td>').
                        addClass('html-cell-value').
                        text(data[i].value).
                        appendTo(tr);
                tr.addClass(!(i%2) ? 'even-row' : 'odd-row');
            }
            return htmlDataBlock;
        };
    };
})(jQuery);