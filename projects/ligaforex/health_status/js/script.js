(function($) {
    $.fn.addTouch = function() {
        return this.each(function(i, el) {
            $(el).bind('touchstart touchmove touchend touchcancel',function() {
                var touches = event['changedTouches'],
                    first = touches[0],
                    type = '';
                switch(event.type) {
                    case 'touchstart':
                        type = 'mousedown';
                        break;
                    case 'touchmove':
                        type = 'mousemove';
                        break;
                    case 'touchend':
                        type = 'mouseup';
                        break;
                    case 'touchcancel':
                        type = 'mouseup';
                        break;
                    default:
                        return;
                }
                var simulatedEvent = document.createEvent('MouseEvent');
                simulatedEvent.initMouseEvent(type, true, true, window, 1, first.screenX, first.screenY, first.clientX, first.clientY, false, false, false, false, 0/*left*/, null);
                first.target.dispatchEvent(simulatedEvent);
            });
        });
    };
    $(function() {
        $('#info-table td').each(function() {
            var _this = $(this);
            var link = $('a:first', _this);
            var thisCellData = new cellData(testDataCell);
            if (!link.length) {
                return;
            }
            var oneTooltip = new Tooltip({
                'element': link,
                'title': 'Testing, ms',
                'content': thisCellData.getHTMLData(),
                'position': 'top',
                'animation_duration': 0
            });

            var showTooltip = function() {
                    oneTooltip.show();
                    link.addClass('hover');
                },
                hideTooltip = function() {
                    oneTooltip.hide();
                    link.removeClass('hover');
                };
            link.bind({
                'mouseover': showTooltip,
                'click': function(e) {
                    e.preventDefault();
                }
            });

            $(window).mousemove(function(e) {
                var target = $(e.target);
                if (!(
                    link.is(target) || link.find(target).length
                )) {
                    hideTooltip();
                }
            });
            var unbindMouseHover = function() {
                link.unbind({
                    'mouseover': showTooltip
                }).bind({
                    'click': function(e) {
                        e.preventDefault();
                        showTooltip();
                    }
                });
                $(window).
                    unbind('touchstart', unbindMouseHover).
                    bind('mousedown touchstart', function(e) {
                        var target = $(e.target);
                        if (!link.is(target)) {
                            hideTooltip();
                        }
                    });
            };
            $(window).bind({
                'touchstart': unbindMouseHover
            });
        });

        var tableContainer = $('#servers-table-container'),
            infoTable = $('#info-table').addTouch(),
            positionTable = 0,
            startPositionMove = 0,
            rangeSize = 0,
            newPosition = 0;
        var downTable = function(e) {
            e.preventDefault();
            startPositionMove = e.clientX;
            infoTable.addClass('active-scroll');
            $(window).bind({
                mousemove: moveWindowOfTable,
                mouseup: upWindowOfTable
            });
        };
        var moveWindowOfTable = function(e) {
            e.preventDefault();
            newPosition = positionTable - (e.clientX - startPositionMove);
            newPosition =
                newPosition > 0 ?
                    0 :
                    (newPosition < -rangeSize ? -rangeSize : newPosition);
            infoTable.css({
                right: newPosition
            });
        };
        var upWindowOfTable = function() {
            positionTable = newPosition;
            $(window).unbind({
                mousemove: moveWindowOfTable,
                mouseup: upWindowOfTable
            });
            infoTable.removeClass('active-scroll');
        };
        var iniScroll = function() {
            infoTable.bind({
                mousedown: downTable
            });
        };
        var uniniScroll = function() {
            infoTable.unbind({
                mousedown: downTable
            });
        };

        var windowResize = function() {
            if ((rangeSize = infoTable.outerWidth() - tableContainer.outerWidth()) > 0) {
                infoTable.css({left: '', right: 0}).addClass('scrolled');
                iniScroll();
            } else {
                infoTable.css({left: 0, right: ''}).removeClass('scrolled');
                uniniScroll();
            }
        };
        $(window).bind({
            'resize': windowResize
        });
        windowResize();
    });


    var testDataCell = [
        {
            title: 'Quote latency',
            value: 20
        },
        {
            title: 'Equity check latency',
            value: 200
        },
        {
            title: 'Order open latency',
            value: 150
        },
        {
            title: 'Order close latency',
            value: 150
        },
        {
            title: 'Order list latency',
            value: 20
        }
    ];

})(jQuery);
