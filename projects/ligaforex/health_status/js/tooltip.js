var iniBlueHint;
var unIniBlueHint;
var Tooltip;
(function($) {
    var body;
    $(function() {
        body = $('body');
    });
    Tooltip = function(params) {
        var animationDuration = params['animation_duration'] || 0;
        var position = params['position'] || 'bottom';
        var element = params['element'];
        var title = params['title'] || '';
        var content = params['content'] || '';
        var fullTooltip = $('<div>').addClass('blue-hint').hide(),
            contentTooltip = $('<div>').addClass('content-hint').appendTo(fullTooltip),
            titleTooltip = false,
            textTooltip = $('<div>').addClass('text-hint').
                appendTo(contentTooltip).html(content),
            arrowTooltip = $('<div>').addClass('arrow-hint').appendTo(contentTooltip);
        if (title.length) {
            titleTooltip = $('<div>').addClass('title-hint').prependTo(contentTooltip).html(title);
        }
        fullTooltip.addClass(position ? 'hint-position-' + position : '');
        fullTooltip.appendTo(body);

        if (params['class'] || false) {
            fullTooltip.addClass(params['class']);
        }
        if (params['is_fixed'] || false) {
            fullTooltip.css({
                'position': 'fixed'
            });
        }
        this.getTooltipJQuery = function() {
            return fullTooltip;
        };
        this.setTitle = function(title) {
            if (title && title.length) {
                if (!titleTooltip) {
                    titleTooltip = $('<div>').addClass('title-hint').appendTo(contentTooltip).html(title);
                }
                titleTooltip.html(title);
            } else if (titleTooltip) {
                titleTooltip.empty().remove();
                titleTooltip = false;
            }

        };
        this.setContent = function(content) {
            textTooltip.html(content);
        };
        this.setPosition = function(newPosition) {
            fullTooltip.removeClass(position ? 'hint-position-' + position : '');
            position = newPosition || 'bottom';
            fullTooltip.addClass(position ? 'hint-position-' + position : '');
        };
        var isVisible = false;
        this.show = function(callback) {
            callback = callback || function() {};
            var offset = element.offset();
            var elementSize = {
                width: element.outerWidth(),
                height: element.outerHeight()
            };
            var elementOffset = {
                top: offset.top,
                left: offset.left
            };
            if (!isVisible) {
                isVisible = true;
                fullTooltip.show().css({
                    'top': 0,
                    'left': 0
                });
                var endCss = {
                        opacity: 1
                    },
                    startCss = {
                        opacity: 1
                    };
                if (position == 'top') {
                    endCss.top = elementOffset.top - fullTooltip.height();
                    startCss.top = elementOffset.top  - fullTooltip.height() + elementSize.height / 2;
                } else {
                    endCss.top = elementSize.height + elementOffset.top;
                    startCss.top = elementSize.height / 2 + elementOffset.top;
                }
                var tooltipWidth = fullTooltip.outerWidth(),
                    leftPosition = -tooltipWidth/ 2 +
                        element.offset().left +
                        element.outerWidth() / 2;

                startCss.left = Math.min(
                    leftPosition,
                    body.width() - tooltipWidth
                );
                fullTooltip.css(startCss).animate(endCss, animationDuration, function() {
                    if (typeof callback == 'function') {
                        callback(fullTooltip);
                    }
                });
            }
        };
        this.hide = function(callback) {
            callback = callback || function() {};
            var offset = element.offset();
            var elementSize = {
                width: element.outerWidth(),
                height: element.outerHeight()
            };
            var elementOffset = {
                top: offset.top,
                left: offset.left
            };
            if (isVisible) {
                isVisible = false;
                var endCss = {
                    opacity: 0
                };
                if (position == 'top') {
                    endCss.top = elementOffset.top  - fullTooltip.height() + elementSize.height / 2;
                } else {
                    endCss.top = elementSize.height / 2 + elementOffset.top;
                }
                fullTooltip.stop().animate(endCss, animationDuration, function() {
                    fullTooltip.hide();
                    fullTooltip.removeClass('right-position');
                    fullTooltip.css({'right': '', 'left': '', 'bottom': '', 'top': ''});
                    fullTooltip.css(endCss);
                });
            }
        };
        if (params['show_now'] || false) {
            this.show();
            element.removeAttr('onmouseover');
        }
        return this;
    };

    $.fn.tooltip = function(show) {
        $(this).each(function() {
            var _this = $(this);

            var data = {
                'element': _this,
                'title': _this.attr('data-hint-title') || false,
                'content': _this.attr('title') || false,
                'is_fixed': _this.is('[data-hint-fixed]'),
                'close_click': _this.is('[data-hint-close-click]'),
                'position': _this.attr('data-hint-position') || 'bottom',
                'class': _this.attr('data-hint-class') || '',
                'show_now': show,
                'animation_duration': 150
            };
            _this.removeAttr('data-hint-title title data-hint-fixed data-hint-close-click data-hint-position data-hint-class');
            var oneTooltip = new Tooltip(data);

            _this.bind({
                'mouseover': oneTooltip.show
            });
            if (data['close_click'] || false) {
                _this.bind({
                    'click': oneTooltip.hide
                });
            }

            _this.data('tooltip', oneTooltip);
            _this.data('window-move', function(e) {
                var tooltipObject = _this.data('tooltip') || false,
                    tooltipElement = tooltipObject ? tooltipObject.getTooltipJQuery() : false;

                if (tooltipElement && tooltipElement.length) {
                    var target = $(e.target);
                    if (!(_this.is(target) ||
                        _this.find(target).length ||
                        tooltipElement.is(target) ||
                        tooltipElement.find(target).length
                        )) {
                        oneTooltip.hide();
                    }
                } else {
                    $(this).untooltip();
                }
            });
            $(window).bind('mousemove', _this.data('window-move'));
        });
    };
    $.fn.untooltip = function() {
        $(this).each(function() {
            var _this = $(this);
            if (_this.data('tooltip') || false) {
                var oneTooltip = _this.data('tooltip');
                _this.unbind({
                    'mouseover': oneTooltip.show,
                    'click': oneTooltip.hide
                });
                oneTooltip.getTooltipJQuery().empty().remove();
                _this.data('tooltip', false);
                $(window).unbind('mousemove', _this.data('window-move'));
                _this.data('window-move', false);
            }
        });
    };
    iniBlueHint = function(element) {
        $(element).tooltip(true);
    };
    unIniBlueHint = function(element) {
        $(element).untooltip();
    };
    $(function() {
        $('[data-hint]').tooltip();
    });
})(jQuery);
