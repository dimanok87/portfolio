(function($) {
    var navigation_block;
    $.fn.addTouch = function() {
        return this.each(function(i, el) {
            $(el).bind('touchstart touchmove touchend touchcancel',function() {
                var touches = event['changedTouches'],
                    first = touches[0],
                    type = '';
                switch(event.type) {
                    case 'touchstart':
                        type = 'mousedown';
                        break;
                    case 'touchmove':
                        type = 'mousemove';
                        event.preventDefault();
                        break;
                    case 'touchend':
                        type = 'mouseup';
                        break;
                    case 'touchcancel':
                        type = 'mouseup';
                        break;
                    default:
                        return;
                }
                var simulatedEvent = document.createEvent('MouseEvent');
                simulatedEvent.initMouseEvent(type, true, true, window, 1, first.screenX, first.screenY, first.clientX, first.clientY, false, false, false, false, 0/*left*/, null);
                first.target.dispatchEvent(simulatedEvent);
            });
        });
    };


    $.fn.iniLanding = function(options) {

        options = $.extend({
            'item-selector': '>.article',
            'slider-selector': '.second-slider',
            'item-content-selector': '>.content-article:first',
            'onchangeSlide': false
        }, options);

        var start_index = 0;
        var margin_top = 0;

        var Slider = function(slider_object, parent) {
            this.buttons_item = [];
            this.end_index = 0;

            parent = parent || false;

            var slider_model = this,
                visible_item_index = 0,
                items = $(options['item-selector'], slider_object),
                items_data = {},
                top_css = 0,
                max_item_index = items.length - 1,
                parent_jq = slider_object.parent().css({'overflow': 'hidden'}),
                backgrounds = $('> [data-background]', parent_jq);

            var position_slider = 0;

            var setParallaks = function(is_noscroll) {
                is_noscroll = is_noscroll || false;
                var item = items_data['item_' + visible_item_index],
                    middle_z = 0,
                    zelements = $(item['for_parallaks']).each(function() {
                        middle_z+= parseInt($(this).attr('data-z-index'));
                    });
                middle_z = middle_z / zelements.length;
                zelements.each(function() {
                    var _this = $(this),
                        center_parallaks = item['scroll_content'] / 2,
                        z_index = parseInt(_this.attr('data-z-index')),
                        limit = item['scroll_content'] / (100 / (_this.attr('data-z-limit') || 100));
                    z_index = margin_top > limit ? middle_z : z_index;
                    var abs_scroll = margin_top - center_parallaks,
                        margin = abs_scroll - abs_scroll * (100 - z_index) / 100;
                    is_noscroll ?
                        _this.animate({'top':  margin + '%'}) :
                        _this.css({'top':  margin + '%'});

                });
            };

            var scrollPage = function(e, delta, fsc) {

                var full_scroll = fsc ? (fsc['range'] || fsc['range'] == 0 ? fsc['range'] : false) : false;
                e ? e.preventDefault() : false;

                var item = items_data['item_' + visible_item_index];
                var margin_on = full_scroll !== false ? full_scroll : delta * e.deltaFactor;

                margin_top = margin_top - margin_on / slider_object.height() * 100;
                margin_top = margin_top > item['scroll_content'] ?
                    item['scroll_content'] :
                    (margin_top < 0 ? 0 : margin_top);

                var top_position =
                    margin_top == item['scroll_content'] && margin_on < 0 ?
                        true : (margin_top == 0 && margin_on > 0 ? false : null);
                if (top_position !== null) {
                    toItem(visible_item_index + (top_position ? 1 : -1));
                    return true;
                } else {
                    var margin = -item['top'] - margin_top;
                    slider_object.css({
                        'top': margin + '%'
                    });
                    var top_per_percent = top_css / 100,
                        new_position = -margin / top_per_percent,
                        old_position = -position_slider / top_per_percent;
                    position_slider = margin;

                    backgrounds.length ? fadeBackground(old_position, new_position, 1) : false;
                    setParallaks();
                    return false;
                }
            };

            /* For touch devices */

            var scroll_position = false;
            var no_touch = false;

            var noTouch = function(e) {
                no_touch = true;
                resetHandlersMouse();
                $(window).unbind({'mousedown': mouseDownSection});
            };
            var mouseMove = function(e) {
                var range = e.clientY - scroll_position;
                if (scrollPage(false, false, {range: range})) {
                    resetHandlersMouse();
                    scroll_position = false;
                } else {
                    scroll_position = scroll_position + range;
                }
            };
            var resetHandlersMouse = function() {
                $(window).unbind({
                    'mousemove': mouseMove,
                    'mouseup': mouseUp,
                    'selectstart dragstart': noTouch
                });
            };
            var mouseDownSection = function(e) {
                scroll_position = e.clientY;
                $(window).bind({
                    'mousemove': mouseMove,
                    'mouseup': mouseUp,
                    'selectstart dragstart': noTouch
                });
            };
            var mouseUp = function() {
                resetHandlersMouse();
                scroll_position = false;
            };
            this.iniTouch = function() {
                !no_touch ? $(window).bind({'mousedown': mouseDownSection}) : false;
            };

            this.resetTouch = function() {
                $(window).unbind({'mousedown': mouseDownSection});
                scroll_position ? resetHandlersMouse() : false;
            };

            /* / For touch devices */

            var active_imgs = [];
            var old_active_imgs = [];
            var fadeBackground = function(start, end, anim_time) {
                old_active_imgs = active_imgs;
                var imgs = [];

                backgrounds.each(function(i) {
                    var _this = $(this);
                    var this_border_fade = i / backgrounds.length * 100;
                    var to_up = (this_border_fade + (1 / backgrounds.length * 100)) > start && this_border_fade < end;
                    var to_down = this_border_fade < start && (this_border_fade + (1 / backgrounds.length * 100)) > end;

                    if (to_up || to_down) {
                        to_up ? imgs.push({
                            image: _this,
                            delay: anim_time * (this_border_fade - start) / 100
                        }) : imgs.unshift({
                            image: _this,
                            delay: anim_time * (start - this_border_fade - (1 / backgrounds.length * 100)) / 100
                        });
                        active_imgs.push(_this);
                    }
                });

                var iniImage = function(k) {
                    setTimeout(function() {
                        imgs[k]['image'].css({zIndex: 1 + k}).fadeIn(500, function() {
                            k ? imgs[k-1]['image'].stop().hide() : false;
                            imgs[k]['image'].stop().css({zIndex: ''});
                        });

                    }, imgs[k]['delay']);
                };

                if (imgs.length > 1) {
                    for (var k = 0; k < imgs.length; k++) {
                        iniImage(k);
                    }
                }
            };

            var toItem = function(index, navigation, child_index) {
                navigation = navigation || false;
                var abs_index = index;
                index = index > max_item_index ? max_item_index : (index < 0 ? 0 : index);
                child_index = child_index || 0;

                if ((visible_item_index != index) || child_index === true) {
                    var item = items_data['item_' + index],
                        active_item = items_data['item_' + visible_item_index];

                    slider_model['buttons_item'][visible_item_index] ?
                        slider_model['buttons_item'][visible_item_index].removeClass('active') :
                        false;

                    slider_model['buttons_item'][index] ?
                        slider_model['buttons_item'][index].addClass('active') :
                        false;

                    var center_position = item['scroll_content'] / 2,
                        anim_time = 500 * Math.abs(visible_item_index - index);

                    if (!active_item.child) {
                        slider_object.unbind('mousewheel', scrollPage);
                    }

                    margin_top = !navigation ? (visible_item_index > index ? item['scroll_content'] : 0) : center_position;
                    slider_object.stop();
                    var margin = -item['top'] - margin_top,
                        new_position = -margin / (top_css / 100),
                        old_position = -position_slider / (top_css / 100),
                        new_margin = {top: (position_slider = margin) + '%'};

                    if (backgrounds.length && child_index !== true) {
                        fadeBackground(old_position, new_position, anim_time);
                    }

                    child_index && parent ? slider_object.css(new_margin) :
                    slider_object.animate(new_margin, anim_time);

                    if ((active_item || false) && active_item.child) {
                        for (var i = 0; i < active_item.child['buttons_item'].length; i++) {
                            active_item.child['buttons_item'][i].removeClass('active');
                        }
                    }

                    navigation_block.
                        removeClass('active-slide-' + (visible_item_index + (parent.end_index || 0))).
                        addClass('active-slide-' + (index + (parent.end_index || 0)));

                    options.onchangeSlide ? options.onchangeSlide(index + (parent.end_index || 0)) : false;

                    visible_item_index = index;

                    if (item.child) {
                        item.child.toItem(child_index, navigation, true);
                        item.child.iniKeyBoard();
                        slider_model.remKeyBoard();
                    } else {
                        setTimeout(function() {
                            slider_object.bind('mousewheel', scrollPage);
                        }, anim_time);
                        setParallaks();
                    }
                } else {
                    if (parent && abs_index < 0) {
                        slider_object.unbind('mousewheel', scrollPage);
                        parent.toItem(parent.end_index - 1, navigation);
                        parent.iniKeyBoard();
                        slider_model.remKeyBoard();
                    }
                }
            };

            var toNextItem = function() {
                toItem(visible_item_index + 1, true);
            };

            var toPrevItem = function() {
                toItem(visible_item_index - 1, true);
            };

            var keyPressHandler = function(e) {
                switch (e.which) {
                    case 40:
                        toNextItem();
                        break;
                    case 38:
                        toPrevItem();
                        break;
                }
            };

            this.iniKeyBoard = function() {
                $(window).bind({'keydown': keyPressHandler});
                this.iniTouch();
            };

            this.remKeyBoard = function() {
                $(window).unbind({'keydown': keyPressHandler});
                this.resetTouch();
            };

            !parent ? slider_model.iniKeyBoard() : false;

            this.toItem = toItem;
            this.toNextItem = toNextItem;
            this.toPrevItem = toPrevItem;

            var iniButton = function(i) {
                slider_model['buttons_item'][i].click(function() {
                    toItem(i, true);
                });
            };

            var resizeBackground = function(img) {
                img.css({'width': '', 'top': '', 'height': '', 'left': ''});
                var slider_height = slider_object.height(),
                    slider_width = slider_object.width();

                slider_height / img.height() > slider_width / img.width() ?
                    img.height('100%').css({'left': ((slider_width - img.width()) / 2) / (slider_width / 100) + '%'}) :
                    img.width('100%').css({'top': ((slider_height - img.height()) / 2) / (slider_height / 100) + '%'});
            };

            $(window).resize(function() {
                for (var i in items_data) {
                    var imgs = items_data[i]['backgrounds'];
                    for (var k = 0; k < imgs.length; k++) {
                        resizeBackground(imgs[k]);
                    }
                }
            });

            items.each(function(i) {
                var _this = $(this);

                var is_scroll = _this.is('[data-noscroll]') ? 0 : 70;
                var height = 100 + is_scroll;
                var content_article = $(options['item-content-selector'], _this).height(100 / height * 100  + '%');

                var item = items_data['item_' + i] = {
                    jq: _this.height(height + '%'),
                    for_parallaks: $('>[data-z-index]', content_article),
                    scroll_content: is_scroll,
                    height: height,
                    top: top_css,
                    child: false,
                    slider: $(options['slider-selector'], _this),
                    backgrounds: []
                };

                $('[data-background]', _this).each(function(i) {
                    var img = $(this).load(function() {
                        item['backgrounds'].push(img);
                        resizeBackground(img);
                    });
                    i ? img.hide() : false;
                });

                top_css+= i != max_item_index ? height : 0;

                if (!item['slider'].length) {
                    var button_nav = $('<a>').addClass('nav-item').attr({
                        'href': 'javascript:void(0)'
                    }).click(function(e) {
                            e.preventDefault();
                        }).mouseenter(function() {
                            content_button.show('fast', function() {
                                content_button.css({
                                    display: '',
                                    height: '',
                                    padding: '',
                                    margin: '',
                                    width: '',
                                    opacity: ''
                                });
                            });
                        }).mouseleave(function() {
                            content_button.stop().hide('fast');
                        });
                    var content_button = $('<span>').
                        addClass('name-article').
                        appendTo(button_nav).
                        hide().
                        text(_this.attr('data-title') || 'Page #' + (start_index+1));

                    !start_index ? button_nav.addClass('active') : false;
                    !start_index ? navigation_block.addClass('active-slide-' + start_index) : false;

                    slider_model['buttons_item'].push(button_nav);
                    navigation_block.append(button_nav);
                    iniButton(i);
                    start_index++;
                } else {
                    slider_model.end_index = start_index;
                    item.child = new Slider(item['slider'], slider_model);
                    for (var k = 0; k < item.child['buttons_item'].length; k++) {
                        item.child['buttons_item'][k].each(function() {
                            var ch_index = k;
                            $(this).click(function() {
                                toItem(slider_model.end_index, true, ch_index);
                            });
                        });
                    }
                }
            });

            slider_object.bind('mousewheel', scrollPage);

        };
        new Slider($(this));

    };

    $(function() {
        var youtube_url = 'http://www.youtube.com/embed/8ZUhxCtnNF8?autoplay=1',
            youtube_frame = $('#youtube_frame'),
            youtube_container = $('#youtube_container').click(function() {
                youtube_frame.attr({'src': youtube_url}).show();
                youtube_container.unbind('click').addClass('active');
            });

        navigation_block = $('#nav');
        $(window).addTouch();
        $('#section').iniLanding({
            onchangeSlide: function(item) {}
        });

        $('#first-slide').fadeIn(2000);
    });


})(jQuery);