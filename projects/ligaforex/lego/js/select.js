(function($) {
    var selectNum = 0;
    $.fn.select = function(action) {
        action = action || false;
        return $(this).each(function() {
            var _this = $(this);
            if (_this.data('selectObject') || false) {
                switch (action) {
                    case 'refresh':
                        _this.data('selectObject').Refresh();
                        return;
                        break;
                    default:
                        _this.data('selectObject').Refresh();
                        return;
                    }
                }

            new (function() {
                _this.data('selectObject', this);
                this.Refresh = function() {
                    optsContainer.html('');
                    hiddenElements.html('');
                    options = $('option', _this).each(function() {
                        var option = $(this),
                            textItem = option.text(),
                            oneOptionBlock = $('<li>').
                                addClass('select-option').
                                html(
                                    $('<span>').text(textItem)
                                ).mousedown(function(e) {
                                    e.preventDefault();
                                    hideList();
                                    _this.val(option.val()).change();
                                    return false;
                                });
                        option.data('option_decor', oneOptionBlock);
                        if (option.is(':selected')) {
                            oneOptionBlock.addClass('selected');
                            oldSelectedOption = option;
                            selectButtonText.text(option.text());
                        }
                        hiddenElements.append($('<div>').addClass('hidden-item').text(textItem));
                        optsContainer.append(oneOptionBlock);
                    });
                };
                selectNum++;
                var oldSelectedOption = false,
                    isVisibleList = false,
                    noShow = false;

                var groupContainer = _this.parents('.fields-group:first');

                if (groupContainer.length) {
                    var childs = false;
                    for (var i = 0; i < (childs = groupContainer.get(0).childNodes).length; i++) {
                        if (childs[i].nodeName == '#text') {
                            $(childs[i]).remove();
                        }
                    }
                }

                _this.focus(function() {
                        selectContainer.addClass('focus');
                    }).blur(function() {
                        selectContainer.removeClass('focus');
                        hideList();
                    }).change(function() {
                        var selectedOption = options.filter(':selected'),
                            decor = selectedOption.data('option_decor').addClass('selected');
                        oldSelectedOption.data('option_decor').removeClass('selected');
                        oldSelectedOption = selectedOption;
                        if (isVisibleList) {
                            var position = decor.position().top;
                            if (position < listContainer.scrollTop()) {
                                listContainer.scrollTop(position);
                            } else {
                                var normalPosition = position + decor.outerHeight() - listContainer.height();
                                if (normalPosition > listContainer.scrollTop()) {
                                    listContainer.scrollTop(normalPosition);
                                }
                            }
                        }
                        selectButtonText.text(selectedOption.text());
                        oldValue = _this.val();
                    }).keyup(function() {
                        if (oldValue != _this.val()) {
                            _this.change();
                        }
                    }).keydown(function(e) {
                        var key = e.which;
                        if (key == 13 || key == 32 || key == 27) {
                            e.preventDefault();
                            isVisibleList || key == 27 ? hideList() : showList();
                        }
                    });

                var oldValue = _this.val(),
                    parentLabel = _this.parents('label:first'),
                    in_label = parentLabel.length,

                    selectContainer = $(in_label ? '<div>' : '<label>').
                        addClass('select' + ' ' + (_this.attr('class') || '')),
                    selectButtonArea = $('<div>').
                        addClass('select-active-area').
                        appendTo(selectContainer).
                        bind({
                            'click': function() {
                                !isVisibleList && !noShow ? showList() : hideList();
                                noShow = false;
                            },
                            'mousedown': function() {
                                noShow = isVisibleList;
                            }
                        }),
                    optsContainer = $('<ul>').addClass('select-list'),
                    listContainer = $('<div>').
                        addClass('select-list-container').
                        append(optsContainer).
                        hide().mousedown(function(e) {
                            e.preventDefault();
                        }),
                    hiddenElements = $('<div>'),
                    selectButton = $('<div>').addClass('select-button'),
                    selectButtonText = $('<div>').addClass('select-button-text').appendTo(selectButton);

                selectContainer.append(listContainer, hiddenElements);
                var showList = function() {
                    if (!isVisibleList) {
                        listContainer.show();
                        $(window).scrollTop() + $(window).height() <
                            (listContainer.offset().top + listContainer.outerHeight()) ?
                            listContainer.css({
                                'top': 'auto',
                                'bottom': '100%'
                            }) :
                            listContainer.css({
                                'top': '100%'
                            });
                        listContainer.hide().slideDown(0);
                        selectContainer.addClass('active');
                        isVisibleList = true;
                    }
                };

                var hideList = function() {
                    if (isVisibleList) {
                        listContainer.slideUp(0, function() {
                            listContainer.css({
                                'top': '100%',
                                'bottom': 'auto'
                            });
                        });
                        selectContainer.removeClass('active');
                        isVisibleList = false;
                    }
                };


                if (in_label) {
                    selectContainer.addClass('label');
                } else {
                    parentLabel = selectContainer;
                }
                var idSelect = _this.attr('id') || 'select-' + (new Date()).getTime() + selectNum;
                parentLabel.attr('for', idSelect);
                _this.after(selectContainer);
                $('<div>').
                    addClass('select-original-container').
                    append(
                        _this.attr('id', idSelect)
                    ).appendTo(selectContainer);

                selectButtonArea.append(selectButton);
                var options;
                this.Refresh();
            })();

        });
    };
})(jQuery);
