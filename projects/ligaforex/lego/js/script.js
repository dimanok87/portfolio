(function($) {
    $(function() {
        $('select').select();
        $('[data-hint]').tooltip();
    })
})(jQuery);
