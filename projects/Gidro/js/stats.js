(function() {
    $(function() {
        var graphContainer = document.getElementById('graph-container');
        var getTemplate = function(data) {
            var date = data[0],
                d = date.getDate(),
                m = date.getMonth() + 1,
                y = date.getFullYear(),
                h = date.getHours(),
                min = date.getMinutes();

            d = d < 10 ? '0' + d : d;
            m = m < 10 ? '0' + m : m;
            h = h < 10 ? '0' + h : h;
            min = min < 10 ? '0' + min : min;

            return '<div class="graph-tooltip">' +
                    '<div class="content-hint">' +
                        '<div class="text-hint">Дата: ' + d + '.' + m + '.' + y + '</div>' +
                        '<div class="text-hint">Время: ' + h + ':' + min + '</div>' +
                        '<div class="text-hint">Цена: ' + data[1] + ' руб.</div>' +
                    '</div>' +
                '</div>';
        };

        function drawChart() {
            var chartData = new google.visualization.DataTable();

            chartData.addColumn('date', 'Year');
            chartData.addColumn('number', 'Sales');
            chartData.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});

            for (var i = 0; i < data.length; i++) {
                chartData.addRow([
                    data[i][0],
                    data[i][1],
                    getTemplate(data[i])
                ]);
            }

            var chart = new google.visualization.LineChart(graphContainer);
            chart.draw(chartData, {
                curveType: 'function',
                chartArea: {
                    left: 0,
                    top: 0,
                    width: '100%',
                    height: 240,
                    border: 1,
                    backgroundColor: {
                        stroke: '#666',
                        strokeWidth: 1,
                        fill: '#fff'
                    }
                },
                backgroundColor: {
                    fill: 'transparent'
                },
                hAxis: {
                    textPosition: 'out',
                    format:'dd.MM.y',
                    color: '#fff',
                    textStyle: {
                        color: '#808080',
                        fontSize: 13
                    },
                    gridlines: {
                        color: 'transparent',
                        count: 6
                    },
                    baselineColor: 'transparent'
                },
                vAxis: {
                    gridlines: {
                        color: 'transparent',
                        count: 0
                    },
                    baselineColor: 'transparent'
                },
                colors: ['#ca2a2a'],
                tooltip: {
                    isHtml: true,
                    boxShadow: false
                }
            });
        }
        google.setOnLoadCallback(function(){
            drawChart();
        });
    });
})(jQuery);