(function($) {
    $(function() {
        $(function(){
            $(".date-block").each(function() {
                var _th = $(".date-block").datepicker({
                    disabled: false,
                    dateFormat: "dd.mm.yy"
                });
                $('.date-open-btn', _th.parent()).on('click', function() {
                    _th.focus();
                });
            });

            $('.ranger-btn').each(function() {
                var _th = $(this);
                var valueBlock = $('.value-ranger:first', _th.parent());
                _th.openRanger({
                    event: 'click',
                    val: valueBlock.text() * 1,
                    min: valueBlock.attr('data-min') * 1,
                    max: valueBlock.attr('data-max') * 1,
                    onSave: function(val) {
                        valueBlock.text(val);
                    }
                });
            });

        });
    });
})(jQuery);