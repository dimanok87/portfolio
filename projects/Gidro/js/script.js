(function($) {

    /*------------------------------------ Создание списка установок ------------------------------------*/
    /* Тестовый список установок */
    var plantsList = [
        {
            id: 1233,
            type: 'гидропонная NFC',
            status: true
        },
        {
            id: 1243,
            type: 'гидропонная NFC #2',
            status: true
        },
        {
            id: 241,
            type: 'гидропонная NFC #4',
            status: false
        },
        {
            id: 42,
            type: 'гидропонная NFC',
            status: true
        }
    ];

    var propertiesTitlesOfPlant = {
        'number': 'номер установки',
        'type': 'тип системы',
        'status': 'статус работы',
        'button': 'управление установкой'
    };

    var statusesPlantText = {
        'success': 'работает исправно',
        'error': 'ошибки в работе'
    };

    var createPlantItem = function(options) {
        var itemBlock = $('<div>').addClass('plant-item');
        var itemBlockCont = $('<div>').addClass('plant-item-content').appendTo(itemBlock);

        var rowValue = $('<div>').addClass('plant-item-row');
        var controlBtn = $('<div>').addClass('plant-item-button').text(propertiesTitlesOfPlant['button']);
        options['controlClick'] ? controlBtn.on('click', function(){
            options['controlClick'](options['id'] ? options['id'] : options);
        }) : false;
        itemBlockCont.append(
            rowValue.clone().append(
                $('<div>').addClass('plant-item-title-row').text(propertiesTitlesOfPlant['number']),
                ':',
                $('<div>').addClass('plant-item-value-row').text(options['id'])
            ),
            rowValue.clone().append(
                $('<div>').addClass('plant-item-title-row').text(propertiesTitlesOfPlant['type']),
                ':',
                $('<div>').addClass('plant-item-value-row').text(options['type'])
            ),
            rowValue.clone().append(
                $('<div>').addClass('plant-item-title-row').text(propertiesTitlesOfPlant['status']),
                ':',
                $('<div>').
                    addClass('plant-item-value-row ' + (!options['status'] ? 'plant-status-failed' : 'plant-status-success')).
                    text(options['status'] ? statusesPlantText['success'] : statusesPlantText['error'])
            ),
            $('<div>').addClass('plant-item-button-line').append(controlBtn)
        );


        return itemBlock;
    };
    /*------------------------------------ / Создание списка установок ------------------------------------*/

    /*------------------------------------ Открытие ползунка ------------------------------------*/
    var openRanger = function(options) {

        /* Закрываем ranger */
        var closeRanger = function() {
            rangerContainer.detach().empty().remove();
            rangerMask.detach().empty().remove();
        };

        /* Обработчик сохраниния значения */
        var saveRangerValue = function() {
            options['onSave'] ? options['onSave'](options['val']) : false;
            closeRanger();
        };

        var body = $('body:first');
        var win = $(window);

        var rangerContainer = $('<div>').addClass('ranger-container');
        var rangerContentBlock = $('<div>').addClass('ranger-content-block').appendTo(rangerContainer);
        var rangerMask = $('<div>').addClass('ranger-mask').on('click', closeRanger);

        var oldValue;
        var rangerValue = $('<input>').on({
            'focus': function () {
                oldValue = rangerValue.val();
            },
            'change': function() {
                var thVal = rangerValue.val();
                if (oldValue != thVal) {
                    var v = isNaN(thVal) ? oldValue : Math.min(options['max'], Math.max(options['min'], thVal));
                    setValue(v);
                    rangerPosition = (v - options['min']) / fromNullMaxValue * 100;
                }
            }
        });

        var rangerLine = $('<div>').addClass('ranger-line');
        var rangerToggle = $('<div>').addClass('ranger-toggle').appendTo(rangerLine);
        var fromNullMaxValue = options['max'] - options['min'];


        var rangerPosition = ((options['val'] - options['min'])) / fromNullMaxValue * 100;

        /* Устанавливаем значение для ranger */
        var setValue = function(val) {
            rangerValue.val(options['val'] = val);
            rangerToggle.css({
                left: ((val - options['min'])) / fromNullMaxValue * 100 + '%'
            });
        };


        /* Полоса рэнджера */
        var buttonsLine = $('<div>').addClass('buttons-line');
        var saveButton = $('<button>').addClass('ranger-button').text('сохранить').on('click', saveRangerValue);
        var cancelButton = $('<button>').addClass('ranger-button').text('отмена').on('click', closeRanger);

        /* Строим весь блок рэнджера */
        body.append(rangerMask, rangerContainer);
        rangerContentBlock.append(
            rangerValue,
            rangerLine,
            buttonsLine.append(saveButton, cancelButton)
        );

        setValue(options['val']);

        /* Инициализируем ползунок для изменения значения */
        var startMovePosition = 0;
        var fullWidth = 0;
        var downRanger = function(e) {
            if (e.which != 1) return;
            fullWidth = rangerLine.width();
            e.preventDefault();
            startMovePosition = e.clientX;
            rangerToggle.off('mousedown', downRanger);
            win.on({
                'mousemove': moveRanger,
                'mouseup': upRanger
            });
            return false;
        };

        var moveRanger = function(e) {
            e.preventDefault();
            var percentsVal = rangerPosition + ((e.clientX - startMovePosition) / fullWidth * 100);
            percentsVal = Math.min(100, Math.max(0, percentsVal));
            var value = options['min'] + Math.round(percentsVal * fromNullMaxValue / 100);
            setValue(value);
            return false;
        };

        var upRanger = function(e) {
            rangerPosition = Math.min(100, Math.max(0, rangerPosition + ((e.clientX - startMovePosition) / fullWidth * 100)));
            rangerToggle.on('mousedown', downRanger);
            win.off({
                'mousemove': moveRanger,
                'mouseup': upRanger
            });
        };

        rangerToggle.on('mousedown', downRanger);
    };
    /*------------------------------------ / Открытие ползунка ------------------------------------*/


    $(function() {
        /* Инициализируем элемент для отображения ranger */
        $('[data-ranger]').on('click', function() {
            var _this = $(this);
            openRanger({
                val: _this.val() * 1,
                min: _this.attr('data-min') * 1,
                max: _this.attr('data-max') * 1,
                onSave: function(val) {
                    _this.val(val);
                    console.log(val);
                }
            });
        });

        /* Строим список установок */
        var plantsListContainer = $('#plants-list');
        var startControlPlant = function(id) {
            alert('Запуск скрипта управления установкой с id #' + id);
        };
        for (var i = 0; i < plantsList.length; i++) {
            /* Вешаем обработчик на нажатие кнопки "управление установкой" для открытия нового окна */
            /* В качестве аргумента выступает id, который прописывался в параметрах для установки */
            plantsList[i]['controlClick'] = startControlPlant;
            plantsListContainer.append(createPlantItem(plantsList[i]));
        }

    })
})(jQuery);