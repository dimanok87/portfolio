(function($) {

    var productionsPosition = [55.76, 37.64];
    $(function() {
        ymaps.ready(init);
        var myMap, myPlacemark;
        function init(){
            myMap = new ymaps.Map("map", {
                center: productionsPosition,
                zoom: 7,
                controls: []
            });
            myMap.behaviors.disable('scrollZoom');
            myPlacemark = new ymaps.Placemark(productionsPosition, {}, {
                preset: 'islands#redIcon'
            });
            myMap.geoObjects.add(myPlacemark);
            myMap.behaviors.disable('scrollZoom');
        }

        /* Функция для слежки за объектом. Передаём новые координыты, метка устанавливается в них, а карат центруется по ним */
        var initializeProductPosition = function(position) {
            myPlacemark.geometry.setCoordinates(position);
            myMap.setCenter(position);
        };


    })
})(jQuery);