(function($) {
    var openRanger = function(options) {

        /* Закрываем ranger */
        var closeRanger = function() {
            rangerContainer.detach().empty().remove();
            rangerMask.detach().empty().remove();
        };

        /* Обработчик сохраниния значения */
        var saveRangerValue = function() {
            options['onSave'] ? options['onSave'](options['val']) : false;
            closeRanger();
        };

        var body = $('body:first');
        var win = $(window);

        var rangerContainer = $('<div>').addClass('ranger-container');
        var rangerContentBlock = $('<div>').addClass('ranger-content-block').appendTo(rangerContainer);
        var rangerMask = $('<div>').addClass('ranger-mask').on('click', closeRanger);

        var oldValue;
        var rangerValue = $('<input>').on({
            'focus': function () {
                oldValue = rangerValue.val();
            },
            'change': function() {
                var thVal = rangerValue.val();
                if (oldValue != thVal) {
                    var v = isNaN(thVal) ? oldValue : Math.min(options['max'], Math.max(options['min'], thVal));
                    setValue(v);
                    rangerPosition = (v - options['min']) / fromNullMaxValue * 100;
                }
            }
        });

        var rangerLine = $('<div>').addClass('ranger-line');
        var rangerToggle = $('<div>').addClass('ranger-toggle').appendTo(rangerLine);
        var fromNullMaxValue = options['max'] - options['min'];


        var rangerPosition = ((options['val'] - options['min'])) / fromNullMaxValue * 100;

        /* Устанавливаем значение для ranger */
        var setValue = function(val) {
            rangerValue.val(options['val'] = val);
            rangerToggle.css({
                left: ((val - options['min'])) / fromNullMaxValue * 100 + '%'
            });
        };


        /* Полоса рэнджера */
        var buttonsLine = $('<div>').addClass('buttons-line');
        var saveButton = $('<button>').addClass('ranger-button').text('сохранить').on('click', saveRangerValue);
        var cancelButton = $('<button>').addClass('ranger-button').text('отмена').on('click', closeRanger);

        /* Строим весь блок рэнджера */
        body.append(rangerMask, rangerContainer);
        rangerContentBlock.append(
            rangerValue,
            rangerLine,
            buttonsLine.append(saveButton, cancelButton)
        );

        setValue(options['val']);

        /* Инициализируем ползунок для изменения значения */
        var startMovePosition = 0;
        var fullWidth = 0;
        var downRanger = function(e) {
            if (e.which != 1) return;
            fullWidth = rangerLine.width();
            e.preventDefault();
            startMovePosition = e.clientX;
            rangerToggle.off('mousedown', downRanger);
            win.on({
                'mousemove': moveRanger,
                'mouseup': upRanger
            });
            return false;
        };

        var moveRanger = function(e) {
            e.preventDefault();
            var percentsVal = rangerPosition + ((e.clientX - startMovePosition) / fullWidth * 100);
            percentsVal = Math.min(100, Math.max(0, percentsVal));
            var value = options['min'] + Math.round(percentsVal * fromNullMaxValue / 100);
            setValue(value);
            return false;
        };

        var upRanger = function(e) {
            rangerPosition = Math.min(100, Math.max(0, rangerPosition + ((e.clientX - startMovePosition) / fullWidth * 100)));
            rangerToggle.on('mousedown', downRanger);
            win.off({
                'mousemove': moveRanger,
                'mouseup': upRanger
            });
        };
        rangerToggle.on('mousedown', downRanger);
    };

    /* Создали плгин для изменения значений */
    $.fn.openRanger = function(params) {
        return $(this).each(function() {
            $(this).on(params['event'], function() {
                openRanger(params);
            });
        });
    };

})(jQuery);