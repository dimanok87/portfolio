'use strict';

var module = angular.module('app');

module.config(function($routeProvider) {
    $routeProvider.
        when(
        '/', {
            templateUrl: 'templates/dashboard.html',
            controller: 'DashboardController',
            params: {
                name: 'dashboard',
                itemText: 'Dashboard',
                htmlTitle: 'Dashboard',
                pageTitle: 'Dashboard',
                icon: 'dashboard',
                mainMenu: true
            }
        }).
        when(
        '/configuration', {
            templateUrl: 'templates/configuration.html',
            controller: 'ConfigurationController',
            params: {
                name: 'configuration',
                itemText: 'Configuration',
                htmlTitle: 'Configuration',
                pageTitle: 'Configuration',
                icon: 'configuration',
                mainMenu: true
            }
        }).
        when(
        '/quote-and-book', {
            templateUrl: 'templates/quote-and-book.html',
            controller: 'QuoteBookController',
            params: {
                itemText: 'Quote & Book',
                htmlTitle: 'Quote & Book',
                pageTitle: 'Quote & Book',
                icon: 'quotebook',
                mainMenu: true
            }
        }).
        when(
        '/pending-shipments', {
            templateUrl: 'templates/pending-shipments.html',
            controller: 'PendingShipmentsController',
            params: {
                itemText: 'Pending Shipments',
                htmlTitle: 'Pending Shipments',
                pageTitle: 'Pending Shipments',
                icon: 'pendingshipments',
                mainMenu: true
            }
        }).
        when(
        '/orders', {
            templateUrl: 'templates/orders.html',
            controller: 'OrdersController',
            params: {
                itemText: 'Orders',
                htmlTitle: 'Orders',
                pageTitle: 'Orders',
                icon: 'orders',
                mainMenu: true
            }
        }).
        when(
        '/manifesting', {
            templateUrl: 'templates/manifesting.html',
            controller: 'ManifestingController',
            params: {
                itemText: 'Manifesting',
                htmlTitle: 'Manifesting',
                pageTitle: 'Manifesting',
                icon: 'manifesting',
                mainMenu: true
            }
        }).
        when(
        '/reporting', {
            templateUrl: 'templates/reporting.html',
            controller: 'ReportingController',
            params: {
                itemText: 'Reporting',
                htmlTitle: 'Reporting',
                pageTitle: 'Reporting',
                icon: 'configuration',
                mainMenu: true
            }
        }).
        when(
        '/tracking', {
            templateUrl: 'templates/tracking.html',
            controller: 'TrackingController',
            params: {
                itemText: 'Tracking',
                htmlTitle: 'Tracking',
                pageTitle: 'Tracking',
                icon: 'tracking',
                mainMenu: true
            }
        }).
        when(
        '/returns', {
            templateUrl: 'templates/returns.html',
            controller: 'ReturnsController',
            params: {
                itemText: 'Returns',
                htmlTitle: 'Returns',
                pageTitle: 'Returns',
                icon: 'returns',
                mainMenu: true
            }
        }).
        otherwise({
            redirectTo: '/'
        });
});