'use strict';

var module = angular.module('app');

module.config(function($routeProvider) {
    $routeProvider.
        when(
        '/dashboard/my-profile', {
            templateUrl: 'templates/dashboard/my-profile.html',
            controller: 'DashboardProfileController',
            params: {
                parent: 'dashboard',
                htmlTitle: 'Dashboard|My Profile',
                pageTitle: 'My Profile'
            }
        });
});