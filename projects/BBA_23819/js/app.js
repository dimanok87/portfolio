'use strict';

angular.module('app', ['ngRoute', '720kb.datepicker'])
    .controller('menuController', function($scope, $rootScope, $route) {

        $scope.mainMenu = [];
        $scope.menuTemplate = 'templates/elements/main-menu.tpl';
        $rootScope.currentRouteParams = {};
        var menuItems = {};

        for (var i in $route.routes) {
            if ($route.routes[i].params && $route.routes[i].params.mainMenu) {
                if ($route.routes[i].params.name) {
                    menuItems[$route.routes[i].params.name] = $route.routes[i];
                }
                $scope.mainMenu.push($route.routes[i]);
            }
        }

        var activateItemMenu = function(event, current) {
            if (current.$$route.params) {
                if (current.$$route.params.mainMenu) {
                    $scope.currentRoute = current.$$route;
                } else if (current.$$route.params.parent && menuItems[current.$$route.params.parent]) {
                    $scope.currentRoute = menuItems[current.$$route.params.parent];
                }
                $rootScope.currentRouteParams = current.$$route.params;
            } else {
                $rootScope.currentRouteParams = {};
            }
        };

        $scope.$on('$routeChangeSuccess', activateItemMenu);
    })
