'use strict';


var module = angular.module('app');

module.controller('DashboardController', function($scope) {
    $scope.transitTable = {
        carriers: [
            {
                value: 0,
                text: 'Show All'
            },
            {
                value: 1,
                text: 'BBA'
            },
            {
                value: 2,
                text: 'Fast Way'
            },
            {
                value: 3,
                text: 'Toll'
            }
        ]
    };
}).directive('chart', function() {
    return {
        controller: function($scope, $element) {
            var data = {
                labels: ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL"],
                datasets: [
                    {
                        label: "My First dataset",
                        fillColor: "rgba(128, 197, 216, 0.7)",
                        strokeColor: "rgba(220,220,220,0)",
                        pointColor: "rgba(220,220,220,0)",
                        pointStrokeColor: "rgba(0, 0, 0, 0)",
                        pointHighlightFill: "#ffb000",
                        pointHighlightStroke: "#c0dce1",
                        data: [65, 59, 80, 81, 56, 55, 40]
                    },
                    {
                        label: "My Second dataset",
                        fillColor: "rgba(57, 141, 202, 0.7)",
                        strokeColor: "rgba(151,187,205,0)",
                        pointColor: "rgba(151,187,205,0)",
                        pointStrokeColor: "rgba(0, 0, 0, 0)",
                        pointHighlightFill: "#ffb000",
                        pointHighlightStroke: "#c0dce1",
                        data: [28, 48, 40, 19, 86, 27, 90]
                    }
                ]
            };
            var options = {

                ///Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines : true,

                //String - Colour of the grid lines
                scaleGridLineColor : "rgba(0,0,0,0)",

                // String - Scale label font declaration for the scale label
                scaleFontFamily: "Ubuntu, sans-serif",
                // String - Scale label font declaration for the scale label
                scaleFontStyle: "bold",


                // String - Scale label font colour
                scaleFontColor: "#1e252b",

                //Number - Width of the grid lines
                scaleGridLineWidth : 1,

                //Boolean - Whether to show horizontal lines (except X axis)
                scaleShowHorizontalLines: true,

                //Boolean - Whether to show vertical lines (except Y axis)
                scaleShowVerticalLines: true,

                //Boolean - Whether the line is curved between points
                bezierCurve : true,

                //Number - Tension of the bezier curve between points
                bezierCurveTension : 0.4,

                //Boolean - Whether to show a dot for each point
                pointDot : true,

                // Boolean - Whether to show labels on the scale
                scaleShowLabels: true,


                // Boolean - Determines whether to draw tooltips on the canvas or not
                showTooltips: true,

                // Function - Determines whether to execute the customTooltips function instead of drawing the built in tooltips (See [Advanced - External Tooltips](#advanced-usage-custom-tooltips))
                customTooltips: function(tooltip) {
                    // tooltip will be false if tooltip is not visible or should be hidden
                    if (!tooltip) {
                        return;
                    }
                    // Otherwise, tooltip will be an object with all tooltip properties like:
                    // tooltip.caretHeight
                    // tooltip.caretPadding
                    // tooltip.chart
                    // tooltip.cornerRadius
                    // tooltip.fillColor
                    // tooltip.font...
                    // tooltip.text
                    // tooltip.x
                    // tooltip.y
                    // etc...

                },

                tooltipTemplate: false,

                //Number - Radius of each point dot in pixels
                pointDotRadius : 4,

                //Number - Pixel width of point dot stroke
                pointDotStrokeWidth : 1,

                //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                pointHitDetectionRadius : 20,

                //Boolean - Whether to show a stroke for datasets
                datasetStroke : true,

                //Number - Pixel width of dataset stroke
                datasetStrokeWidth : 2,

                //Boolean - Whether to fill the dataset with a colour
                datasetFill : true

                //String - A legend template
                //legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"

            };
            var ctx = $element.get(0).getContext("2d");
            var myLineChart = new Chart(ctx).Line(data, options);
        }
    }
});