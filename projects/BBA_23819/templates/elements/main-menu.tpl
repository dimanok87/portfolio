<ul class="main-menu-list">
    <li ng-repeat="itemMenu in mainMenu" class="main-menu-item" ng-class="{active:currentRoute==itemMenu}">
        <a href="#{{itemMenu.originalPath}}">
            <img ng-src="images/icons/menu/{{itemMenu.params.icon}}_off.svg" class="item-icon off-item-icon"/>
            <img ng-src="images/icons/menu/{{itemMenu.params.icon}}_on.svg" class="item-icon on-item-icon"/>
            <span>{{itemMenu.params.itemText}}</span>
        </a>
    </li>
</ul>
