(function($) {
    $.fn.addTouch = function() {
        return this.each(function(i, el) {
            $(el).bind('touchstart touchmove touchend touchcancel',function() {
                var touches = event['changedTouches'],
                    first = touches[0],
                    type = '';
                switch(event.type) {
                    case 'touchstart':
                        type = 'mousedown';
                        break;
                    case 'touchmove':
                        event.preventDefault();
                        type = 'mousemove';
                        break;
                    case 'touchend':
                        type = 'mouseup';
                        break;
                    case 'touchcancel':
                        type = 'mouseup';
                        break;
                    default:
                        return;
                }
                var simulatedEvent = document.createEvent('MouseEvent');
                simulatedEvent.initMouseEvent(type, true, true, window, 1, first.screenX, first.screenY, first.clientX, first.clientY, false, false, false, false, 0/*left*/, null);
                first.target.dispatchEvent(simulatedEvent);
            });
        });
    };
    $.fn.adaptiveSlider = function(options) {
        options = options || {};
        return $(this).each(function() {
            var _this = $(this);
            var staticSlides = _this.is('[data-static-slider]');
            if (_this.data('slider-adaptive')) {
                _this.data('slider-adaptive').reIni();
                return;
            }

            var sliderObject = new (function() {
                var container = _this;

                var list = $('ul', container);
                var items = $('li', list);
                var firstItem = items.first();
                if (options['repeated']) {
                    list.append(firstItem.clone(true));
                    list.prepend(items.last().clone(true));
                }
                var navigation = $('.slider-navigation', container);

                var visibleSlideItems = 0;
                var countSlides = 0;
                var activeSlide = false;

                var animated = false;
                var pagingBtns = [];

                var maxMargin = 0;

                var toSlide = function(n, noanim) {
                    if (!options['repeated']) {
                        n = Math.max(0, Math.min(n, countSlides - 1));
                    } else {
                        n = n < 0 ? countSlides -1 : n > countSlides - 1 ? 0 : n;
                    }

                    pagingBtns[activeSlide] ?
                        pagingBtns[activeSlide].removeClass('active') :
                        false;

                    if (animated) return;

                    if (activeSlide === n) noanim = true;

                    pagingBtns[activeSlide] ?
                        pagingBtns[activeSlide].removeClass('active') :
                        false;

                    pagingBtns[n] ? pagingBtns[n].addClass('active') : false;

                    if (!options['repeated']) {
                        n == 0 ? prevSlideButton.addClass('disabled') : prevSlideButton.removeClass('disabled');
                        n == countSlides - 1 ? nextSlideButton.addClass('disabled') : nextSlideButton.removeClass('disabled');
                    }

                    activeSlide = n;

                    animated = true;
                    firstItem.animate({
                        marginLeft: -(Math.min(maxMargin, 100 * activeSlide)) + '%'
                    }, noanim ? 0 : 'fast', function() {
                        animated = false;
                    });


                };
                var toNextSlide = function() {
                    toSlide(activeSlide + 1);
                };
                var toPrevSlide = function() {
                    toSlide(activeSlide - 1);
                };

                var nextSlideButton = $('<span>').addClass('nav-btn nav-btn-next').on('click', toNextSlide);
                var prevSlideButton = $('<span>').addClass('nav-btn nav-btn-prev').on('click', toPrevSlide);
                var navigateButtonsContainer = $('<div>').addClass('nav-paging');
                var navigateButton = $('<span>').addClass('paging-btn');

                var createPagingBtn = function(k) {
                    return navigateButton.clone().on('click', function() {
                        toSlide(k);
                    }).appendTo(navigateButtonsContainer);
                };

                var createPagingButtons = function() {
                    var k = 0;
                    pagingBtns = [];
                    while(k < countSlides) {
                        pagingBtns.push(createPagingBtn(k));
                        k++;
                    }
                };

                var iniSlider = function(startSlider) {
                    if (container.is(':hidden')) return false;
                    if (staticSlides) {
                        list.css('width', '');
                        visibleSlideItems = Math.floor(list.width() / items.width());
                    } else {
                        visibleSlideItems = Math.round(list.width() / items.width());
                    }

                    var newCountSlides = Math.ceil(items.length / visibleSlideItems);

                    maxMargin = Math.max(0, (items.length / visibleSlideItems - 1) * 100);

                    if (newCountSlides > 1) {
                        if (countSlides <= 1) {
                            navigation.append(prevSlideButton, navigateButtonsContainer, nextSlideButton);
                        }

                        navigateButtonsContainer.html('');
                        countSlides = newCountSlides;

                        createPagingButtons();
                        toSlide(activeSlide, true);
                    } else {
                        nextSlideButton.detach();
                        prevSlideButton.detach();
                        navigateButtonsContainer.detach();
                    }
                    if (startSlider === true) toSlide(0);

                    if (staticSlides) list.css('width', items.width() * Math.floor(list.width() / items.width()));
                };
                $(window).on("resize", iniSlider);
                this.reIni = iniSlider;
                iniSlider(true);

            })();
            $(this).data('slider-adaptive', sliderObject);
        });
    };

    $.fn.tabs = function(action, params) {

        return $(this).each(function() {

            var _this = $(this);

            if (_this.data('tabs_init')) {
                switch (action) {
                    case 'open':
                        _this.data('tabs_init').openTab(params);
                        break;

                }
                return;
            }

            var tabObject = new (function() {
                var links = $('[data-tab]', _this);
                var blocks = $('[data-tab-block]', _this);
                var title = $('[data-tab-title]', _this);

                var active = blocks.filter('[data-tab-active]:first');

                var activeBlock = false;
                var activeLnk = active.length ?
                    links.filter('[data-tab="' + active.attr('data-tab-block') + '"]') :
                    links.first('data-tab');

                var activateBlock = function(lnk) {
                    var nameBlock = lnk.attr('data-tab');
                    var tabBlock = blocks.filter('[data-tab-block="' + nameBlock + '"]');
                    if (tabBlock.is(activeBlock)) return;

                    activeLnk ? activeLnk.removeClass('active') : false;
                    activeLnk = lnk.addClass('active');

                    if (title.length) {
                        title.text(activeLnk.attr('data-tab-title-text') || title.attr('data-tab-default') || '');
                    }

                    if (tabBlock.length) {
                        if (activeBlock) {
                            activeBlock.slideUp();
                            activeBlock = tabBlock.slideDown();
                        } else {
                            activeBlock = tabBlock.show();
                        }
                        $('.slider-container', activeBlock).adaptiveSlider();
                    }
                };
                this.openTab = function(tn) {
                    activateBlock(links.filter('[data-tab="' + tn + '"]'));
                };
                links.each(function() {
                    var lnk = $(this);
                    lnk.on('click', function() {
                        activateBlock(lnk);
                    });
                });
                activateBlock(activeLnk);
            });
            _this.data('tabs_init', tabObject);
        })
    };

    $(function() {
        var sections = $('section');
        var showedSection = -1;
        var inAnimate = false;
        var duration = 800;
        var iPadDuration = 200;
        var body = $('body');
        var prevNextPagination = $('#prev-next-pagination');
        var nextBtn = $('[data-to-next]', prevNextPagination);
        var prevBtn = $('[data-to-prev]', prevNextPagination);
        var activeScroll = 0;
        var toSlide = function(n) {
            if (inAnimate) return;
            if (n == showedSection) return;
            inAnimate = true;
            var activeSection = sections.eq(n);
            if (!activeSection.length) return;
            var oldSection = sections.eq(showedSection);
            body.removeClass(oldSection.data('body-class'));
            body.addClass(activeSection.data('body-class') || '');
            if (n == 0 && showedSection != 0) {
                prevBtn.addClass('no-active');
            }
            if (n != 0 && showedSection == 0) {
                prevBtn.removeClass('no-active');
            }
            if (n == sections.length - 1 && showedSection != sections.length - 1) {
                nextBtn.addClass('no-active');
            }
            if (n != sections.length - 1 && showedSection == sections.length - 1) {
                nextBtn.removeClass('no-active');
            }
            menuLinks.filter('[href="#' + $('.anchor', oldSection).attr('id') + '"]').removeClass('active');
            var activeAnchor = $('.anchor', activeSection);
            if (!activeAnchor.length) return;
            var offset = activeAnchor.offset()['top'];
            menuLinks.filter('[href="#' + $('.anchor', activeSection).attr('id') + '"]').addClass('active');
            $({temporary_x: currentScroll}).animate({temporary_x: offset}, {
                duration: device.tablet() ? iPadDuration : duration,
                step: function () {
                    $(window).scrollTop(activeScroll = this.temporary_x);
                },
                complete: function() {
                    $(window).scrollTop(activeScroll = offset);
                    currentScroll = offset;
                    setTimeout(function() {
                        inAnimate = false;
                    }, 200);
                }
            });
            showedSection = n;
        };
        var toNextSlide = function(e) {
            e ? e.preventDefault() : false;
            if (inAnimate) return;
            var newShowedSection = Math.min(showedSection + 1, sections.length - 1);
            if (showedSection != newShowedSection) {
                toSlide(newShowedSection);
            }
        };
        var toPrevSlide = function(e) {
            e ? e.preventDefault() : false;
            if (inAnimate) return;
            var newShowedSection = Math.max(showedSection - 1, 0);
            if (showedSection != newShowedSection) {
                toSlide(newShowedSection);
            }
        };
        var menu = $('#navigation');
        var menuLinks = $('a', menu);
        var isClick = false;
        menuLinks.each(function() {
            var _th = $(this);
            var selectorAnchor = _th.attr('href');
            var thAnchor = $(selectorAnchor);
            var section = thAnchor.parents('section');
            section.data('anchor', thAnchor);
            _th.on('click', function(e) {
                isClick = true;
                e.preventDefault();
                toSlide(sections.index(section));
                setTimeout(function() {
                    isClick = false;
                }, duration + 30);
            });
        });

        var currentScroll = 0;

        $('[data-to-next]').on('click', toNextSlide);
        $('[data-to-prev]').on('click', toPrevSlide);


        var startedTouchPoint = 0;
        var startedScroll = 0;

        var pseudoScroll = function(e) {
            $(window).scrollTop(currentScroll - (e.clientY - startedTouchPoint));
        };

        var watchScroll = function(e) {
            e.preventDefault();
            $(window).on('mouseup', watchMouseUp);
            $(window).on('mousemove', pseudoScroll);
            $(window).off('mousemove', watchScroll);
        };

        var watchMouseUp = function(e) {
            var newScroll = currentScroll - (e.clientY - startedTouchPoint);
            var toScroll = currentScroll == newScroll ? 0 : newScroll > currentScroll;
            currentScroll = newScroll;
            $(window).off('mouseup', watchMouseUp);
            $(window).off('mousemove', pseudoScroll);
            if (toScroll === 0) return;
            toScroll ? toNextSlide() : toPrevSlide();
        };
        var iniScrollTablet = function(e) {
            startedTouchPoint = e.clientY;
            $(window).on('mousemove', watchScroll);
        };


        toSlide(0);
        if (!(device.mobile() || device.tablet())) {
            $(window).scrollTop(0);
            $(window).on("scroll", function(e) {
                if (inAnimate || isClick) {
                    $(this).scrollTop(activeScroll);
                    return false;
                }
                var newScroll = $(this).scrollTop();
                if (newScroll !== currentScroll) {
                    newScroll > currentScroll ? toNextSlide() : toPrevSlide();
                }
                return false;
            });
        } else if (device.tablet()) {
            $(window).on('mousedown', iniScrollTablet);
            $(window).addTouch();
        }


        $('.slider-container').adaptiveSlider();
        $('.slider-container-repeated').adaptiveSlider({
            repeated: true
        });
        $('.tabs-list').tabs();

        $('[data-tabs-label]').each(function() {
            var _this = $(this);
            var tabsBlock = _this.attr('data-tabs-label');
            var tabForOpen = _this.attr('data-open-tab');
            if (tabForOpen) {
                _this.on('click', function(e) {
                    $(tabsBlock).tabs('open', tabForOpen);
                });
            }
        });

        $('a[href^=#]').each(function(e) {
            var _this = $(this);
            var href = _this.attr('href');
            var anchor = $(href);
            if (anchor.length) {
                _this.on('click', function(e) {
                    e.preventDefault();
                    var parentSection = anchor.parents('section');
                    toSlide(sections.index(parentSection));
                });
            }
        });



        /* Инициализация кнопки меню */
        var activateMenuBtn = $('.menu-activate-button').click(function() {
            activateMenuBtn.toggleClass('active');
        });
    });


})(jQuery);
