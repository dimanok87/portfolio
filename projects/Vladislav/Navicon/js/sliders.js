(function($) {
    $.fn.adaptiveSlider = function(options) {
        options = options || {};
        return $(this).each(function() {
            var _this = $(this);
            var staticSlides = _this.is('[data-static-slider]');
            if (_this.data('slider-adaptive')) {
                _this.data('slider-adaptive').reIni();
                return;
            }

            var sliderObject = new (function() {
                var container = _this;

                var list = $('ul', container);
                var items = $('li', list);
                var firstItem = items.first();
                if (options['repeated']) {
                    list.append(firstItem.clone(true));
                    list.prepend(items.last().clone(true));
                }
                var navigation = $('.slider-navigation', container);

                var visibleSlideItems = 0;
                var countSlides = 0;
                var activeSlide = false;

                var animated = false;
                var pagingBtns = [];

                var maxMargin = 0;

                var toSlide = function(n, noanim) {
                    if (!options['repeated']) {
                        n = Math.max(0, Math.min(n, countSlides - 1));
                    } else {
                        n = n < 0 ? countSlides -1 : n > countSlides - 1 ? 0 : n;
                    }

                    pagingBtns[activeSlide] ?
                        pagingBtns[activeSlide].removeClass('active') :
                        false;

                    if (animated) return;

                    if (activeSlide === n) noanim = true;

                    pagingBtns[activeSlide] ?
                        pagingBtns[activeSlide].removeClass('active') :
                        false;

                    pagingBtns[n] ? pagingBtns[n].addClass('active') : false;

                    if (!options['repeated']) {
                        n == 0 ? prevSlideButton.addClass('disabled') : prevSlideButton.removeClass('disabled');
                        n == countSlides - 1 ? nextSlideButton.addClass('disabled') : nextSlideButton.removeClass('disabled');
                    }

                    activeSlide = n;

                    animated = true;
                    firstItem.animate({
                        marginLeft: -(Math.min(maxMargin, 100 * activeSlide)) + '%'
                    }, noanim ? 0 : 'fast', function() {
                        animated = false;
                    });


                };
                var toNextSlide = function() {
                    toSlide(activeSlide + 1);
                };
                var toPrevSlide = function() {
                    toSlide(activeSlide - 1);
                };

                var nextSlideButton = $('<span>').addClass('nav-btn nav-btn-next').on('click', toNextSlide);
                var prevSlideButton = $('<span>').addClass('nav-btn nav-btn-prev').on('click', toPrevSlide);
                var navigateButtonsContainer = $('<div>').addClass('nav-paging');
                var navigateButton = $('<span>').addClass('paging-btn');

                var createPagingBtn = function(k) {
                    return navigateButton.clone().on('click', function() {
                        toSlide(k);
                    }).appendTo(navigateButtonsContainer);
                };

                var createPagingButtons = function() {
                    var k = 0;
                    pagingBtns = [];
                    while(k < countSlides) {
                        pagingBtns.push(createPagingBtn(k));
                        k++;
                    }
                };

                var iniSlider = function(startSlider) {
                    if (container.is(':hidden')) return false;
                    if (staticSlides) {
                        list.css('width', '');
                        visibleSlideItems = Math.floor(list.width() / items.width());
                    } else {
                        visibleSlideItems = Math.round(list.width() / items.width());
                    }

                    var newCountSlides = Math.ceil(items.length / visibleSlideItems);

                    maxMargin = Math.max(0, (items.length / visibleSlideItems - 1) * 100);

                    if (newCountSlides > 1) {
                        if (countSlides <= 1) {
                            navigation.append(prevSlideButton, navigateButtonsContainer, nextSlideButton);
                        }

                        navigateButtonsContainer.html('');
                        countSlides = newCountSlides;

                        createPagingButtons();
                        toSlide(activeSlide, true);
                    } else {
                        nextSlideButton.detach();
                        prevSlideButton.detach();
                        navigateButtonsContainer.detach();
                    }
                    if (startSlider === true) toSlide(0);

                    if (staticSlides) list.css('width', items.width() * Math.floor(list.width() / items.width()));
                };
                $(window).on("resize", iniSlider);
                this.reIni = iniSlider;
                iniSlider(true);

            })();
            $(this).data('slider-adaptive', sliderObject);
        });
    };

    $(function() {
        $('.slider-container').adaptiveSlider();
        $('.slider-container-repeated').adaptiveSlider({
            repeated: true
        });
    });
})(jQuery);