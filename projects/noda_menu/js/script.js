(function($) {
    $.fn.iniMegaMenu = function() {
        return $(this).each(function() {
            var _this = $(this);
            var activeItem = false;
            var animated = false;

            var hideActiveItem = function() {
                if (!activeItem) return;
                var activeLi = activeItem['li'];
                activeItem['sub'].slideUp('fast', function() {
                    activeLi.removeClass('active');
                }).css('z-index', '');
                activeItem = false;
            };

            var showNewActiveItem = function(li, sub) {
                if (activeItem && activeItem['li'].is(li)) return hideActiveItem();
                hideActiveItem();
                activeItem = {
                    'li': li.addClass('active'),
                    'sub': sub.slideDown('fast').css('z-index', 5)
                };
            };
            $('.to_top', _this).on('click', hideActiveItem);
            var items = $('li', _this).each(function() {
                var li = $(this);
                var dropLink = $('.drop', li);
                var sub = $('.megamenu_fullwidth', li);
                if (!dropLink.length) return;
                dropLink.on('click', function() {
                    showNewActiveItem(li, sub);
                });
            });
            $(window).on('click', function(e) {
                var target = $(e.target);
                if (!items.find(target).length) {
                    hideActiveItem();
                }
            });
        });
    };
    $(function() {
        $('.megamenu_container').iniMegaMenu();
    })
})(jQuery);