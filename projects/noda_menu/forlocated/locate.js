var redirectURL = 'http://www.windsor.ru/schools/stpete/';
var cityRegExp  = /^Санкт-/;

(function() {
    if (!$.cookie('located')) {
        $.ajax({
            'url': 'http://api.sypexgeo.net/jsonp',
            'dataType': 'JSONP',
            'callback': 'redirect',
            success: function (data) {
                if (cityRegExp.test(data['city']['name_ru'])) {
                    window.location = redirectURL;
                }
            }
        })
    }
    $.cookie('located', '1', {expires: 30});
})(jQuery);