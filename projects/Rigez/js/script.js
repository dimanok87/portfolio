(function($) {
    $(function() {
        var menu = $('.main-menu:first');
        var menuLinks = $('a', menu);

        var isClick = false;
        menuLinks.each(function() {
            var _th = $(this);
            var selectorAnchor = _th.attr('href');
            var thAnchor = $(selectorAnchor);
            if (!thAnchor.length) return;
            _th.data('anchor', thAnchor.length ? thAnchor : false);
            _th.on('click', function(e) {
                e.preventDefault();
                if (oldActiveItem && _th.is(oldActiveItem)) return;
                oldActiveItem ? oldActiveItem.removeClass('active') : false;
                oldActiveItem = _th;
                isClick = true;
                _th.addClass('active');
                $({top: $(window).scrollTop()}).animate({top: thAnchor.offset()["top"]}, {
                    duration: 1000,
                    step: function() {
                        $(window).scrollTop(this.top);
                    },
                    complete: function() {
                        isClick = false;
                    }
                });
            });
        });

        var header = $('header:first');
        var win = $(window);

        var oldActiveItem = false;
        var serchAnchors = function() {
            var winHeight = win.height() / 2;
            var activeItem = false;
            var winScroll = win.scrollTop();
            menuLinks.each(function() {
                var _th = $(this);
                var anch = _th.data('anchor');
                if (!anch) return;
                var offsetTop = anch.offset()['top'];
                if (offsetTop < winScroll && (!activeItem || activeItem['offset'] < offsetTop) || (offsetTop - winScroll < winHeight)) {
                    activeItem = {
                        offset: offsetTop,
                        item: _th
                    };
                }
            });
            oldActiveItem ? oldActiveItem.removeClass('active') : false;
            oldActiveItem = activeItem ? activeItem.item.addClass('active') : false;
        };
        serchAnchors();

        var scrollTimeout = false;
        win.on('scroll', function() {
            if (header.offset()['top'] + header.outerHeight() < win.scrollTop()) {
                menu.addClass('fixed');
            } else {
                menu.removeClass('fixed')
            }
            scrollTimeout ? clearTimeout(scrollTimeout) : false;
            if (isClick) return;
            scrollTimeout = setTimeout(serchAnchors, 100);
        });


        /* PopUp`s */
        var defTitles = {
            callme: 'Заказать звонок',
            quest: 'Задать вопрос'
        };
        var body = $('body:first');
        var activePopUp = false;
        var closePopup = function() {
            if (!activePopUp) return;
            activePopUp.fadeOut(function() {
                activePopUp.detach();
                activePopUp = false;
            });
            mask.fadeOut();
        };
        var openPopup = function(name, title) {
            if (activePopUp) return;
            activePopUp = popups[name].appendTo(body).fadeIn();
            title ? $('.form-modal-title:first', popups[name]).text(title) : false;
            mask.fadeIn();
        };
        var mask = $('<div>').addClass('popup-mask').on('click', closePopup).hide().appendTo(body);
        var popups = {};
        $('.popup-window').each(function() {
            var _this = $(this);
            var popupName = _this.data('name');
            popups[popupName] = _this;
        });
        $('[data-open-popup]').each(function() {
            var _this = $(this);
            var popupName = _this.data('open-popup');
            _this.on('click', function(e) {
                e.preventDefault();
                openPopup(popupName, _this.is('.btn') ? _this.text() : defTitles[popupName]);
                return false;
            });
        });
        $('[data-close-popup]').on('click', closePopup);
        /* / PopUp`s */



        /* Отзывы */
        var scrolledItems = [];
        $('[data-scroll-class]').each(function() {
            var _this = $(this);
            scrolledItems.push(_this);
        });
        var iniScrolledItems = function() {
            var newScrolled = [];
            for (var k = 0; k < scrolledItems.length; k++) {
                var offset = scrolledItems[k].offset()['top'];
                if (
                    offset + (scrolledItems[k].innerHeight() / 2) >= win.scrollTop() &&
                    offset + (scrolledItems[k].innerHeight() / 1.5) <= win.scrollTop() + screen.height
                ) {
                    scrolledItems[k].addClass('inscroll');
                } else {
                    newScrolled.push(scrolledItems[k]);
                }
            }
            scrolledItems = newScrolled;
        };
        win.on('scroll', iniScrolledItems);
        iniScrolledItems();


        /* Sliders */
        $.fn.Slider = function() {
            return $(this).each(function() {
                var container = $(this);
                var parent = container.parent();
                var list = $('ul:first', container);
                var items = $('li', list);

                var widthList = 0;  // %
                var scrollList = 0; // %

                var prevButton = $('<div>').addClass('paging-button prev-button').on('click', function() {
                    scrollList-= 100;
                    reIniSlider();
                });
                var nextButton = $('<div>').addClass('paging-button next-button').on('click', function() {
                    scrollList+= 100;
                    reIniSlider();
                });

                var testScrolled = function() {
                    if (scrollList <= 0) {
                        prevButton.detach();
                        scrollList = Math.max(scrollList, 0);
                    } else {
                        prevButton.appendTo(parent);
                    }
                    if (widthList - scrollList > 100) {
                        nextButton.appendTo(parent);
                    } else {
                        nextButton.detach();
                        scrollList = Math.min(scrollList, widthList - 100);
                    }
                    list.css({'left': -scrollList + '%'});
                };
                var reIniSlider = function() {
                    items.width(100 / items.length + '%');
                    widthList = 100 * items.length;
                    list.width(widthList + '%');
                    testScrolled();
                };

                $('img', list).on('load', function() {
                    reIniSlider();
                });
                reIniSlider();
            });
        };
        $('.slider-list').Slider();
    });
})(jQuery);




