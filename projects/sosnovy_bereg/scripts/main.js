(function($) {
    var inner_block;
    var colors = {
        'booked': {
            color: '#00ff6c',
            opacity: 0.3,
            status: 'Забронирован'
        },
        'sold': {
            color: '#ff002a',
            opacity: 0.33,
            status: 'Куплен'
        },
        'sale': {
            color: '#fffc00',
            opacity: 0.6,
            status: 'Акция'
        },
        'free': {
            color: '#00ffff',
            opacity: 0.6,
            status: 'Свободен'
        }
    };
    var overed_zone = false;

    var Zone = function(t) {
        var type_place = t.attr('data-type') || 'free',
            color = colors[type_place], coo = t.attr('coords'),
            spl_coo = coo.replace(/\s+/, '').split(','),
            xmax = 0, ymax = 0, xmin = 5000, ymin = 5000, vis_baloon = false,
            baloon = false, svg = false, dbl_svg, overed = false, cont_baloon = false;

        for (var i = 0; i < spl_coo.length / 2; i++) {
            var x = spl_coo[i*2], y = spl_coo[i*2 + 1];
            xmax = Math.max(xmax, x);
            ymax = Math.max(ymax, y);
            xmin = Math.min(xmin, x);
            ymin = Math.min(ymin, y);
        }

        var center = [xmin + ((xmax - xmin) / 2), ymin + ((ymax - ymin) / 2)];

        var hideBaloon = function(e) {
            var target = $(e.target);
            if (vis_baloon && !target.is(cont_baloon) && (!(cont_baloon || false) || !cont_baloon.find(target).length)) {
                cont_baloon ? cont_baloon.stop().fadeOut('fast', function() {$(this).remove()}) : false;
                cont_baloon = false;
                baloon ? baloon.stop().fadeOut('fast', function() {$(this).remove() }) : false;
                baloon = false;
                vis_baloon = false;
                $(window).off('mousedown', hideBaloon);
                !overed ? hideZone() : t.on('mousedown', showBaloon);
            }
        };
        var hideZone = function() {
            dbl_svg ? dbl_svg.hide().remove() : false;
            dbl_svg = false;
            svg ? svg.show(): false;
        };
        var showBaloon = function(e) {
            e.preventDefault();
            baloon = $('<div>').addClass('baloon').css({left: center[0], top: center[1]}).hide().appendTo(inner_block).fadeIn('fast');
            cont_baloon = $('<div>').addClass('content-baloon').css({left: center[0], top: center[1]}).hide().appendTo(inner_block);
            cont_baloon.append($('<div>').addClass('text-baloon').append(
                    $('<div>').addClass('name-area').text(t.attr('data-name')),
                    $('<div>').addClass('status-area').text(color['status']),
                    $('<div>').addClass('size-area').text(t.attr('data-size')),
                    $('<div>').addClass('price-area').text(t.attr('data-price'))
                )).fadeIn('fast', function() {
                    $(window).on('mousedown', hideBaloon);
                    t.off('mousedown', showBaloon);
                });
            vis_baloon = true;
        };

        var free_svg = $('<svg xmlns="http://www.w3.org/2000/svg" version="1.1">'  +
            '<polygon points="'+coo+'" fill="' + colors['free']['color'] + '" opacity="' + colors['free']['opacity'] + '"/>' +
            '</svg>').hide();

        if (type_place != 'free') {
            svg = $('<svg xmlns="http://www.w3.org/2000/svg" version="1.1">'  +
                '<polygon points="'+coo+'" fill="' + color['color'] + '" opacity="' + color['opacity'] + '"/>' +
                '</svg>').appendTo(inner_block);
        }
        var tobj = this;

        var overZone = function(e) {
            e.preventDefault();
            t.off('mouseenter', overZone);
            overed_zone = tobj;
            overed = true;
            inner_block.append(dbl_svg = (dbl_svg || free_svg.clone()).css({'display': 'block'}));
            svg ? svg.hide(): false;
            t.on('mouseleave', outZone);
            !vis_baloon ? t.on('mousedown', showBaloon) : false;
            return false;
        };

        var outZone = function(e) {
            e.preventDefault();
            overed_zone = overed = false;
            !vis_baloon ? hideZone() : false;
            t.on('mouseenter', overZone);
            t.off('mouseleave', outZone);
            !vis_baloon ? t.off('mousedown', showBaloon) : false;
            return false;
        };
        t.on('mouseenter', overZone);
    };


    $(function() {
        inner_block = $('#map-region');
        $('area').each(function() {
            new Zone($(this));
        });
        $('.link').each(function() {
            var t = $(this).hover(
                function() {
                    img.show();
                },
                function() {
                    img.hide();
                });
            var img = $('#sector-'+ t.attr('data-sector'));
        });
        var map_container = $('<div>').addClass('map-road');
        $('#show-map').click(function() {
            var cont = map_container.clone();
            openPopup({'content': cont, title: '<span>Карта проезда</span>'});
            new google.maps.Map(cont.get(0), {
                center: new google.maps.LatLng(-33.85700475676243, 151.2160563468933),
                zoom: 16,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                draggable: true
            });
        });
        $('[data-for-popup]').each(function() {

        });
    })
})(jQuery);