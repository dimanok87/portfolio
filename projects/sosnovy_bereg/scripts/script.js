var body, openPopup;
(function($) {
    var mask_popup = $('<div>').addClass('mask-popup').hide(),
        popup_wind = $('<div>').addClass('popup-wind').hide(),
        popup_cont = $('<div>').addClass('content-popup'),
        popup_title = $('<div>').addClass('title-popup'),
        close_button = $('<div>').addClass('close-popup').click(function() {
            closePopup();
        }),
        opened_popup = false,
        opened_mask = false;

    openPopup = function(params) {
        opened_mask = mask_popup.clone().appendTo(body).fadeIn('fast');
        opened_popup = popup_wind.clone();
        opened_popup.append(
            popup_title.clone().html(params['title'] || '').append(close_button.clone(true)),
            popup_cont.clone().html(params['content'])
        ).appendTo(body).fadeIn('fast');
        params['width'] ? opened_popup.width(params['width']) : false;
        opened_popup.height() < mask_popup.height() ? opened_popup.height(mask_popup.height()) : false;
        opened_popup.css({'margin-left': -opened_popup.width()/2 + 'px', 'margin-top': -opened_popup.height()/2 + 'px'});
    };

    var closePopup = function() {
        opened_popup ? opened_popup.fadeOut('fast', function() {
            $(this).remove();
        }) : false;
        opened_popup = false;
        opened_mask ? opened_mask.fadeOut('fast', function() {
            $(this).remove();
        }) : false;
        opened_mask = false;
    };

    $(function() {
        body = $('body:first');
        $('[data-popup]').each(function() {
            var t = $(this).click(function(e) {
                e.preventDefault();
                $.ajax({
                    url: href,
                    dataType: type_file,
                    success: function(content) {
                        openPopup({
                            content: $('<div>').addClass('popup-' + type_file).html(content),
                            width: type_file == 'text' ? 585 : false
                        });
                    }
                });
                return false;
            }), href = t.attr('href');
            var type_file = t.attr('href').split('.');
            type_file = type_file[type_file.length - 1];
            type_file = type_file == 'txt' ? 'text' : 'html';
        });
        $('[data-print]').each(function() {
            var t = $(this).click(function(e) {
                e.preventDefault();
                $.ajax({
                    url: href,
                    dataType: type_file,
                    success: function(content) {
                        $('<div>').html(content).printElement();
                    }
                });
                return false;
            }), href = t.attr('href');
            var type_file = t.attr('href').split('.');
            type_file = type_file[type_file.length - 1];
            type_file = type_file == 'txt' ? 'text' : 'html';
        });

    })
})(jQuery);