$(function () {

	'use strict';

	$(document).on('click', '.js-get-form', function(event) {
		event.preventDefault();
		$('.b-popup_form,.b-popup__shadow').fadeIn(450);
	});

	var swiper = new Swiper('.swiper-container', {
        nextButton: '.larr',
        prevButton: '.rarr',
        speed: 800,
        autoplay: 4500,
        loop: true,
        simulateTouch: false,
        effect:'fade'
    });

    $(document).on('keypress', '.js-textarea', function(event) {
    	if(calculate_lenght()) {
    		event.preventDefault();
    	}
    });

    $(document).on('change', '.js-textarea', function(event) {
    	if(calculate_lenght()) {
    		event.preventDefault();
    	}
    });

    $(document).on('keyup', '.js-textarea', function(event) {
    	if(calculate_lenght()) {
    		event.preventDefault();
    	}
    });

    $(document).on('keydown', '.js-textarea', function(event) {
    	if(calculate_lenght()) {
    		event.preventDefault();
    	}
    });


	$('body').on('focus', '.b-form__input',function() {
		$(this).removeClass('error');
	});
	$('body').on('change', '.b-form__input',function() {
		$(this).validateMe();
	});


	$('body').on('submit', '.js-form', function(event) {
		event.preventDefault();
		var th=$(this);
		var _check_obj = this;
		$(this).find('input[type="text"],textarea,select').each(function() {
			$(this).validateMe();
		});
		window.setTimeout(function(){
			if (th.find(".error").size()>=1){
				return false;
			}
			else if (th.find(".error").size()>=1) {
				return false;
			}
			else {
				var _params = $(_check_obj).serialize();
				
				$.ajax({
					url: th.attr('action'),
					data: th.serialize(),
					type: th.attr('method'),
					dataType: 'html',
					error: function(msg) {
						$('.b-popup_form').fadeOut(450);
			        	$('.b-popup_error').show();
			        	setTimeout(function() {
		        			$('.b-popup__close').click();
			        	},2500);
					},
					success: function(msg){
			        	$('.b-popup_form').fadeOut(450);
			        	$('.b-popup_tnx').show();
			        	setTimeout(function() {
		        			$('.b-popup__close').click();
			        	},2500);
			      	}
				});
			}
		},100);
	});

	$('.b-popup__close').click(function(event) {
		event.preventDefault();
		$('.b-popup__shadow,.b-popup').fadeOut(450);
	});

	$('.b-popup__shadow').click(function(event) {
		if($(event.target).closest('.b-popup').length==0) {
			$('.b-popup__close').click();
		}
	});

	if($('body').attr('data-logged')=='true') {
		$('.b-popup_form,.b-popup__shadow').show();
	}

});


function calculate_lenght() {
	var $this=$('.js-textarea');
	var maxlength=$this.attr('maxlength'),
		currlength=$this.val().length,
		end=morftest(maxlength-currlength);
	$('.js-textarea-counter').text(maxlength-currlength+' символ'+end);
	if(currlength<=maxlength) {
		return 0
	}
	else {
		return 1
	}
}

function morftest(n) {
	var str='';
	if((n%10==1) && (n!=11)) {
		str='';
	}
	else {
		str='ов';
	}
	return str
}

$.fn.validateMe = function() {
	var t_val=$(this).val(),
    	t_parent=$(this);
    if(!(t_val)) {
        t_parent.addClass('error');
    }
    else if($(this).attr('name')=='popup_tell') {
        if(!(validateTell($(this).val()))) {
        	t_parent.addClass('error');
        }
    }

    else if($(this).attr('name')=='popup_mail') {
        if(!(validateEmail($(this).val()))) {
        	t_parent.addClass('error');
        }
    }
};


function validateTell($elem) {
    var tellReg= /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
    if( !tellReg.test( $elem ) ) {
    return false;
  } else {
    return true;
  }
}
function validateEmail($elem) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  if( !emailReg.test( $elem ) ) {
    return false;
  } else {
    return true;
  }
}