(function($) {

    var APP_REDIRECT = 'http://portfolio.mysiwe.com/projects/YJ/Slider/vkaccess.html';
    var VK_APP_ID = 4908457;
    var FB_APP_ID = 687762341329018;

    var ERRORS = {
        LIMIT_FILE_SIZE: {
            title: 'Не удалось добавить изображение',
            text: 'Размер изображения не должен превышать 5Мб'
        },
        MIME_TYPE_ERROR: {
            title: 'Не удалось добавить изображение',
            text: 'Вы можете добавить только изображение'
        },
        SOCIAL_ERROR: {
            title: 'Не удалось добавить изображение',
            text: 'Фотография не найдена'
        }
    };


    var form, fileField, socialField, photoPreview, photoContainer, vkButton, fbButton, maxFileSize, photoPreviewSrc;

    var emptyFileField = function() {
        !socialField.val().length ? photoPreview.attr('src', photoPreviewSrc).css({width: '',height: '', margin: ''}) : false;
        var cloneFileField = fileField.clone(true);
        fileField.after(cloneFileField);
        fileField.empty().remove();
    };

    var showError = function(options) {
        var alphaMask = $('<div>').css({
            position: 'fixed',
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            zIndex: 9998

        });
        var popupError = $('<div>').addClass('b-popup b-popup_error').css({
            zIndex: 9999
        }).hide();

        var closeButton = $('<div>').addClass('b-popup__close').appendTo(popupError);
        var headerError = $('<div>').addClass('b-popup__hdr').html(options['title']).appendTo(popupError);
        var text = $('<p>').html(options['text']).appendTo(popupError);

        closeButton.on('click', function() {
            alphaMask.fadeOut(function() {
                alphaMask.empty().remove();
            });
            popupError.fadeOut(function() {
                popupError.empty().remove();
            });
            form.fadeIn();
        });
        var form = $('.b-popup_form:first').fadeOut(500);
        $('body').append(alphaMask, popupError);
        popupError.fadeIn(500);

    };

    var previewFile = function(file, cb) {
        if (!file.type.match('image.*')) {
            emptyFileField();
            showError(ERRORS['MIME_TYPE_ERROR']);
        } else {
            if (file['size'] > maxFileSize) {
                emptyFileField();
                showError(ERRORS['LIMIT_FILE_SIZE']);
                return;
            }
            socialField.val('');
            var fileReader = new FileReader();
            fileReader.onload = (function(file) {
                return function(e) {
                    cb ? cb(this.result) : false;
                };
            })(file);
            fileReader.readAsDataURL(file);
        }
        return false;
    };
    var previewImage = function(src) {
        var image = new Image();
        $(image).on('load', function() {
            var cW = photoContainer.width(),
                cH = photoContainer.height(),
                k = Math.min(image.width / cW, image.height / cH),
                w = image.width / k,
                h = image.height / k;
            photoPreview.css({
                width: w,
                height: h,
                marginLeft: (cW - w) / 2,
                marginTop: (cH - h) / 2
            }).attr('src', src);
        });
        image.src = src;
    };

    $(function() {
        vkButton = $('#vk-photo-button');
        fbButton = $('#fb-photo-button');
        form = $('.b-popup__form:first');
        fileField = $('#photo-field');
        socialField = $('#photo-field-social');
        photoPreview = $('.b-slider__photo', form);
        photoContainer = $('.b-form__photoinput', form);
        maxFileSize = $('#max-file-size').val();
        photoPreviewSrc = photoPreview.attr('src');
        fileField.on('change', function() {
            previewFile(this['files'][0], previewImage);
        });
        vkButton.on('click', function(e) {
            e.preventDefault();
            openVKAuth();
            return false;
        });
        fbButton.on('click', function(e) {
            e.preventDefault();
            openFBAuth();
            return false;
        });
    });

//--- Для ВК ----

    var VK_PARAMS_PHOTO = false;
    var openVKAuth = function() {
        if (VK_PARAMS_PHOTO) {
            window['callbackVK'](VK_PARAMS_PHOTO);
            return;
        }
        var url = 'https://oauth.vk.com/authorize';
        var winParams = [
            'client_id=' + VK_APP_ID,
            'redirect_uri=' + encodeURIComponent(APP_REDIRECT),
            'display=popup',
            'v=5.30',
            'response_type=token',
            'state=callbackVK'
        ];
        window.open(url + '?' + winParams.join('&'), 'VK', "width=800,height=600");
    };
    window['VKCallbackFunc'] = function(result) {
        var photo = result['response'][0]['photo_max']
        if (photo) {
            emptyFileField();
            socialField.val(photo);
            previewImage(photo);
        } else {
            showError(ERRORS['SOCIAL_ERROR']);
        }
    };

    window['callbackVK'] = function(params, win) {
        win ? win.close() : false;
        if (!VK_PARAMS_PHOTO) {
            VK_PARAMS_PHOTO = params['expires_in'] ? params : false;
            VK_PARAMS_PHOTO ? setTimeout(function() {
                VK_PARAMS_PHOTO = false;
            }, params['expires_in'] * 1000) : false;
        }

        var script = document.createElement('SCRIPT');
        script.src = "https://api.vk.com/method/users.get?user_id=" +
        params['user_id'] + "&v=5.30&access_token=" +
        params['access_token'] + "&callback=VKCallbackFunc&fields=photo_max";
        document.getElementsByTagName("head")[0].appendChild(script);
    };

//--- Для FB ----
    var FB_PARAMS_PHOTO = false;
    var openFBAuth = function() {
        if (FB_PARAMS_PHOTO) {
            window['callbackFB'](FB_PARAMS_PHOTO);
            return;
        }
        var url = 'https://www.facebook.com/dialog/oauth';
        var winParams = [
            'app_id=' + FB_APP_ID,
            'redirect_uri=' + encodeURIComponent(APP_REDIRECT),
            'display=popup',
            'response_type=token',
            'state=callbackFB',
            'scope=public_profile'
        ];
        window.open(url + '?' + winParams.join('&'), 'FB', "width=600,height=600");
    };
    window['FBCallbackFunc'] = function(photo) {
        if (photo && photo['data'] && photo['data']['url']) {
            emptyFileField();
            socialField.val(photo['data']['url']);
            previewImage(photo['data']['url']);
        } else {
            showError(ERRORS['SOCIAL_ERROR']);
        }
    };
    window['callbackFB'] = function(params, win) {
        win ? win.close() : false;
        if (!FB_PARAMS_PHOTO) {
            FB_PARAMS_PHOTO = params['expires_in'] ? params : false;
            FB_PARAMS_PHOTO ? setTimeout(function() {
                FB_PARAMS_PHOTO = false;
            }, params['expires_in'] * 1000) : false;
        }
        var script = document.createElement('SCRIPT');
        script.src = "https://graph.facebook.com/v2.3/me/picture?access_token=" +
        params['access_token'] + "&callback=FBCallbackFunc&method=get&width=800&height=600&pretty=0&sdk=joey";
        document.getElementsByTagName("head")[0].appendChild(script);
    };

})(jQuery);