var headerHeight = 65;
(function($) {
    $(function() {
        //--- Инициализация карты ---
        ymaps.ready(init);
        var myMap;
        function init(){
            myMap = new ymaps.Map("map", {
                center: [55.76, 37.64],
                zoom: 7,
                controls: []
            });
            myMap.behaviors.disable('scrollZoom');
        }
        //--- / Инициализация карты ---

        //--- Инициализация меню ---
        var activeLink = false;
        var activateAnchor = function(anchor, lnk, fl) {
            var activateSelected = function() {
                if (!lnk.is(activeLink)) {
                    activeLink.removeClass("selected");
                    activeLink = lnk.addClass("selected");
                } else {
                    return;
                }
            };
            if (fl || false) {
                activateSelected();
                return;
            }
            $({top: $(window).scrollTop()}).animate({top: anchor.offset()["top"] - headerHeight}, {
                duration: 1000,
                step: function() {
                    $(window).scrollTop(this.top);
                }
            }, function() {
                activateSelected();
            });
        };
        var iniAnchor = function() {
            var _this = $(this);
            var attrName = _this.attr("name");
            var link = $("a[href=#" + attrName + "]").click(function(e) {
                e.preventDefault();
                activateAnchor(_this, $(this));
            });
            _this.data("links", link);
            if (link.is(".selected")) {
                activeLink = link;
            }
        };
        var anchors = $("a[name]").each(iniAnchor);
        //--- / Инициализация меню ---

        var scrollTimeout = false;
        //--- Инициализация скролла ---
        $(window).bind("scroll", function() {
            scrollTimeout ? clearTimeout(scrollTimeout) : false;
            scrollTimeout = setTimeout(function() {
                scrollTimeout = false;
                var windScroll = $(window).scrollTop();
                anchors.each(function() {
                    var _this = $(this);
                    if (_this.offset()["top"] + 50 >= windScroll) {
                        activateAnchor(_this, _this.data("links"), true);
                        return false;
                    }
                });
            }, 100);
        });
        //--- /Инициализация скролла ---

        //--- Отзывы ---
        var activeCommentBlock = false, activeCommentLink = false;
        var iniCommentLink = function() {
            var _this = $(this);
            var commentBlock = $("#" + _this.attr("data-comment"));
            if (!commentBlock.is(":hidden")) {
                activeCommentLink = _this.addClass("active-link");
                activeCommentBlock = commentBlock;
            }
            _this.click(function() {
                if (!activeCommentBlock.is(commentBlock)){
                    activeCommentBlock.hide();
                    activeCommentLink.removeClass("active-link");
                    activeCommentLink = _this.addClass("active-link");
                    activeCommentBlock = commentBlock.show();
                }
            });
        };
        var commentsLinks = $("[data-comment]").each(iniCommentLink);
        //--- / Отзывы ---

        //--- PopUp --//
        var pageMask = $("<div>").addClass("popup-background").hide();
        var popupForm = $("#popup-form");
        $("body:first").append(pageMask, popupForm);
        var closePopupButton = $("#close-popup", popupForm);
        $(".open-popup").click(function() {
            $(pageMask).fadeIn('fast');
            popupForm.fadeIn('fast');
        });
        $(pageMask).click(function() {
            $(pageMask).fadeOut('fast');
            popupForm.fadeOut('fast');
        });
        closePopupButton.click(function() {
            $(pageMask).fadeOut('fast');
            popupForm.fadeOut('fast');
        });
        //--- PopUp --//
        //--- Slider --//
        var countSlides = 3;
        var dopList = 52;
        $('#articles-news').each(function() {
            var _th = $(this);
            var list = $("ul", _th);
            var items = $(".item-news", list);
            var itemsLength = items.length;
            var maxSteps = itemsLength - countSlides;
            var nextButton = $(".next-button", _th).click(function() {
                toSlide(prevStep + 1);
            });
            var prevButton = $(".prev-button", _th).hide().click(function() {
                toSlide(prevStep - 1);
            });
            var percentsPlus = dopList / _th.width() * 50 * (itemsLength - 1);
            var itemWidth = 100 / itemsLength;
            var listWidth = 100 / countSlides * itemsLength + percentsPlus;
            list.width(listWidth + "%");
            items.width(itemWidth + "%");
            var prevStep = 0;
            var toSlide = function(n) {
                if (n < 0 || n > maxSteps) {
                    return;
                }
                if (n == 0) {
                    prevButton.hide();
                } else if (prevStep == 0) {
                    prevButton.show();
                }
                if (n == maxSteps) {
                    nextButton.hide();
                } else if (prevStep == maxSteps) {
                    nextButton.show();
                }
                prevStep = n;
                list.animate({
                    left: -(listWidth / itemsLength) * prevStep + "%"
                });
            }
        });
        //--- Slider --//
        //--- Валидация формы ---
        var regExps = {
            email: /^[a-zA-Z0-9\-\.\_]+\@[a-zA-Z0-9\-\.\_]+\.[a-zA-Z]{2,4}$/,
            phone: /^\+?[\d]{1,3}\s?\(?[\d]{1,4}\)?\s?[\d\-?]{3,10}$/,
            name: /^[a-zA-Zа-яА-Я]{3,20}$/
        };
        var emptyErrors = {
            email: "Введите e-mail",
            phone: "Введите номер",
            name: "Введите Имя"
        };
        var errorRegexpTexts = {
            email: "Неверный формат e-mail",
            phone: "Проверьте номер",
            name: "Проверьте Имя"
        };
        var validateField = function(inp) {
            var errorBaloon = inp.data("errorBaloon");
            var name = inp.attr("name");
            var regExp = regExps[name];
            var error = false;
            var emptyErrorText = emptyErrors[name] || false;
            var formatError = errorRegexpTexts[name] || false;
            var val = inp.val();
            var errorMsg = false;
            if (val != inp.data("oldValue") || !val.length) {
                if (!val.length) {
                    errorMsg = emptyErrorText;
                } else if (!regExp.test(val)) {
                    errorMsg = formatError;
                }
            }
            if (errorMsg) {
                errorBaloon.text(errorMsg);
                if (!error) {
                    errorBaloon.fadeIn();
                    error = true;
                    inp.data('error', true);
                }
            } else {
                if (error) {
                    errorBaloon.fadeOut();
                    error = false;
                    inp.data('error', false);
                }
            }
            inp.data("oldValue", val);
        };
        var validateFormActivate = function(form) {
            var inputs = $("input", form).each(function() {
                var inp = $(this);
                var errorBaloon = $("<div>").addClass("error-field").hide();
                inp.data("errorBaloon", errorBaloon);
                inp.data("oldValue", "");
                inp.after(errorBaloon);
                inp.bind("blur", function(){
                    validateField($(this));
                });
            });

            form.bind("submit", function(e) {
                e.preventDefault();
                var data = {};
                var errors = false;
                inputs.each(function() {
                    var inp = $(this);
                    validateField(inp);
                    if (inp.data('error')) errors = true;
                    data[inp.attr('name')] = inp.val();
                });
                if (errors) {
                    return;
                }
                $.ajax({
                    url: "sendmail.php",
                    data: data,
                    type: "POST",
                    success: function(data) {
                        console.log(data);
                    }
                });
            });
        };
        //--- Валидация формы ---
        $('form').each(function() {
            validateFormActivate($(this));
        });
        var closePopup = function() {
            popupMask.fadeOut(function() {
                popupMask.detach();
            });
            openedPopup.fadeOut(function() {
                openedPopup.detach();
            });
        };
        var popupMask = pageMask.clone("false").click(closePopup);
        var openPopup = function(html) {
            popupMask.appendTo("body").fadeIn();
            openedPopup = html.appendTo("body").fadeIn();
        };
        //--- Privacy open ---
        var policyHtml = false;
        var openedPopup = false;
        var openPrivacy = function(e) {
            e.preventDefault();
            if (policyHtml) {
                openPopup(policyHtml);
                return;
            }
            $.ajax({
                "url": "policy.html",
                "success": function(data) {
                    var closeButton = $('<div class="close-popup-button"></div>').click(closePopup);
                    openPopup(policyHtml = $(data).hide().prepend(closeButton));
                }
            });
        };
        $(".privacy-open").click(openPrivacy);
        //--- Privacy open ---
    })
})(jQuery);