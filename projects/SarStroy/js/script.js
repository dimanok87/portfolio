(function($) {
    $(function() {
        //--- Инициализация карты ---
        var myMap;
        function init(){
            myMap = new ymaps.Map("map", {
                center: [51.552344, 46.019264],
                zoom: 16,
                controls: []
            });
            myMap.behaviors.disable('scrollZoom');
            var myPlacemark = new ymaps.Placemark([51.552344, 46.019264]);
            myMap.geoObjects.add(myPlacemark);
        }
        //--- / Инициализация карты ---
        ymaps.ready(init);


        var menu = $('.main-menu:first');
        var menuLinks = $('a', menu);
        var anchors = $('a.anchor');

        var isClick = false;
        menuLinks.each(function() {
            var _th = $(this);
            var selectorAnchor = _th.attr('href');
            var thAnchor = $(selectorAnchor);
            if (!thAnchor.length) return;
            _th.data('anchor', thAnchor.length ? thAnchor : false);
            _th.on('click', function(e) {
                e.preventDefault();
                if (oldActiveItem && _th.is(oldActiveItem)) return;
                oldActiveItem ? oldActiveItem.removeClass('active') : false;
                oldActiveItem = _th;
                isClick = true;
                _th.addClass('active');
                $({top: $(window).scrollTop()}).animate({top: thAnchor.offset()["top"]}, {
                    duration: 1000,
                    step: function() {
                        $(window).scrollTop(this.top);
                    },
                    complete: function() {
                        isClick = false;
                    }
                });
            });
        });

        var header = $('header:first');
        var win = $(window);

        var oldActiveItem = false;
        var serchAnchors = function() {
            var winHeight = win.height() / 2;
            var activeItem = false;
            var winScroll = win.scrollTop();
            menuLinks.each(function() {
                var _th = $(this);
                var anch = _th.data('anchor');
                if (!anch) return;
                var offsetTop = anch.offset()['top'];
                if (offsetTop < winScroll && (!activeItem || activeItem['offset'] < offsetTop) || (offsetTop - winScroll < winHeight)) {
                    activeItem = {
                        offset: offsetTop,
                        item: _th
                    };
                }
            });
            oldActiveItem ? oldActiveItem.removeClass('active') : false;
            oldActiveItem = activeItem.item.addClass('active');
        };
        serchAnchors();

        var scrollTimeout = false;
        win.on('scroll', function() {
            if (header.offset()['top'] + header.outerHeight() < win.scrollTop()) {
                menu.addClass('fixed');
            } else {
                menu.removeClass('fixed')
            }
            scrollTimeout ? clearTimeout(scrollTimeout) : false;
            if (isClick) return;
            scrollTimeout = setTimeout(serchAnchors, 100);
        });
    });
})(jQuery);