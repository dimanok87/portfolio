var rangersValues = [
    {
        user_count: 1,
        price_license: 6300
    },
    {
        user_count: 5,
        price_license: 21600
    },
    {
        user_count: 10,
        price_license: 41400
    },
    {
        user_count: 20,
        price_license: 78000
    },
    {
        user_count: 50,
        price_license: 187200
    },
    {
        user_count: 100,
        price_license: 360000
    }
];

var modulesPrice = {
    on_premises: {
        high_load: 84600,
        component_accounting: 13000,
        component_trade: 17400,
        component_payroll_hr: 17400,
        component_erp: 360000
    }
};


var highLoadValue = 5;

(function($) {
    $.fn.iniRangers = function(rangerValues, miniVariants) {
        miniVariants = miniVariants || false;
        return $(this).each(function() {
            var _this = $(this),
                oldValue,
                value = _this.val(),
                width = 100 / (rangerValues.length - 1),
                rangerArrowPosition,
                rangerContainer = $('<div>').
                addClass('ranger-container').
                bind('mousedown mousemove', function(e) {
                    e.preventDefault();
                });
            var axisContainer = $('<div>').addClass('ranger-axis');
            var createLabel = function(val) {
                return $('<span>').text(val).click(function() {
                    _this.val(value = val).change();
                    setArrowPosition();
                })
            };
            for (var i = 0; i < rangerValues.length; i++) {
                var oneCellAxis = $('<div>').addClass('ranger-cell').
                    html(createLabel(rangerValues[i]));
                oneCellAxis.css({
                    'left': i * width + '%',
                    'width': width + '%'
                });
                axisContainer.append(oneCellAxis);
            }
            var startPosition;
            var downArrow = function(e) {
                startPosition = e.clientX;
                rangerArrow.unbind('mousedown', downArrow);
                oldValue = value;
                $(window).bind({
                    'mousemove': moveWindow,
                    'mouseup': endRanger
                });
            };
            var moveWindow = function(e) {
                var rangePosition = e.clientX - startPosition;
                startPosition = e.clientX;
                rangerArrowPosition = rangerArrowPosition + rangePosition / (axisContainer.width() / 100);
                rangerArrowPosition = rangerArrowPosition < 0 ? 0 : (
                    rangerArrowPosition > 100 ? 100 : rangerArrowPosition
                );
                rangerArrow.css({
                    left: rangerArrowPosition + '%'
                });
                setFieldValue();
            };
            var endRanger = function() {
                rangerArrow.bind('mousedown', downArrow);
                $(window).unbind({
                    'mousemove': moveWindow,
                    'mouseup': endRanger
                });
            };
            var setArrowPosition = function() {
                if (value < rangerValues[rangerValues.length - 1]) {
                    for (var i = 0; i < rangerValues.length; i++) {
                        if (value < rangerValues[i]) {
                            var k = i - 1,
                                rangeCells = rangerValues[i] - rangerValues[k],
                                oneDigit = rangeCells / width;
                            rangerArrowPosition = width * k + (value - rangerValues[k]) / oneDigit;
                            break;
                        }
                    }
                } else {
                    rangerArrowPosition = 100;
                }
                rangerArrow.css({
                    left: rangerArrowPosition + '%'
                });
            };
            var setFieldValue = function() {
                var oldValue = value;
                if (rangerArrowPosition > 0 && rangerArrowPosition < 100) {
                    var cellNumber = Math.floor(rangerArrowPosition / width),
                        rangeCells = rangerValues[cellNumber + 1] - rangerValues[cellNumber],
                        oneDigit = rangeCells / width;
                    value = Math.round((rangerArrowPosition - width * cellNumber) * oneDigit + rangerValues[cellNumber]);
                } else {
                    value = rangerArrowPosition == 0 ? rangerValues[0] : rangerValues[rangerValues.length - 1];
                }
                if (miniVariants) {
                    if (value >= 10) {
                        value = Math.ceil(value / 10) * 10;
                    } else if (value > 5) {
                        value = Math.ceil(value / 5) * 5;
                    }
                    _this.unbind('change', changeValue);
                }
                if (oldValue != value)
                    _this.val(value).change();
                _this.bind('change', changeValue)
            };

            var rangerArrow = $('<div>').
                addClass('ranger-arrow').
                bind('mousedown', downArrow);

            _this.after(rangerContainer.append(
                axisContainer.append(rangerArrow)
            ));
            setArrowPosition();
            var changeValue = function() {
                value= _this.val();
                if (miniVariants) {
                    if (value >= 10) {
                        value = Math.ceil(value / 10) * 10;
                    } else if (value > 5) {
                        value = Math.ceil(value / 5) * 5;
                    }
                    _this.unbind('change', changeValue);
                }
                _this.val(value);
                setArrowPosition();
            };
            _this.bind('change', changeValue);
        });
    };







var formsModels = {
    on_premises: function(form, params) {
        var getCountLicenses = function(count, licenses_counts) {
            var result = [],
                fullPrice = 0,
                users = 0;
            for (var i = licenses_counts.length - 1; i >= 0; i--) {
                var ceilLicense = Math.floor(count / licenses_counts[i]['user_count']),
                    thisPrice = ceilLicense * licenses_counts[i]['price_license'];
                result.push({
                    'type-license': licenses_counts[i]['user_count'],
                    'count-license': ceilLicense,
                    'price-license': licenses_counts[i]['price_license'],
                    'full-price': thisPrice
                });
                users+= ceilLicense * licenses_counts[i]['user_count'];
                if (ceilLicense) {
                    count = count % licenses_counts[i]['user_count'];
                    fullPrice+= thisPrice;
                }
            }
            return {
                'price': fullPrice,
                'licenses': result,
                'count': users
            };
        };
        var countUsersBlock = $('.count-users', form),
            countLicenses,
            fields = {};
        var licensesPriceBlock = $('.total-license-price', form);
        var inputs = $('input', form).keypress(function(e) {
            if (e.which == 13) {
                e.preventDefault();
                $(this).change();
                return false;
            }
        }).each(function() {
            var _this = $(this);
            fields[_this.attr('name')] = _this;
        });

        var rangerParams = [];
        for (var i = 0; i < rangersValues.length; i++ ) {
            rangerParams.push(rangersValues[i]['user_count']);
        }
        if (fields['high_load']) {
            var oldHighLoad = fields['high_load'].is(':checked');
            fields['high_load'].change(function() {
                oldHighLoad = fields['high_load'].is(':checked');
            });
        }
        var setLicensesPrice = function() {
            var countComponents = 0;
            var fullPrice = 0;
            for (var i in fields) {
                if (params['prices'][i] && fields[i].is(':checked')) {
                    if (i != 'high_load') {
                        fullPrice+= params['prices'][i];
                        countComponents++;
                    } else {
                        fullPrice+= params['prices'][i];
                    }
                }
            }
            fullPrice+= countLicenses['price'] * countComponents;
            fullPrice = Math.round((fullPrice / _CUR) * 100) / 100;
            licensesPriceBlock.text((fullPrice + '').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + ' EUR');
        };
        var returnCheck = function(e) {
            e.preventDefault();
            $(e['target']).attr('checked', 'checked').change();
        };
        $('.ranger', form).iniRangers(rangerParams, true).change(function() {
            var val = $(this).val();
            countLicenses = getCountLicenses(val - 1, params['license-counts']);
            countUsersBlock.text(countLicenses['count'] + 1);
            setLicensesPrice();
            val >= highLoadValue ?
                fields['high_load'].attr({
                    'checked': 'checked'
                }).bind('click', returnCheck): fields['high_load'].attr({
                'checked': oldHighLoad ? 'checked' : false
            }).unbind('click', returnCheck);
        }).change();
        inputs.change(setLicensesPrice);
    },
    'saas': function(form) {

        var _ERP_COMPONENT = 'component_erp';
        var _OTHER_COMPONENTS = [
            'component_accounting', 'component_trade', 'component_payroll_hr'
        ];
        var _PRICE_ERP = 60;
        var _PRICE_OTH = 35;


        var fields = {};
        var inputs = $('input', form).keypress(function(e) {
            if (e.which == 13) {
                e.preventDefault();
                $(this).change();
                return false;
            }
        }).change(function() {
                setPriceToContent();
            }).each(function() {
            var _this = $(this);
            fields[_this.attr('name')] = _this;
        });
        var getPrice = function() {
            if (fields[_ERP_COMPONENT].is(':checked')) {
                return fields['count_months'].val() * _PRICE_ERP * fields['count_users'].val();
            } else {
                var price = 0;
                for (var i = 0; i < _OTHER_COMPONENTS.length; i++) {
                    if (fields[_OTHER_COMPONENTS[i]].is(':checked')) {
                        price+= fields['count_months'].val() * _PRICE_OTH * fields['count_users'].val();
                    }
                }
                return price;
            }

        };

        var countUsersBlock = $('.count-users', form),
            countMonthsBlock = $('.count-months', form),
            licensesPriceBlock = $('.total-license-price', form);

        var setPriceToContent = function() {
            var price = getPrice();

            countMonthsBlock.text(fields['count_months'].val());
            countUsersBlock.text(fields['count_users'].val());
            licensesPriceBlock.text((price + '').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + ' EUR');
        };

        var togetherComponent = inputs.filter('[data-together-component]').change(function() {
            togetherComponent.is(':checked') ?
                separateComponents.attr('disabled', 'disabled') :
                separateComponents.removeAttr('disabled');
        });
        var separateComponents = inputs.filter('[data-separate-component]').change(function() {
            var _this = $(this);
            if (_this.is(':checked')) {
                togetherComponent.attr('disabled', 'disabled');
            } else if (!_this.is(':checked')) {
                if (!separateComponents.filter(':checked').length) {
                    togetherComponent.removeAttr('disabled');
                }
            }
        });
        var rangerParams = [];
        for (var i = 0; i < rangersValues.length; i++ ) {
            rangerParams.push(rangersValues[i]['user_count']);
        }
        $('.ranger', form).iniRangers(rangerParams);
    }
};


    $(function() {
        var forms = {};
        $('form.calculate-form').each(function() {
            var _this = $(this),
                nameForm = _this.attr('data-form-name') || false;
            if (nameForm && formsModels[nameForm]) {
                forms[nameForm] = new formsModels[nameForm](
                    $(this), {
                    'license-counts': rangersValues,
                    'prices': modulesPrice[nameForm] || false
                });
            }
        });
    });

})(jQuery);
