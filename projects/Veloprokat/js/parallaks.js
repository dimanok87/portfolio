(function($) {	
	$.fn.parallaks = function(options) {
		return $(this).each(function() {
			var _this = $(this);
			_this.height(100 + options.outset + "%")
			var iniParalaks = function() {
				var scroll = $(window).scrollTop();
				var scrollPercent = scroll / (document.body.offsetHeight - $(window).height());
				_this.css({
					top: -options.outset*scrollPercent + "%"
				});
			}
			$(window).bind("scroll", iniParalaks);
		});
	};
	$.fn.bgSizing = function() {
		return $(this).each(function() {
			var _this = $(this);
			var moveAttr = _this.attr("data-move") || false;
			var win = _this.parent();
			var iniHeight = function() {
				var winWidth = win.width(), winHeight = win.height();
				_this.css({width: 'auto', height: 'auto'});
				var widthK = winWidth/_this.width(), heightK = winHeight/_this.height();
				
				if ((widthK > 1 && widthK < heightK) || (widthK < heightK)) {
					var cssAttr = {
						top: 0
					};
					_this.css({height: '100%'});					
					if (!moveAttr) {
						cssAttr["left"] = -(_this.width() - winWidth) / 2;
					} else {
						cssAttr[moveAttr] = -(_this.width() - winWidth);
					}
					_this.css(cssAttr);
				} else {
					_this.css({width: '100%'});
					_this.css({left: 0, top: - (_this.height() - winHeight) / 2
					});
				}
			};
			_this.load(function() {
				$(window).bind("resize", iniHeight);
				iniHeight();
			});
		});
	};
	
	
})(jQuery)
