(function($) {
    $(function () {
        //--- Инициализация карты ---
        var myMap;

        function init() {
            myMap = new ymaps.Map("map", {
                center: [51.469462, 46.127987],
                zoom: 16,
                controls: []
            });
            myMap.behaviors.disable('scrollZoom');
            var myPlacemark = new ymaps.Placemark([51.469462, 46.127987]);
            myMap.geoObjects.add(myPlacemark);
        }

        //--- / Инициализация карты ---
        ymaps.ready(init);



        var menu = $('.main-menu:first');
        var menuLinks = $('a', menu);
        var anchors = $('a.anchor');

        var isClick = false;
        menuLinks.each(function() {
            var _th = $(this);
            var selectorAnchor = _th.attr('href');
            var thAnchor = $(selectorAnchor);
            if (!thAnchor.length) return;
            _th.data('anchor', thAnchor.length ? thAnchor : false);
            _th.on('click', function(e) {
                e.preventDefault();
                if (oldActiveItem && _th.is(oldActiveItem)) return;
                oldActiveItem ? oldActiveItem.removeClass('active') : false;
                oldActiveItem = _th;
                isClick = true;
                _th.addClass('active');
                $({top: $(window).scrollTop()}).animate({top: thAnchor.offset()["top"]}, {
                    duration: 1000,
                    step: function() {
                        $(window).scrollTop(this.top);
                    },
                    complete: function() {
                        isClick = false;
                    }
                });
            });
        });

        var header = $('header:first');
        var win = $(window);

        var oldActiveItem = false;
        var serchAnchors = function() {
            var winHeight = win.height() / 2;
            var activeItem = false;
            var winScroll = win.scrollTop();
            menuLinks.each(function() {
                var _th = $(this);
                var anch = _th.data('anchor');
                if (!anch) return;
                var offsetTop = anch.offset()['top'];
                if (offsetTop < winScroll && (!activeItem || activeItem['offset'] < offsetTop) || (offsetTop - winScroll < winHeight)) {
                    activeItem = {
                        offset: offsetTop,
                        item: _th
                    };
                }
            });
            oldActiveItem ? oldActiveItem.removeClass('active') : false;
            oldActiveItem = activeItem ? activeItem.item.addClass('active') : false;
        };
        serchAnchors();

        var scrollTimeout = false;
        win.on('scroll', function() {
            if (header.offset()['top'] + header.outerHeight() < win.scrollTop()) {
                menu.addClass('fixed');
            } else {
                menu.removeClass('fixed')
            }
            scrollTimeout ? clearTimeout(scrollTimeout) : false;
            if (isClick) return;
            scrollTimeout = setTimeout(serchAnchors, 100);
        });


        /* Инициализация каталога */
        var catalog = $('#catalog');
        var categories =  $('.one-category', catalog);
        var openedCategory = false;

        var openCategory = function(cat) {
            resetShowDescription();
            if (!cat.is(openedCategory)) {
                deActivateSubCategory();
                if (!openedCategory) {
                    catalog.addClass('opened-category');
                } else {
                    openedCategory.removeClass('is-opened-category');
                }
                openedCategory = cat.addClass('is-opened-category');
            }
        };

        var activeSubCategory = false;

        var deActivateSubCategory = function() {
            if (activeSubCategory) {
                activeSubCategory['lnk'].removeClass('active');
                activeSubCategory['list'].removeClass('is-opened');
                activeSubCategory['cat'] ? activeSubCategory['cat'].removeClass('is-subcat-opened') : false;
                activeSubCategory = false;
            }
        };
        var activateSubCategory = function(lnk, productsList, cat) {
            if (activeSubCategory) {
                if (lnk.is(activeSubCategory['lnk'])) {
                    return;
                }
                deActivateSubCategory();
            }
            activeSubCategory = {
                lnk: lnk.addClass('active'),
                list: productsList.addClass('is-opened'),
                cat: cat.addClass('is-subcat-opened')
            };
        };

        var resetShowDescription = function() {
            if (showedDescription) {
                showedDescription['item'].removeClass('is-show-desc');
                showedDescription['list'].not(showedDescription['item']).show();
                showedDescription = false;
            }
        };
        var showedDescription = false;

        categories.each(function() {
            var _this = $(this);
            var nameCategory = $('.category-name:first', _this);
            var subCategoriesList = $('.categories-list:first', _this);
            var productsLists = $('.products-categories-list', _this);
            var bgImage = $('.image-container:first img', _this);

            var productsItems = $('li', productsLists).each(function() {
                var _th = $(this);
                var lnk = $('a:first', _th);
                var desc = $('p:first', _th);
                lnk.on('click', function(e) {
                    e.preventDefault();
                    productsItems.not(_th).hide();
                    _th.addClass('is-show-desc');
                    showedDescription = {
                        item: _th,
                        list: productsItems
                    };
                    return false;
                });
            });

            _this.on('mouseenter', function() {
                bgImage.stop().animate({
                    opacity: 0
                }, function() {
                    bgImage.hide();
                });
            }).on('mouseleave', function() {
                bgImage.stop().show();
                bgImage.animate({
                    opacity: 1
                });
            });
            nameCategory.on('click', function(e) {
                e.preventDefault();
                openCategory(_this);
                return false;
            });

            $('a', subCategoriesList).each(function() {
                var lnk = $(this);
                var productsList = productsLists.filter('[data-list-index="' + lnk.attr('data-item-index') + '"]');
                lnk.on('click', function(e) {
                    e.preventDefault();
                    openCategory(_this);
                    activateSubCategory(lnk, productsList, _this);
                    return false;
                });
            });
        });
    })
})(jQuery);