var appContainer = false;
var loaderMask = jQuery('<div>').addClass('loader-mask').css({opacity: 0});

function showLoader() {
    loaderMask.appendTo(appContainer);
    loaderMask.stop().animate({
        opacity: 1
    });
}

function hideLoader() {
    loaderMask.stop().animate({
        opacity: 0
    }, function() {
        loaderMask.detach();
    });
}

(function($) {

    screenError = function(params) {

        var screenItem = $('.pages-item:first');
        var errorsContainer = screenItem.data('errors-block') || $('<div>').addClass('errors-container');
        screenItem.append(errorsContainer).data('errors-block', errorsContainer);

        var removeError = function() {
            errorDIV.slideUp('fast', function() {
                errorDIV.detach().empty().remove();
            });
        };

        var errorDIV = $('<div>').addClass('error-screen').hide();
        var buttonsContainer = $('<div>').addClass('error-screen-btns');
        var closeButton = $('<div>').addClass('error-screen-btn error-screen-close').on('click', removeError);
        var errorBtn = $('<span>').addClass('error-screen-btn');

        params = params || {};
        params['buttons'] = params['buttons'] || [];

        errorDIV.text(params['text'] || 'Error');
        errorDIV.prepend(buttonsContainer);

        var createButton = function(btn) {
            btn['classes'] = btn['classes'] || '';
            errorBtn.clone().on('click', function() {
                btn['action'] ? btn['action']() : false;
                removeError();
            }).text(btn['text']).addClass(btn['classes']).appendTo(buttonsContainer);
        };
        if (params['buttons'].length) {
            for (var i = 0; i < params['buttons'].length; i++) {
                createButton(params['buttons'][i]);
            }
        } else {
            closeButton.appendTo(buttonsContainer);
        }
        errorDIV.appendTo(errorsContainer);
        errorDIV.slideDown('fast');
    };

    $(function() {
        appContainer = $('.app-container');
    });
})(jQuery);