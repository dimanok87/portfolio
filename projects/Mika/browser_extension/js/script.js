(function($) {
    $(function() {
        var startPage = $('#first-page');
        var yandexTasks = $('#yandex-tasks');
        var settingsPage = $('#settings-page');

        var okPageButtonContent = $('<img src="images/svg/b-ok.svg" alt="">');
        var plusButtonContent = $('<img src="images/svg/b-plus.svg" alt="">');




        var toPage = function(page, params) {
            page ? showContentBlock(page, params) : toBackContent({to: 'right'});

            resetScrolledContent(page);
            iniScrollContent(page);
            $('textarea', page).autoTextarea();

            if (settingsActive) {
                settBtn.removeClass('active');
                settingsActive = false;
                $('#settings-screen').on('click', toSettings).off('click', toBack);
            }
        };

        var settingsActive = false;

        var toBack = function() {
            toPage();
        };

        var showStartPage = function() {
            toPage(startPage, {
                to: 'right',
                buttonContent: plusButtonContent,
                buttonAction: showTaskForm
            });
        };

        var showTaskForm = function() {
            toPage(yandexTasks, {
                buttonContent: okPageButtonContent,
                buttonAction: showStartPage
            });
        };

        $('.tasks-table .table-row').on('click', function(e) {
            if ($(e.target).is('a')) return;
            showTaskForm();
        });

        $('#to-start-page, #logo-app').on('click', function() {
            showStartPage();
        });



        var showStaffSearch = function() {
            settingsScreenParams = {
                buttonContent: okPageButtonContent,
                buttonAction: showStaffList
            };
            showScreenVariant(settingsPage, 'staff-search');
            setActionButtonContent(okPageButtonContent);
            setActionButton(function() {
                showStaffList();
            });
        };
        var showStaffList = function() {
            settingsScreenParams = {
                buttonContent: plusButtonContent,
                buttonAction: showStaffSearch
            };
            setActionButtonContent(plusButtonContent);
            showScreenVariant(settingsPage, 'staff-list');
            setActionButton(function() {
                showStaffSearch();
            });
        };
        var settingsScreenParams = {
            buttonContent: plusButtonContent,
            buttonAction: showStaffSearch
        };
        var toSettings = function() {
            toPage(settingsPage, settingsScreenParams);
            settBtn.addClass('active');
            settingsActive = true;
            $('#settings-screen').off('click', toSettings).on('click', toBack);
        };

        var settBtn = $('#settings-screen').on('click', toSettings);

        var resetScrolledContent = function(scrolledBlock) {
            var scrolledMenu = $('.app-menu-container:first', scrolledBlock);
            var scrolledContent = $('.app-content-container', scrolledBlock);
            scrolledContent.css({
                'top': scrolledMenu.outerHeight()
            });
            scrolledMenu.css({
                'top': 0
            });
            scrolledContent.off('scroll');
        };

        var iniScrollContent = function(scrolledBlock) {
            var scrollTop = 0,
                scrolledMenu = $('.app-menu-container:first', scrolledBlock),
                menuHideLine = $('.app-menu-line:first', scrolledMenu),
                menuHideLineHeight = menuHideLine.outerHeight(),
                animated = false,
                hidedTop = 0,
                toHide,
                timeout = false;

            var otherLinesMenu = $('.app-menu-line:not(:first)', scrolledMenu);
            if (!otherLinesMenu.length) {
                return;
            }
            otherLinesMenu.each(function() {
                hidedTop+= $(this).outerHeight();
            });

            var scrolledContent = $('.app-content-container', scrolledBlock);
            scrolledContent.on('scroll', function() {
                var thScroll = scrolledContent.scrollTop();
                var toThHide = thScroll > scrollTop ? 'top' : 'bottom';
                if (animated || toThHide == toHide || thScroll == scrollTop) {
                    scrollTop = thScroll;
                    return;
                }
                toHide = thScroll > scrollTop ? 'top' : 'bottom';
                timeout ? clearTimeout(timeout) : false;
                scrolledContent.stop().animate({
                    'top': hidedTop + (thScroll > scrollTop ? 0 : menuHideLineHeight)
                }, 200, 'linear', function() {
                    timeout = setTimeout(function() {
                        animated = false;
                    }, 100);
                });
                scrolledMenu.stop().animate({
                    'top': thScroll > scrollTop ? -menuHideLineHeight : 0
                }, 200, 'linear');
                animated = true;

                scrollTop = thScroll;
            });
        };

        showStartPage();

        $('[data-picker-label]').datepickerExt();
        $('textarea').autoTextarea();
        $('.chosen-select').iniStaff();
        $('.chosen-select-nosearch').iniSingleSelect();
        $('.search-staff-container').iniFilter({
            'field': '.search-input',
            'element': '.user-row',
            'empty': '.no-filter-result',
            'text': '.user-name-text'
        });
    })
})(jQuery);