(function($) {
    $.datepicker.regional['default'] = {
        closeText: 'Done',
        prevText: '',
        nextText: '',
        currentText: 'Today',
        monthNames: ['January','February','March','April','May','June',
            'July','August','September','October','November','December'],
        monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
        dayNamesShort: ['S','M','T','W','T','F','S'],
        dayNamesMin: ['S','M','T','W','T','F','S'],
        weekHeader: 'Wk',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        showOtherMonths: true,
        yearSuffix: ''};

    $.datepicker.setDefaults($.datepicker.regional['default']);
    var resetBtn = $('<div class="reset-btn-container"><span class="link link-opacity">Not chosen</span></div>');
    $.fn.datepickerExt = function() {
        return $(this).each(function() {
            var _th = $(this);
            var calendarField = _th.next('[data-picker]');
            var parentTableContent = _th.parents('.content-table:first');
            calendarField.datepicker({
                onSelect: function() {
                    calendarField.removeClass('hidden-field');
                    _th.hide();
                },
                minDate: new Date(),
                beforeShow: function() {
                    setTimeout(function() {
                        widget.append(resetBtn.clone(true).on('mousedown', function() {
                            calendarField.val('').datepicker('refresh');
                            _th.show();
                        }));
                        widget.appendTo(parentTableContent);
                        widget.css({
                            top: calendarField.offset()['top'] + calendarField.outerHeight() - parentTableContent.offset()['top']
                        });

                    }, 0);
                },
                selectOtherMonths: true
            });
            var widget = $(this).datepicker( "widget" );
        })
    };
}(jQuery));