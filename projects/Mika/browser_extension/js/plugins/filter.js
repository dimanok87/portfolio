(function($) {
    $.fn.iniFilter = function(options) {
        return $(this).each(function() {
            var _this = $(this);
            var input =  $(options['field'], _this);
            var elements = $(options['element'], _this);
            var noresult = $(options['empty'], _this);


            input.on('keyup', function() {
                var search = input.val();
                var regexp = new RegExp(search, 'im');
                var showedResults = [];
                elements.each(function() {
                    var _th = $(this);
                    var textBlock = $(options['text'], _th);
                    if (regexp.test(textBlock.text())) {
                        showedResults.push(_th.show());
                    } else {
                        _th.hide();
                    }
                });

                if (!showedResults.length) {
                    noresult.show();
                } else {
                    noresult.hide();
                }
            });
        });
    }
})(jQuery);