(function($) {
    $.fn.autoTextarea = function() {
        return $(this).each(function() {
            var _this = $(this);

            var height = 0;
            var createClone = function() {
                var parent = _this.parent().css({
                    position: 'relative',
                    overflow: 'hidden'
                });
                var cloneTextarea = $('<span>').addClass('textarea').css({
                    position: 'absolute',
                    visibility: 'hidden',
                    zIndex: 1,
                    width: '100%'
                }).appendTo(parent);
                _this.css({
                    position: 'relative',
                    zIndex: 2
                });
                return cloneTextarea;
            };
            var onchaneHeight = function() {
                var cloneTextarea = _this.data('oninitautoresize');
                cloneTextarea.text(_this.val());
                if (height != cloneTextarea.outerHeight()) {
                    height = cloneTextarea.outerHeight();
                    _this.stop().animate({
                        minHeight: height
                    }, 100);
                }
            };
            if (!_this.data('oninitautoresize')) {
                _this.on('keyup keypress keydown', onchaneHeight);
                _this.data('oninitautoresize', createClone());
                onchaneHeight();
            } else {
                onchaneHeight();
            }
        });
    };
})(jQuery);