(function($) {
    var createRadioStaff = function(params) {
        var item = $('<div class="staff-item">' +
            '<label class="radio-button">' +
            '<input type="radio" name="' + params['name'] + '" value="' + params['value'] + '">' +
            '<span>' + params['text'] + '</span>' +
            '</label>' +
            '<svg class="delete-staff" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="10px" viewBox="0 0 10 10" enable-background="new 0 0 10 10" xml:space="preserve">' +
            '<line fill="none" stroke="#8A8D99" stroke-miterlimit="10" x1="1" y1="1" x2="9" y2="9"></line>' +
            '<line fill="none" stroke="#8A8D99" stroke-miterlimit="10" x1="1" y1="9" x2="9" y2="1"></line>' +
            '</svg>' +
            '</div>');
        $('.delete-staff:first', item).on('click', function(e) {
            e.preventDefault();
            params['onClose']();
            return false;
        });
        return item;
    };

    $.fn.iniStaff = function() {
        return $(this).each(function() {
            var _this = $(this);
            var opts = $('option', _this);
            var parentsRow = _this.parents('.table-row:first');
            var parentCell = _this.parents('.table-cell:first');
            var radioContainer = $('.radio-staff-container:first', parentCell);
            var notChoseButton = $('<label class="chose-button link svg-link link-opacity">not chosen</label>');
            var name = _this.attr('name');
            var radioBtns = {};
            _this.chosen({});
            var input = $('.search-field input:first', parentCell).appendTo(radioContainer);
            radioContainer.append(input);
            input.on('focus', function() {
                notChoseButton.hide();
            }).on('blur', function() {
                notChoseButton.show();
            });
            var parentScreenHolder = _this.parents('.app-content-container:first');
            var chosenBlock = $('.chosen-container:first', parentCell);
            var chosenDrop = $('.chosen-drop:first', chosenBlock);
            _this.on('chosen:showing_dropdown', function() {
                parentsRow.css({
                    position: 'relative',
                    zIndex: 2
                });
                if (parentScreenHolder.offset()['top'] + parentScreenHolder.height() < chosenDrop.outerHeight() + chosenDrop.offset()['top']) {
                    chosenDrop.prependTo(chosenBlock.addClass('top-position'));
                }
            });
            _this.on('chosen:hiding_dropdown', function() {
                parentsRow.css({
                    position: '',
                    zIndex: ''
                });
                chosenDrop.appendTo(chosenBlock.removeClass('top-position'));
            });
            var oldVal = true;
            var changeValue = function() {
                var v = _this.val();
                if (v !== oldVal) {
                    if (!v && oldVal) {
                        input.after(notChoseButton);
                    } else if (v && !oldVal) {
                        notChoseButton.detach();
                    }
                    oldVal = v;
                }
            };
            var id = 'chosen-search-' + name;
            input.attr('id', id).addClass('label');
            $('<label>').appendTo(parentCell).addClass('chose-label').attr({'for': id});
            notChoseButton.attr({'for': id});
            _this.on('change', function(e, obj) {
                if (obj['selected']) {
                    var option = opts.filter('[value="' + obj['selected'] + '"]');
                    if (!radioBtns[obj['selected']]) {
                        radioBtns[obj['selected']] = createRadioStaff({
                            text: option.text(),
                            name: name + '-radio',
                            value: option.attr('value'),
                            onClose: function() {
                                option.removeAttr('selected');
                                _this.trigger('chosen:updated');
                                radioBtns[obj['selected']].empty().remove();
                                radioBtns[obj['selected']] = false;
                                changeValue();
                            }
                        });
                        radioBtns[obj['selected']].on('mouseup', function(e) {
                            return false;
                        })
                    }
                    input.before(radioBtns[obj['selected']]);
                } else if (obj['deselected']) {
                    if (radioBtns[obj['deselected']]) {
                        radioBtns[obj['deselected']].empty().remove();
                        radioBtns[obj['deselected']] = false;
                    }
                }
                changeValue();
            });
            changeValue();
        });
    };
})(jQuery);