(function($) {
    $.fn.iniSingleSelect = function() {
        return $(this).each(function() {
            var _this = $(this);
            var parentsRow = _this.parents('.table-row:first');
            var parentCell = _this.parents('.select-holder:first');
            var parentScreenHolder = _this.parents('.app-content-container:first');
            _this.chosen({disable_search_threshold: 10});

            var chosenBlock = $('.chosen-container:first', parentCell).on('mouseup click ', function() {
                return false;
            });
            var chosenDrop = $('.chosen-drop:first', chosenBlock);
            var catName = _this.attr('data-for-category');

            var categoryBlock = false;
            if (catName) {
                categoryBlock = $('[data-category="' + catName + '"]', parentsRow);
            }

            var onChangeSelect = function() {
                var val = $('option:selected', _this).attr('value');
                if (val === 'null') {
                    chosenBlock.addClass('no-active-variant');
                    categoryBlock ? categoryBlock.removeClass('active') : false;
                } else {
                    chosenBlock.removeClass('no-active-variant');
                    categoryBlock ? categoryBlock.addClass('active') : false;
                }
            };
            _this.on('chosen:showing_dropdown', function() {
                parentsRow.css({
                    position: 'relative',
                    zIndex: 2
                });
                if (parentScreenHolder.offset()['top'] + parentScreenHolder.height() < chosenDrop.outerHeight() + chosenDrop.offset()['top']) {
                    chosenDrop.prependTo(chosenBlock.addClass('top-position'));
                    var minOffset = parentScreenHolder.offset()['top'];
                    var offset = chosenDrop.offset()['top'];
                    if (offset < minOffset) {
                        chosenDrop.css({'marginBottom': offset - minOffset + chosenBlock.height()});
                    }
                }
            });
            _this.on('chosen:hiding_dropdown', function() {
                parentsRow.css({
                    position: '',
                    zIndex: ''
                });
                chosenDrop.appendTo(chosenBlock.removeClass('top-position')).css({'marginBottom': ''});
            });
            _this.on('change', onChangeSelect);
            onChangeSelect();
        });
    };
})(jQuery);