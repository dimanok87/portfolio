(function($) {
    var mask = $('<div>').css({ position: 'absolute', zIndex: 999, left: 0, right: 0, top: 0, bottom: 0, cursor: 'e-resize'});
    $.fn.scroller = function() {
        return $(this).each(function() {

            if ($(this).data('scrolledMenu')) {
                $(this).data('scrolledMenu').reIniWidth();
                return;
            }

            var menu = new (function(list) {
                var marginLeft = 0;
                var startMovePosition = 0;
                var iniDown = function(e) {
                    e.preventDefault();
                    list.off('mousedown', iniDown);
                    list.on({'mouseup': breakDown, 'mousemove': iniScroll});
                    return false;
                };
                var iniScroll = function(e) {
                    e.preventDefault();
                    startMovePosition = e.clientX - marginLeft;
                    list.off({'mouseup': breakDown, 'mousemove': iniScroll});
                    $(window).on({'mousemove': moveList, 'mouseup': endMove});
                    mask.appendTo($('body:first'));
                };
                /*var toMargin = function() {
                 list.css({marginLeft: marginLeft});
                 };*/
                var moveList = function(e) {
                    e.preventDefault();
                    marginLeft = e.clientX - startMovePosition;
                    toEndMargin(true);
                };
                var breakDown = function() {
                    list.off({'mouseup': breakDown, 'mousemove': iniScroll});
                    list.on('mousedown', iniDown);
                };
                var toEndMargin = function(inCss) {
                    marginLeft = Math.min(0, Math.max(container.width() - width - 5, marginLeft));
                    list[inCss ? 'css' : 'animate']({marginLeft: marginLeft});
                };
                var endMove = function() {
                    $(window).off({'mousemove': moveList, 'mouseup': endMove});
                    list.on('mousedown', iniDown);
                    toEndMargin();
                    mask.detach();
                };

                // list.on('mousedown', iniDown);
                list.on('mousewheel DOMMouseScroll', function(e) {
                    var event = e.originalEvent;
                    marginLeft-= event.deltaY ? event.deltaY / 2 : (event.wheelDelta ? -event.wheelDelta : (event.detail * 3));
                    toEndMargin(true);
                });

                var container = list.parent();
                var width = 0;
                this.reIniWidth = function() {
                    $('li', list).each(function() {
                        width+= $(this).outerWidth(true);
                    });
                    list.width(width + 10);
                    marginLeft = 0;
                    toEndMargin(true);
                };
                this.reIniWidth();
            })($(this));

            $(this).data('scrolledMenu', menu)

        });
    };
})(jQuery);