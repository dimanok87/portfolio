var contentsList = false;
var showedContent = false;
var showedBlock = false;
var oldContent = false;
var actionButton;


var showScreenVariant = function(block, variant) {
    var variantedBlock = $('[data-screen-variant="' + variant + '"]:first', block);
    var showedBlock = block.data('showed-screen') || false;
    if (!showedBlock) {
        showedBlock = $('[data-screen-variant]', block).not(':hidden').first();
        block.data('showed-screen', showedBlock);
    }
    if (showedBlock && showedBlock.is(variantedBlock)) return;

    if (showedBlock) {
        showedBlock.slideUp('fast');
        block.data('showed-screen', variantedBlock.slideDown('fast'));
    } else {
        block.data('showed-screen', variantedBlock.show());
    }
};

var setActionButtonContent = function(icon) {
    if (showedBlock['params'] && showedBlock['params']['buttonContent']) {
        showedBlock['params']['buttonContent'] = icon;
    }
    actionButton.html(icon);
};

var setActionButton = function(action) {
    if (showedBlock['params'] && showedBlock['params']['buttonAction']) {
        actionButton.off('click', showedBlock['params']['buttonAction']);
        showedBlock['params']['buttonAction'] = action;
    }
    actionButton.on('click', action);
};

var showContentBlock = function(block, params) {
    if (block.is(showedBlock['block']) || !contentsList) return;

    params = params || {};
    params['to'] = params['to'] || 'left';


    if (showedBlock && showedBlock['params'] && showedBlock['params']['buttonAction']) {
        actionButton.off('click', showedBlock['params']['buttonAction']);
    }

    params['buttonContent'] && params['buttonAction'] ?
        actionButton.show().html(params['buttonContent']).on('click', params['buttonAction']) :
        actionButton.hide();


    var container = $('<div>').addClass('pages-item');

    contentsList.width('200%');
    container.width('50%');

    showedContent ? showedContent.width('50%') : false;
    var menu = $('.app-menu-list', block);
    switch(params['to']) {
        case 'left':

            contentsList.append(container.append(block.show()));
            oldContent = showedBlock;
            showedBlock = {block: block, params: params};
            if (!menu.is('[data-static]')) {
                $('.app-menu-list', block).scroller();
            }
            if (!showedContent) {
                showedContent = container;
                return;
            }
            contentsList.animate({'margin-left': '-100%'}, 200, 'linear', function() {
                    showedContent.detach();
                    contentsList.css({'margin-left': 0});
                    showedContent = container;
                    contentsList.width('100%');
                    container.width('100%');
                });
            break;
        default:

            contentsList.prepend(container.append(block.show()));
            oldContent = showedBlock;
            showedBlock = {block: block, params: params};
            if (!menu.is('[data-static]')) {
                $('.app-menu-list', block).scroller();
            }
            if (!showedContent) {
                showedContent = container;
                return;
            }
            contentsList.
                css({'margin-left': '-100%'}).
                animate({'margin-left': 0}, 200, 'linear', function() {
                    showedContent.detach();
                    showedContent = container;
                    contentsList.width('100%');
                    container.width('100%');
                });
            break;
    }
};

var toBackContent = function(params) {
    showContentBlock(oldContent['block'], $.extend(oldContent['params'], params));
};

(function($) {
    $(function() {
        contentsList = $('.pages-list-container:first');
        actionButton = $('#circle-red-button');
    });
})(jQuery);