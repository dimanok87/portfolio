(function($) {
    $.fn.iniQuests = function() {
        return $(this).each(function() {
            var list = $(this);
            $('li', list).each(function() {
                var li = $(this);
                var quest = $('[data-quest]', li);
                var answer = $('[data-answer]', li);
                quest.on('click', function() {
                    answer.toggle();
                });
            });
        });
    };
    $.fn.dslider = function(options) {
        options = $.extend({
            'list-ul': '.slider-list',
            'list-li': '.slider-item',

            'after-show': false,
            'after-slide': false,
            'after-hide': false,

            'interval': 2000,
            'duration': 300

        }, options);

        var _this = $(this);
        var list = $(options['list-ul'], _this);
        var items = $(options['list-li'], list);
        list.width(items.length * 100 + '%');
        items.width(100 / items.length + '%');

        var activeSlide = 0;

        var toSlide = function(n) {
            var newVisibleIndex = (n > items.length - 1) ? 0 : (n < 0 ? items.length - 1 : n);
            var oldSlide = items.eq(activeSlide);
            var newSlide = items.eq(activeSlide = newVisibleIndex);
            oldSlide.after(newSlide.css({display: 'block'}));

            setTimeout(function() {
                options['after-show'] ?
                    options['after-show'](newSlide) :
                    false;
            }, 0);


            list.animate({
                marginLeft: '-100%'
            }, options['duration'], function() {
                oldSlide.hide();
                options['after-hide'] ?
                    options['after-hide'](oldSlide) :
                    false;
                list.css({
                    marginLeft: 0
                });
                options['after-slide'] ?
                    options['after-slide'](newSlide) :
                    false;
            });
        };

        var toNextButton = $('<span>').addClass('slider-nav-btn next-btn').on('click', function() {
            toSlide(activeSlide + 1);
        }).html('&#8250;');
        var toPrevButton = $('<span>').addClass('slider-nav-btn prev-btn').on('click', function() {
            toSlide(activeSlide - 1);
        }).html('&#8249;');
        _this.append(toNextButton, toPrevButton);

        /*setInterval(function() {
            toSlide(activeSlide + 1);
        }, options['interval']);*/

        options['after-show'] ? options['after-show'](items.eq(activeSlide)) : false;
        options['after-slide'] ? options['after-slide'](items.eq(activeSlide)) : false;
    };


    $(function() {
        $('.quests-list').iniQuests();
        $('.slider-container').dslider();
    })
})(jQuery);