(function($) {
    function initialize() {
        var mapOptions = {
            center: new google.maps.LatLng(-37.8602828, 145.079616),
            zoom: 11,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true
        };
        var map = new google.maps.Map(document.getElementById("contacts-map"), mapOptions);
    }
    $(initialize);
    $(function() {
        var body = $('body:first');
        $(window).on('scroll', function() {
            $(window).scrollTop() > 20 ?
                body.addClass('scrolled-page') :
                body.removeClass('scrolled-page');
        });

        $('#menu-switcher').on('click', function() {
            $(this).toggleClass('active-menu');
        });
    })
})(jQuery);