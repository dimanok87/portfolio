(function($) {

	var price_per_item = {
		'inbound_agents': [828, 1],
		'outbound_agents': [1098, 1],
		'blended_agents': [1098, 1],
		'supervisers': [720, 1],
		'number_peak_time': [1350, 50],
		'ivr': [1200, 50],
		'email_agents': [180, 1],
		'web_agents': [180, 1],
		'quality_management': [180, 0.01, 'ch']
	};
	
	var form_data = {};
	var min_price_block, max_price_block;
	
	var calculateForm = function() {
		var percent = 20,
			system_values = {},
			total = 0;	
		for (var key in price_per_item) {
			var value = price_per_item[key];
			system_values[key] = {
				'name': key, 
				'count': form_data['no_'.key] === true || form_data[key] === true ? 1 : (form_data[key] === undefined ? 0 : form_data[key]) / value[1],
				'price': value[0]
			};
			total+= system_values[key]['total'] = system_values[key]['price'] * system_values[key]['count'];
		}
		
		min_price_block.text((Math.floor(total*(100 - percent)/100) + '').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
		max_price_block.text((Math.floor(total*(100 + percent)/100) + '').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1 "));
		
	};


	$.fn.iniChanger = function(options) {
		
		var wind = $(window),
			start_position = 0,
			max_left = options.parent.width(),
			percent_value = (options.max - options.min) / 100,
			parcent_range = max_left / 100,
			this_position = Math.round(parcent_range * options.value / percent_value);
		
		var mouseDown = function(e) {
			e.preventDefault();
			wind.bind({'mousemove': mouseMove, 'mouseup': mouseUp});
			start_position = e.clientX - this_position;			
		};
		
		var changeValue = function(is_change) {
			this_position = this_position < 0 ? 0 : (this_position > max_left ? max_left : this_position);
			_this.css({left: this_position + 'px'});
			options.value = Math.max(options.value, options.min);
			options.input.val(options.value);
			is_change !== false ? options.input.change() : false;
		};
		var mouseMove = function(e) {
			e ? e.preventDefault() : false;
			this_position = Math.min(e.clientX - start_position, max_left);
			options.value = Math.round(percent_value * this_position / parcent_range);
			options.input.unbind('change', inputChange);
			changeValue();
			options.input.bind('change', inputChange);
		};
		
		var mouseUp = function(e) {
			e.preventDefault();
			wind.unbind({'mousemove': mouseMove, 'mouseup': mouseUp});
		};
		
		var inputChange = function() {
			options.value = parseInt($.trim($(this).val()));
			this_position = Math.round(parcent_range * Math.min(options.value, options.max) / percent_value);
			changeValue(false);
		};
		 
		var _this = $(this);
		changeValue();				
		options.parent.mousedown(function(e) {
			e.preventDefault();
			var offset = options.parent.offset().left;
			this_position = e.clientX - offset;
			options.value = Math.round(percent_value * this_position / parcent_range);
			options.input.unbind('change', inputChange);
			changeValue();
			options.input.bind('change', inputChange);
			mouseDown(e);
		});
        options.input.bind('change', inputChange);
	};
	
	$.fn.iniCheckboxs = function() {
		return $(this).each(function(e) {
			var _this = $(this).change(function() {
				_this.is(':checked') ? parent_label.addClass('checked') : parent_label.removeClass('checked');
			});
			var parent_label = _this.parents('label:first').length ? $('<span>') : $('<label>');
			parent_label.is('label') ? parent_label.attr('for', _this.attr('name')) : false;
			parent_label.addClass('checkbox-decor');
			_this.after(parent_label);
			parent_label.append($('<span>').addClass('for-checked'));
			parent_label.append(_this);
		});
	};
	
	$(function() {	
		var calculator = $('#noda-calculator');
		min_price_block = $('#min-price');
		max_price_block = $('#max-price');
		var text_inputs = $('.text-input', calculator).each(function(i) {

			var _this = $(this),
				min_number = _this.attr('data-min'),
				max_number = _this.attr('data-max'),
                label_max_number = _this.attr('data-max-label') || max_number,
				parent = _this.parent(),
				changer = $('<div>').addClass('changer'),
				changer_line = $('<div>').addClass('changer-line').appendTo(changer),
				changer_element = $('<div>').addClass('changer-element').appendTo(changer_line),
				calculator_row = _this.parents('.one-row-calculator:first');

			parent.append(changer);			
			changer.append(
				$('<div>').addClass('limit min').text(min_number),
				$('<div>').addClass('limit max').text(label_max_number)
			);
			
			var checkbox = calculator_row.find('[type="checkbox"]');
			var parent_field = _this.attr('data-parent') || false;
			
			if (checkbox.length || parent_field) {
				parent_field = $('#' + parent_field);
				var activateField = function() {
					form_data[_this.attr('name')] = _this.val();
					mask_row.fadeOut('fast');
				},
				deactivateField = function() {
					form_data[_this.attr('name')] = 0;
					mask_row.fadeIn('fast');
				},
				mask_row = $('<span>').addClass('mask-row').hide();
				calculator_row.append(mask_row);
				checkbox.length ? checkbox.change(function() {
					!checkbox.is(':checked') ? activateField() : deactivateField();
					calculateForm();
				}) : parent_field.change(function() {
					parent_field.val() * 1 ? activateField() : deactivateField();
					calculateForm();
				});
			}
			
			changer_element.iniChanger({
				'input': _this,
				'min': min_number,
				'max': max_number,
				'parent': changer_line,
				'value': _this.val()
			});
		});
		$('input', calculator).each(function() {
			var _this = $(this),
				name = _this.attr('name'),
				is_checkbox = _this.attr('type') == 'checkbox',
				setDataField = function() {
					form_data[name] = is_checkbox ? _this.is(':checked') : _this.val();
				};
			if (price_per_item[name] !== undefined) {
				_this.change(function() {
					setDataField();
					calculateForm();
				});
				setDataField();
			}
		});
		
		$('#id_inbound_agents').change(function() {
			if ($(this).val() == 0) {
				form_data['email_agents'] = 0;
				form_data['web_agents'] = 0;
				calculateForm();
			}
		}); 
		
		$('input[type="checkbox"]').iniCheckboxs().change();
		calculateForm();
	})
})(jQuery);

