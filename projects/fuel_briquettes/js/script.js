var headerHeight = 65;
(function($) {
    $(function() {
        //--- Инициализация карты ---
        ymaps.ready(init);
        var myMap;
        function init(){
            myMap = new ymaps.Map("map", {
                center: [61.800516, 50.750681],
                zoom: 16,
                controls: []
            });
            var myPlacemark = new ymaps.Placemark([61.800016, 50.750681]);
            myMap.geoObjects.add(myPlacemark);
            myMap.behaviors.disable('scrollZoom');
        }
        //--- / Инициализация карты ---

        //--- Инициализация меню ---
        var activeLink = false;
        var activateAnchor = function(anchor, lnk, fl) {
            var activateSelected = function() {
                if (!lnk.is(activeLink)) {
                    activeLink.removeClass("selected");
                    activeLink = lnk.addClass("selected");
                } else {
                    return;
                }
            };
            if (fl || false) {
                activateSelected();
                return;
            }
            $({top: $(window).scrollTop()}).animate({top: anchor.offset()["top"] - headerHeight}, {
                duration: 1000,
                step: function() {
                    $(window).scrollTop(this.top);
                }
            }, function() {
                activateSelected();
            });
        };
        var iniAnchor = function() {
            var _this = $(this);
            var attrName = _this.attr("name");
            var link = $("a[href=#" + attrName + "]").click(function(e) {
                e.preventDefault();
                activateAnchor(_this, $(this));
            });
            _this.data("links", link);
            if (link.is(".selected")) {
                activeLink = link;
            }
        };
        var anchors = $("a[name]").each(iniAnchor);
        //--- / Инициализация меню ---

        var scrollTimeout = false;
        //--- Инициализация скролла ---
        $(window).bind("scroll", function() {
            scrollTimeout ? clearTimeout(scrollTimeout) : false;
            scrollTimeout = setTimeout(function() {
                scrollTimeout = false;
                var windScroll = $(window).scrollTop();
                var oldAnchor = false;
                anchors.each(function() {
                    var _this = $(this);

                    if (_this.offset()["top"] - $(window).height() / 2 >= windScroll) {
                        activateAnchor(oldAnchor || _this, (oldAnchor || _this).data("links"), true);
                        return false;
                    } else {
                        oldAnchor = _this;
                    }
                    activateAnchor(_this, _this.data("links"), true);
                });
            }, 100);
        });
        //--- /Инициализация скролла ---

        //--- PopUp --//
        var pageMask = $("<div>").addClass("popup-background").hide();
        $("body:first").append(pageMask);

        $.fn.iniPopupButton = function() {
            return $(this).each(function() {
                var _this = $(this);
                var openBlock = $('#' + _this.attr('data-for-open'));
                _this.click(function() {
                    pageMask.fadeIn('fast');
                    openBlock.fadeIn('fast');
                    $('section, header, footer').addClass('blur');
                });
                var closePopupButton = $(".close-popup-button", openBlock);
                pageMask.click(function() {
                    pageMask.fadeOut('fast');
                    openBlock.fadeOut('fast');
                    $('section, header, footer').removeClass('blur');
                });
                closePopupButton.click(function() {
                    pageMask.fadeOut('fast');
                    openBlock.fadeOut('fast');
                    $('section, header, footer').removeClass('blur');
                });
            });
        };
        $('[data-for-open]').iniPopupButton();
        //--- PopUp --//




        //--- Slider --//
        var countSlides = 3;
        var dopList = 52;
        $('#articles-news').each(function() {
            var _th = $(this);
            var list = $("ul", _th);
            var items = $(".item-news", list);
            var itemsLength = items.length;
            var maxSteps = itemsLength - countSlides;
            var nextButton = $(".next-button", _th).click(function() {
                toSlide(prevStep + 1);
            });
            var prevButton = $(".prev-button", _th).hide().click(function() {
                toSlide(prevStep - 1);
            });
            var percentsPlus = dopList / _th.width() * 50 * (itemsLength - 1);
            var itemWidth = 100 / itemsLength;
            var listWidth = 100 / countSlides * itemsLength + percentsPlus;
            list.width(listWidth + "%");
            items.width(itemWidth + "%");
            var prevStep = 0;
            var toSlide = function(n) {
                if (n < 0 || n > maxSteps) {
                    return;
                }
                if (n == 0) {
                    prevButton.hide();
                } else if (prevStep == 0) {
                    prevButton.show();
                }
                if (n == maxSteps) {
                    nextButton.hide();
                } else if (prevStep == maxSteps) {
                    nextButton.show();
                }
                prevStep = n;
                list.animate({
                    left: -(listWidth / itemsLength) * prevStep + "%"
                });
            }
        });
        //--- Slider --//
        //--- Валидация формы ---
        var regExps = {
            email: /^[a-zA-Z0-9\-\.\_]+\@[a-zA-Z0-9\-\.\_]+\.[a-zA-Z]{2,4}$/,
            phone: /^\+?[\d]{1,3}\s?\(?[\d]{1,4}\)?\s?[\d\-?]{3,10}$/,
            name: /^[a-zA-Zа-яА-Я]{3,20}$/
        };
        var emptyErrors = {
            email: "Введите e-mail",
            phone: "Введите номер",
            name: "Введите Имя"
        };
        var errorRegexpTexts = {
            email: "Неверный формат e-mail",
            phone: "Проверьте номер",
            name: "Проверьте Имя"
        };
        var validateField = function(inp) {
            var errorBaloon = inp.data("errorBaloon");
            var name = inp.attr("name");
            var regExp = regExps[name];
            var emptyErrorText = emptyErrors[name] || false;
            var formatError = errorRegexpTexts[name] || false;
            var val = inp.val();
            var error = regExp.test(val);
            var errorMsg = false;
            if (val != inp.data("oldValue") || !val.length) {
                if (!val.length) {
                    errorMsg = emptyErrorText;
                } else if (!regExp.test(val)) {
                    errorMsg = formatError;
                }
            }
            if (errorMsg) {
                errorBaloon.text(errorMsg);
                if (!error) {
                    errorBaloon.fadeIn();
                    error = true;
                    inp.data('error', true);
                }
            } else {
                if (error) {
                    errorBaloon.fadeOut();
                    error = false;
                    inp.data('error', false);
                }
            }
            inp.data("oldValue", val);
        };
        var validateFormActivate = function(form) {
            var inputs = $("input", form).each(function() {
                var inp = $(this);
                var errorBaloon = $("<div>").addClass("error-field").hide();
                inp.data("errorBaloon", errorBaloon);
                inp.data("oldValue", "");
                inp.after(errorBaloon);
                inp.bind("blur", function(){
                    validateField($(this));
                });
            });

            form.bind("submit", function(e) {
                e.preventDefault();
                var data = {};
                var errors = false;
                inputs.each(function() {
                    var inp = $(this);
                    validateField(inp);
                    if (inp.data('error')) errors = true;
                    data[inp.attr('name')] = inp.val();
                });
                if (errors) {
                    return;
                }
                $.ajax({
                    url: "sendmail.php",
                    data: data,
                    type: "POST",
                    success: function(data) {
                        openPopup(alertPopup);
                    }
                });
            });
        };
        //--- Валидация формы ---
        $('form').each(function() {
            validateFormActivate($(this));
        });
        var closePopup = function() {
            popupMask.fadeOut(function() {
                popupMask.detach();
            });
            openedPopup.fadeOut(function() {
                openedPopup.detach();
            });
        };
        var openedPopup = false;
        var popupMask = pageMask.clone("false").click(closePopup);
        var openPopup = function(html) {
            popupMask.appendTo("body").fadeIn();
            openedPopup = html.appendTo("body").fadeIn();
        };
        var alertPopup = $('#popup-alert').prepend(
            $('<div>').addClass('close-popup-button').click(closePopup)
        );
        //--- Privacy open ---
    });
    
    
    //---- Вертикальный слайдер ----
    
    $.fn.verticalSlider = function() {
      var activeItem = 0;
      var maxSlides = 2;
      return $(this).each(function() {
        var _this = $(this);
        var list = $('ul', _this); 
        var items = $('li', list);
        var firstItem = items.first();
        var heightItem = firstItem.outerHeight(true);
        var showItem = function(k) {
          k = Math.min(Math.max(k, 0), items.length - maxSlides);
          if (activeItem == k || maxSlides > items.length) return;
          if (k == 0) {
            prevButton.addClass('disabled');
          } else if (!activeItem) {
            prevButton.removeClass('disabled');
          }
          if (k == items.length - maxSlides) {
            nextButton.addClass('disabled');
          } else if (activeItem == items.length - maxSlides) {
            nextButton.removeClass('disabled');
          }
          activeItem = k;
          firstItem.animate({
            marginTop: - heightItem * activeItem
          });
        };
        var prevButton = $('.gallery-button.top-arrow', _this).click(function() {
          showItem(activeItem - 1);
        });
        var nextButton = $('.gallery-button.bottom-arrow', _this).click(function() {
          showItem(activeItem + 1);
        });
        if (items.length <= maxSlides) {
          prevButton.hide();
          nextButton.hide();
        } else {
          showItem(0);
        }
      });
    };
    //--- / Вертикальный слайдер ---

    //--- YouTube player ---

    $.fn.iniYouTube = function() {
        var blockYouTube = $(this);
        var statePlayed = false;
        var initialized = false;
        var control = $('#control');
        var titleVideoBlock = $('#title-video');

        var onPlayed = function() {
            if (initialized) return;
            initialized = true;
            blockYouTube.tubeplayer("pause");
        };
        var opts = {
            width: blockYouTube.width(),
            height: blockYouTube.height(),
            autoPlay: false,
            autoHide: true,
            showinfo: false,
            showControls: false,
            showRelated: false,
            wmode: "transparent",
            preferredQuality: "default",
            modestbranding: false,
            onPlay: function() {
                playButton.hide();
                statePlayed = true;
                $('.play', control).hide();
                $('.pause', control).css({display: 'inline-block'});
            },
            onPause: function() {
                playButton.show();
                statePlayed = false;
                $('.play', control).css({display: 'inline-block'});
                $('.pause', control).hide();
            },
            onPlayerPlaying: onPlayed
        };
        blockYouTube.tubeplayer(opts);
        var playButton = $('#play-button');
        $('#pause-play-button').click(function() {
            blockYouTube.tubeplayer(!statePlayed ? "play" : "pause");
        });
        $('.play', control).click(function() {
            blockYouTube.tubeplayer("play");
        });
        $('.pause', control).click(function() {
            blockYouTube.tubeplayer("pause");
        });
        $('.prev', control).click(function() {
            activateItem(activeItem - 1);
        });
        $('.next', control).click(function() {
            activateItem(activeItem + 1);
        });
        $('.prev-seek', control).click(function() {
            blockYouTube.tubeplayer("seek", blockYouTube.tubeplayer("data")['currentTime'] - 10);
        });
        $('.next-seek', control).click(function() {
            blockYouTube.tubeplayer("seek", blockYouTube.tubeplayer("data")['currentTime'] + 10);
        });
        var activeItem = -1;
        var items = [];
        var activateItem = function(index) {
            if (activeItem == index) return;
            index = index < 0 ? items.length - 1 : (index > items.length - 1 ? 0 : index);
            items[activeItem] ? items[activeItem]['item'].removeClass('opened') : false;
            blockYouTube.tubeplayer("play", {
                id: items[activeItem = index]['key'], time: 0
            });
            items[activeItem]['item'].addClass('opened');
            titleVideoBlock.text($('p', items[activeItem]['item']).text());
        };
        $("#video-list li").each(function(i) {
            var _t = $(this);
            items.push({
                key: _t.attr('data-url-key'),
                item: _t.click(function() {
                    activateItem(i);
                })
            });
        });
        $.tubeplayer.defaults.afterReady = function($player) {
            initialized = false;
            activateItem(0);
            control.removeClass('no-active');
        };
        $(".slides-container a[rel=example_group]").fancybox({
            'transitionIn'		: 'none',
            'transitionOut'		: 'none',
            'titlePosition' 	: 'over',
            'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
                return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
            }
        });
    };

    $.fn.horizontalSlider = function() {
        var _this = $(this);
        var items = $('.one-col-slider', _this);
        var container = $('.slides-container', _this);
        var step = 50; //--- In percents ---
        var marginLeft = 0;
        var iniMargin = function() {
            marginLeft = Math.max(0, Math.min(container.width() - _this.width(), marginLeft));
            marginLeft > 0 ? prevButton.show() : prevButton.hide();
            marginLeft < (container.width() - _this.width()) ? nextButton.show() : nextButton.hide();
            container.animate({
                marginLeft: -marginLeft
            });
        };
        var nextButton = $('.right-arrow', _this).click(function() {
            marginLeft+= _this.width() * step / 100;
            iniMargin();
        });
        var prevButton = $('.left-arrow', _this).click(function() {
            marginLeft-= _this.width() * step / 100;
            iniMargin();
        });
        var size = 0;
        items.each(function() {
            size+= $(this).width();
        });
        container.width(size);
        iniMargin();
        $(window).resize(iniMargin);
    };

    //--- / YouTube player ---
    $(function() {
        $('#video-list').verticalSlider();
        $('#video-frame').iniYouTube();
        $('#horizontal-slider').horizontalSlider();
    })
})(jQuery);
