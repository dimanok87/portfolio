// Utility
if ( typeof Object.create !== 'function' ) {
	Object.create = function( obj ) {
		function F() {}
		F.prototype = obj;
		return new F();
	};
}


(function($, window, document) {
	$.fn.doGlobe = function( options ) {
		 if (Detector.webgl){
			var g = Object.create( globeObj );
			g.init( options, this );
			return this
		 } else {
			var contentDiv = $(this);
			$.get('noWebGl.html', function(data) {
				contentDiv.html(data);
			});
			return false;
		}
    };

	//options
	$.fn.doGlobe.options = {
		globeRadius: 200,
		globeShine: 1,
		globeTexture: "textures/map-texture.jpg",
		globeMinScale: 0.3,
		globeMaxScale: 4,
		globeSegments: 60,
		headLampIntensity: 1,
		headLampColor: 0xffffff,
		headLampPosX: 500,
		headLampPosY: -100,
		headLampPosZ: 4000,
		camX: 0,
		camY: 0,
		camZ: 1500,
		autoRotateSpeed: 0.08,
        lineWidth: 3,
        lineColor: 0x53b5ff,
        onClickPolygon: function(object) {
            var _th = $(this);
            $.colorbox({
                href: object.name + '.html',
                iframe: true,
                width: 850,
                height: 620
            });
        }
	};

	var globeObj = {
		init: function(options, elem) {
			var me = this;
			me.elem = elem;
			me.$elem = $(elem);
			me.targetRotation_x = 0;
			me.targetRotation_y = 0;
			me.hotspotsArr = [];
			me.hotspotsLines = [];
            me.activeName = false;
			me.options = $.extend({}, $.fn.doGlobe.options, options);
			me.windowDimensionX = me.$elem.innerWidth();
			me.windowDimensionY = me.$elem.innerHeight();
			me.refreshGlobe();
			me.animate();
		},
		refreshGlobe: function(){
			var me = this;
			me.setup3D();
			me.generateContent();
			me.setupListeners();
		},
		setup3D: function(){
			var me = this;
			me.globeContainer = new THREE.Object3D();
			me.scene = new THREE.Scene();
			me.camera = new THREE.OrthographicCamera(me.windowDimensionX / - 2, me.windowDimensionX / 2, me.windowDimensionY / 2, me.windowDimensionY / - 2, 1, 2000 );
			me.camera.position.x = me.options.camX;
			me.camera.position.y = me.options.camY;
			me.camera.position.z = me.options.camZ;

			//lighting
			me.headLamp = new THREE.PointLight(me.options.headLampColor, me.options.headLampIntensity);
			me.headLamp.position.set(me.options.headLampPosX, me.options.headLampPosY, me.options.headLampPosZ);
			//globeGeometry
			me.globeGeo = new THREE.Mesh(
                new THREE.SphereGeometry(
                    me.options.globeRadius,
                    me.options.globeSegments,
                    me.options.globeSegments
                ),
                new THREE.MeshPhongMaterial({
                    map: THREE.ImageUtils.loadTexture(me.options.globeTexture),
                    ambient: 0x555555,
                    color: 0xffffff,
                    specular: 0x555555,
                    shininess: me.options.globeShine
                })
            );

			me.globeGeo.position.x = 0;
			me.globeGeo.position.y = 0;
			me.globeGeo.position.z = 0;

            me.mouse = new THREE.Vector2();
            me.raycaster = new THREE.Raycaster();

			me.renderer = new THREE.WebGLRenderer({
                antialias: true,
                alpha: true
            });
			me.renderer.clear();
			me.renderer.setSize(me.windowDimensionX, me.windowDimensionY);
			me.renderer.autoClear = true;
			me.elem.append( me.renderer.domElement );
            me.globeContainer.add(me.globeGeo);
            me.scene.add(me.camera);
            me.scene.add(me.headLamp);
			me.scene.add(me.globeContainer);
		},

		generateContent: function () {
            var me = this;
            var createSVGPathCoords = function(geometry) {
                var str = 'M';
                var coords = geometry.coordinates;
                var parseArray = function(arr2) {
                    return arr2.map(function(arr3) {
                        arr3[0]+= 180;
                        arr3[1]+= 180;
                        return arr3.join(',');
                    }).join('L');
                };
                switch(geometry.type) {
                    case 'Polygon':
                        str+= parseArray(coords[0]);
                        break;
                    case 'MultiPolygon':
                        str+= coords.map(function(coo) {
                            return parseArray(coo[0])
                        }).join('ZM');
                        break;
                }
                str+= 'Z';
                return str;
            };
            var material = new THREE.MeshBasicMaterial({
                visible: false
            });
            var lineMaterial = new THREE.LineBasicMaterial({
                color: me.options.lineColor,
                linewidth: me.options.lineWidth
            });

            $.when($.getJSON("countries.json") ).then(function(data) {
                var countries = [];
                var i, j;
                for (i = 0 ; i < data.features.length ; i++) {
                    var geoFeature = data.features[i];
                    var properties = geoFeature.properties;
                    var feature = createSVGPathCoords(geoFeature.geometry);
                    var mesh = transformSVGPathExposed(feature);
                    for (j = 0 ; j < mesh.length ; j++) {
                        countries.push({"data": properties, "mesh": mesh[j]});
                    }
                }

                for (i = 0 ; i < countries.length ; i++) {
                    var shape3d = countries[i].mesh.extrude({amount: 1, bevelEnabled: false});
                    var linesCounters = shape3d.vertices.map(function(vert) {
                        var pos = me.translateGeoCoords(vert.y - 180, - (vert.x - 180), me.options.globeRadius);
                        vert.x = pos.x;
                        vert.y = pos.y;
                        vert.z = pos.z;
                        return new THREE.Vector3(pos.x, pos.y, pos.z);
                    });

                    var geometry = new THREE.Geometry();
                    geometry.vertices = linesCounters;
                    var line = new THREE.Line(geometry, lineMaterial);
                    line.name = countries[i].data.name;
                    me.hotspotsLines.push(line);
                    var toAdd = new THREE.Mesh(shape3d, material);
                    toAdd.name = countries[i].data.name;
                    me.hotspotsArr.push(toAdd);
                    me.globeGeo.add(toAdd);
                }
            });
        },
		translateGeoCoords: function (lat, long, r) {
			var latPI = lat * Math.PI / 180,
                lngPI = long * Math.PI / 180,
                latCOS = r * Math.cos(latPI);
			return {
                x: latCOS * Math.cos(lngPI),
                y: r * Math.sin(latPI),
                z: latCOS * Math.sin(lngPI)
            }
		},
		doZoom: function(wheelDelta) {
			var me = this;
			wheelDelta*= -1;
			var currScale = me.globeContainer.scale.z;
			var targetScale = wheelDelta * (currScale / 3) + currScale;
            targetScale = Math.max(
                me.options.globeMinScale, Math.min(
                    me.options.globeMaxScale,
                    targetScale
                )
            );
			new TWEEN.Tween(me.globeContainer.scale)
				.to({x: targetScale, y: targetScale, z: targetScale}, 500)
				.easing(TWEEN.Easing.Quartic.EaseOut)
				.start();
            me.renderer.render(me.scene, me.camera);
		},

		setupListeners: function (){
			var me = this;
			me.$elem.on('mousedown', this, me.onDocumentMouseDown);
			me.$elem.on('click', this, me.onDocumentMouseClick);
            me.$elem.on('mousemove', this, me.onMapMouseMove);
			me.$elem.mousewheel( {me:me}, me.onDocumentMouseWheel); 
		},
						
		onDocumentMouseWheel: function(event, delta) {
			var me = event.data.me;
			event.preventDefault();
			me.doZoom(delta)
		},

		onDocumentMouseClick: function(e){
			var me = e.data;
			me.checkHotspotClick();
		},

		checkHotspotClick: function(){
			var me = this;
            if (me.movedPosition) {
                me.movedPosition = false;
                return;
            }
            me.mouse.y = -((event.pageY - me.$elem.offset().top) / me.windowDimensionY) * 2 + 1;
            me.mouse.x = ((event.pageX - me.$elem.offset().left) / me.windowDimensionX) * 2 - 1;
            me.raycaster.setFromCamera(me.mouse, me.camera);
            var intersects = me.raycaster.intersectObjects(me.hotspotsArr);

            if (intersects.length > 0) {
                me.options.onClickPolygon(intersects[0].object);
            }
		},

		onDocumentMouseDown: function( event ) {
			event.preventDefault();
			var me = event.data;
			me.mouseDown = true;
            window.cancelAnimationFrame(me.animationHandler);
			mouseY = event.pageY - me.$elem.offset().top;
			mouseX = event.pageX - me.$elem.offset().left;
			me.$elem.on( 'mousemove', me,  me.onDocumentMouseMove );
			$(document).on( 'mouseup', me, me.onDocumentMouseUp );
			mouseXOnMouseDown = event.clientX - me.windowDimensionX;
			targetRotationOnMouseDown_x = me.targetRotation_x;
			mouseYOnMouseDown = event.clientY - me.windowDimensionY;
			targetRotationOnMouseDown_y = me.targetRotation_y;
			me.options.autoRotate = false
		},
		onDocumentMouseMove: function( event ) {
			var me = event.data;
            me.movedPosition = true;
			mouseX = event.clientX - me.windowDimensionX;
			me.targetRotation_x = targetRotationOnMouseDown_x + ( mouseX - mouseXOnMouseDown ) * 0.02;
			mouseY = event.clientY - me.windowDimensionY;
			me.targetRotation_y = targetRotationOnMouseDown_y + ( mouseY - mouseYOnMouseDown ) * 0.02;

            me.globeContainer.rotation.y += (0.35 * me.targetRotation_x - me.globeContainer.rotation.y) * 0.2;
            me.globeContainer.rotation.x += (0.35 * me.targetRotation_y - me.globeContainer.rotation.x) * 0.2;

            TWEEN.update();
            me.render();
		},
        onMapMouseMove: function(event) {
            event.preventDefault();
            var me = event.data;
            if (me.movedPosition) {
                return;
            }
            me.mouse.y = -((event.pageY - me.$elem.offset().top) / me.windowDimensionY) * 2 + 1;
            me.mouse.x = ((event.pageX - me.$elem.offset().left) / me.windowDimensionX) * 2 - 1;
            me.raycaster.setFromCamera(me.mouse, me.camera);
            var intersects = me.raycaster.intersectObjects(me.hotspotsArr);
            if (me.activeName) {
                me.hotspotsLines.map(function(line) {
                    line.name == me.activeName ? me.globeGeo.remove(line) : false;
                });
            }
            if (intersects.length > 0) {
                var name = intersects[0].object.name;
                me.hotspotsLines.map(function(line) {
                    line.name == name ? me.globeGeo.add(line) : false;
                });
                me.activeName = name;
            }
            TWEEN.update();
            me.renderer.render(me.scene, me.camera);
        },
		onDocumentMouseUp: function( event ) {
			var me = event.data;
			me.mouseDown = false;
			me.$elem.off( 'mousemove', me.onDocumentMouseMove );
			$(document).off('mouseup', me.onDocumentMouseUp );
		},
		animate: function() {
			this.animationHandler = requestAnimationFrame(this.animate.bind(this));
			var me = this;
            me.targetRotation_y = Math.min(5, Math.max(me.targetRotation_y, -5));
            me.targetRotation_x += me.options.autoRotateSpeed;
			me.globeContainer.rotation.y += (0.35 * me.targetRotation_x - me.globeContainer.rotation.y) * 0.2;
			me.globeContainer.rotation.x += (0.35 * me.targetRotation_y - me.globeContainer.rotation.x) * 0.2;
			TWEEN.update();
			me.render();
		},
		render: function() {
			var me = this;
            //me.raycaster.setFromCamera(me.mouse, me.camera);
			me.renderer.render(me.scene, me.camera);
		}
	}

})( jQuery, window, document );
