(function($) {
    $.fn.postSlideShow = function() {
        $(this).addClass('init-slider').each(function() {

            var _this = $(this);
            var list = $('ul', _this);
            var items = $('li', list).on('click', function() {
                goTo(activeIndex + 1);
            });
            var activeIndex = 1;

            var goTo = function(index) {
                index = index < 0 ? items.length - 1 : index > items.length - 1 ? 0 : index;
                if (activeIndex == index) return;
                pagingButtons[activeIndex].removeClass('active');
                activeIndex = index;
                pagingButtons[activeIndex].addClass('active');
                list.css('margin-left', -activeIndex * 100 + '%');
                activeIndex == 0 ?
                    prevSlideButton.addClass('disabled') :
                    activeIndex == items.length - 1 ?
                        nextSlideButton.addClass('disabled') :
                        false;
                activeIndex != 0 ? prevSlideButton.removeClass('disabled') : false;
                activeIndex != items.length - 1 ? nextSlideButton.removeClass('disabled') : false;
            };

            var nextSlideButton = $('<div>').addClass('control-slider next-button slider-button').appendTo(_this);
            var prevSlideButton = $('<div>').addClass('control-slider prev-button slider-button').appendTo(_this);
            var pagingBlock = $('<div>').addClass('paging-slider').appendTo(_this);

            nextSlideButton.click(function() {
                goTo(activeIndex + 1);
            });
            prevSlideButton.click(function() {
                goTo(activeIndex - 1);
            });

            list.width(items.length * 100 + '%');
            items.width(100 / items.length + '%');

            var pagingButtons = [];
            items.each(function(k) {
                var pagingButton = $('<div>').addClass('paging-button slider-button').appendTo(pagingBlock);
                pagingButtons.push(pagingButton);
                pagingButton.click(function() {
                    goTo(k);
                });
            });
            goTo(0);
        });
    };
    $(function() {
        $('#main-menu').scroller();
        $('.with-list').each(function() {
            var active = false;
            var _this = $(this);
            var deactive = function(e) {
                var target = $(e.target);
                if (!(target.is(_this) ||  _this.find(target).length)) {
                    $(window).off('click', deactive);
                    _this.removeClass('active');
                    active = false;
                }
            };
            $(this).on({
                click: function() {
                    if (!active) {
                        _this.addClass('active');
                        active = true;
                        $(window).on('click', deactive);
                    }
                }
            });
        });
        $('.video-inset').each(function() {
            var _this = $(this);
            var w = _this.attr('width');
            var h = _this.attr('height');
            var k = w / h;
            _this.attr('width', '100%');
            _this.attr('height', _this.width() / k);
            $(window).on('resize', function() {
                _this.attr('height', _this.width() / k);
            });
        });
        $('.post-slideshow').postSlideShow();
    })
})(jQuery);