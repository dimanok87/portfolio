(function($) {
    $.fn.sortContent = function(options) {
        return $(this).each(function() {
            var _this = $(this), columns = [];
            var items = $(options['selector'], _this);
            var createColumns = function(columnsLength) {
                if (columns.length) {
                    items.detach();
                    while (columns.length) {
                        columns.shift().empty().remove();
                    }
                }
                for (var k = 0; k < columnsLength; k++) {
                    columns.push(
                        $('<div>').
                            addClass(options['colsClass'] + ' column-' + columnsLength + ' column-index-' + k).
                            appendTo(_this));
                }
                var itemsArray = [];
                var lastedItem = -1;

                var insertToColumn = function() {
                    if (!itemsArray[lastedItem + 1] || !itemsArray[lastedItem + 1]['loaded']) return;
                    lastedItem++;
                    var item = itemsArray[lastedItem]['item'];
                    var myColumn = 0;
                    $(columns).each(function(k) {
                        if (k && columns[k].height() < columns[myColumn].height()) {
                            myColumn = k;
                        }
                    });
                    columns[myColumn].append(item.css('visibility', ''));
                    insertToColumn();
                };
                items.each(function(i) {
                    var item = $(this).appendTo(_this);
                    itemsArray.push({
                        loaded: false,
                        item: item
                    });
                    var images = $('img', item);
                    var loadedImages = 0;
                    if (itemsArray == 1 && !images.length) {
                        insertToColumn();
                        return;
                    }
                    var testImages = function() {
                        loadedImages++;
                        itemsArray[i]['loaded'] = loadedImages == images.length;
                        if (i == (lastedItem + 1)) {
                            insertToColumn();
                        }
                    };
                    images.each(function() {
                        var img = this;
                        var _th = $(this);
                        var height = img.naturalHeight;
                        item.css('visibility', 'hidden');
                        if (height) {
                            testImages();
                        } else {
                            _th.on('load', testImages);
                        }
                    });
                });
            };

            var widthPage = 0;
            var oldCols = 0;
            var reiniCols = function() {
                if (widthPage == _this.width()) return;
                widthPage = _this.width();
                var colsLength = Math.min(options['maxColumns'], Math.floor(widthPage / options['colsWidth']));
                if (colsLength != oldCols) {
                    createColumns(oldCols = colsLength);
                }
            };
            $(window).on('resize', reiniCols);
            reiniCols();
        });
    }
})(jQuery);