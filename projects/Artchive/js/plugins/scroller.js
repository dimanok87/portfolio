(function($) {
    var mask = $('<div>').css({
        position: 'absolute',
        zIndex: 999,
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
    });
    $.fn.scroller = function() {
        return $(this).each(function() {
            var marginLeft = 0;
            var startMovePosition = 0;
            var iniDown = function() {
                list.off('mousedown', iniDown);
                list.on({
                    'mouseup': breakDown,
                    'mousemove': iniScroll
                });
            };
            var iniScroll = function(e) {
                e.preventDefault();
                startMovePosition = e.clientX - marginLeft;
                list.off({
                    'mouseup': breakDown,
                    'mousemove': iniScroll
                });
                $(window).on({
                    'mousemove': moveList,
                    'mouseup': endMove
                });
                mask.appendTo($('body:first'));
            };
            var moveList = function(e) {
                e.preventDefault();
                marginLeft = e.clientX - startMovePosition;
                list.css({
                    marginLeft: marginLeft
                });
            };
            var breakDown = function() {
                list.off({
                    'mouseup': breakDown,
                    'mousemove': iniScroll
                });
                list.on('mousedown', iniDown);
            };
            var endMove = function() {
                $(window).off({
                    'mousemove': moveList,
                    'mouseup': endMove
                });
                list.on('mousedown', iniDown);
                marginLeft = Math.min(0, Math.max(container.width() - width, marginLeft));
                list.animate({
                    marginLeft: marginLeft
                });
                mask.detach();
            };
            var list = $(this).on('mousedown', iniDown);

            var container = list.parent();
            var width = 0;
            var items = $('li', list).each(function() {
                width+= $(this).width();
            });
            list.width(width + 10);
        });
    };
})(jQuery);
