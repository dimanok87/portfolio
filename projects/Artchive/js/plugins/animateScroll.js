(function($) {
    var windowScroll = function(el, duration) {
        var top = el.offset()['top'];
        var win = $(window);
        $({scrt: win.scrollTop()}).animate({scrt: top}, {
            duration: duration,
            step: function() {
                win.scrollTop(this.scrt);
            }
        });
    };
    $(function() {
        $('a[href^="#"]').each(function() {
            var _this = $(this);
            var href = $(this).attr('href').split('#')[1];
            var anchor = $('a[id="' + href + '"]');
            if (anchor.length) {
                _this.on('click', function(e) {
                    e.preventDefault();
                    windowScroll(anchor, 1000);
                });
            }
        });
    })
})(jQuery);