(function($) {
    var sortSelections = function() {
        $('.group-images-sl').each(function() {
            var _this = $(this);
            var loadedImages = 0;
            var imgs = $('img', _this);


            var testImages = function() {
                loadedImages++;
                if (loadedImages == imgs.length) {
                    var oldImage = false;
                    imgs.each(function(k) {
                        var _th = $(this);
                        if (k) {
                            _th.height(oldImage.height() - 30);
                        }
                        oldImage = _th;
                    });
                }
            };

            imgs.each(function() {
                var img = this;
                var _th = $(this);
                var height = img.naturalHeight;
                if (height) {
                    testImages();
                } else {
                    _th.on('load', testImages);
                }
            });

        });
    };
    $(function() {
        sortSelections();
        $('#collections-list').sortContent({
            selector: '.favorites-item',
            colsWidth: 270,
            maxColumns: 4,
            colsClass: 'favorites-column'
        });
    });
})(jQuery);