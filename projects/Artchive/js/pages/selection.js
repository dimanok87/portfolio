(function($) {
    $(function() {
        $('#favorites-list').sortContent({
            selector: '.favorites-item',
            colsWidth: 270,
            maxColumns: 4,
            colsClass: 'favorites-column'
        });
    })
})(jQuery);