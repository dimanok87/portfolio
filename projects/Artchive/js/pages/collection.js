(function($) {
    var sortImages = function() {
        $('.group-images').each(function() {
            var _this = $(this);
            var loadedImages = 0;
            var imgs = $('img', _this);

            var testImages = function() {
                loadedImages++;
                if (loadedImages == imgs.length) {
                    var maxImage = false;
                    imgs.each(function() {
                        var img = $(this);
                        maxImage = !maxImage ?
                        {i: img, h: img.height()} :
                            (img.height() > maxImage['h'] ?
                            {i: img, h: img.height()} : maxImage);
                    });
                    maxImage['i'].prependTo(maxImage['i'].parent());
                }
            };

            imgs.each(function() {
                var img = this;
                var _th = $(this);
                var height = img.naturalHeight;
                if (height) {
                    img.height = height * (img.width / img.naturalWidth);
                    testImages();
                } else {
                    _th.on('load', testImages);
                }
            });

        });
    };
    $(function() {
        sortImages();
        $('#collections-list').sortContent({
            selector: '.favorites-item',
            colsWidth: 270,
            maxColumns: 4,
            colsClass: 'favorites-column'
        });
    })
})(jQuery);