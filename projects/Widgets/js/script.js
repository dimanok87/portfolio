(function($) {
    $(function(){
        var msw = new Mysiwe({
            onInit: function() {
                msw.startApplication('baseApp', {
                    appsList: $('[data-started-apps]:first'),
                    appsBtns: $('[data-app-start]'),
                    appNameAttr: 'data-app-start',
                    appsContainers: $('[data-app-container]'),
                    appsContainersAttr: 'data-app-container'
                });
            }
        });
    });
})(jQuery);