var Mysiwe = function(options) {

    var MSW = this;

    options = jQuery.extend(options, {
        prototypePath: 'js/prototype/prototype.js',
        pluginsPath: 'plugins/%PLUGIN_NAME%.js',
        appsPath: 'js/apps/%APP_NAME%/',
        jsFile: 'script.js',
        modulesPath: 'js/modules/%MODULE_NAME%.js',
        objectsPath: 'objects/%OBJECT_NAME%.js'
    });

    var ajax = jQuery.ajax;
    var appsList = {};


    /*------------------------------------------ Статусы компонентов ------------------------------------------*/

    /* Статусы запуска приложения */
    var appsStatuses = {};

    /* Статусы запуска плагинов приложений */
    var pluginsStatuses = {};

    /* Статусы запуска модулей */
    var modulesStatuses = {};

    /* Статусы запуска объектов приложения */
    var objectsStatuses = {};

    /*------------------------------------------ Callback`и, выполняющиеся после загрузки и выполнения компонентов ------------------------------------------*/
    /* Для приложений */
    var APPS_CALLBACKS = {};

    /* Для общих методов приложения */
    var METHODS_CALLBACKS = {};

    /* Для модулей приложения */
    var MODULES_CALLBACKS = {};

    /* Для плагинов приложения */
    var PLUGINS_CALLBACKS = {};

    /* Для объектов приложения */
    var OBJECTS_CALLBACKS = {};

    /*------------------------------------------ Загруженные и выполненные компоненты ------------------------------------------*/
    /* Общие методы приложений */
    var methods = {};

    /* Плагины */
    var plugins = {};

    /* Модули */
    var modules = {};

    /* Объекты */
    var objects = {};

    /* Очередь стартующих приложений */
        // Надо реализовать очередь выполнения приложений

    /*
        Добавление callback`ов для методов приложения
    */
    var addMethodCallback = function(appName, methodName, callback) {
        METHODS_CALLBACKS[appName] = METHODS_CALLBACKS[appName] || {};
        METHODS_CALLBACKS[appName][methodName] = METHODS_CALLBACKS[appName][methodName] || [];
        METHODS_CALLBACKS[appName][methodName].push(callback);
    };

    /*
        Срабатывает при добавлении публичного метода
    */
    var onShareMethod = function(appName, methodName) {
        var appArray = METHODS_CALLBACKS[appName] || {};
        while ((appArray[methodName] || []).length) {
            appArray[methodName].shift()();
        }
    };

    /*
        Добавление callback`а, который срабатывает после инициализации приложения
     */
    var addAppCallback = function(appName, callback) {
        (APPS_CALLBACKS[appName] = APPS_CALLBACKS[appName] || []).push(callback);
    };

    /*
        Отрабатываем все callback`и после инициализации приложения
    */
    var onStartApplication = function(appName) {
        APPS_CALLBACKS[appName] = APPS_CALLBACKS[appName] || [];
        while(APPS_CALLBACKS[appName].length) {
            APPS_CALLBACKS[appName].shift()();
        }
    };

    /*
        Добавление callback`а, который срабатывает после инициализации приложения
    */
    var addPluginCallback = function(appName, pluginName, callback) {
        PLUGINS_CALLBACKS[appName] = PLUGINS_CALLBACKS[appName] || {};
        PLUGINS_CALLBACKS[appName][pluginName] = PLUGINS_CALLBACKS[appName][pluginName] || [];
        PLUGINS_CALLBACKS[appName][pluginName].push(callback);
    };

    /*
        Отрабатываем все callback`и после инициализации приложения
    */
    var onStartPlugin = function(appName, pluginName) {
        while(PLUGINS_CALLBACKS[appName] && PLUGINS_CALLBACKS[appName][pluginName] && PLUGINS_CALLBACKS[appName][pluginName].length) {
            PLUGINS_CALLBACKS[appName][pluginName].shift()();
        }
    };


    /*
     Добавление callback`а, который срабатывает после инициализации объекта приложения
     */
    var addObjectCallback = function(appName, objectName, callback) {
        OBJECTS_CALLBACKS[appName] = OBJECTS_CALLBACKS[appName] || {};
        OBJECTS_CALLBACKS[appName][objectName] = OBJECTS_CALLBACKS[appName][objectName] || [];
        OBJECTS_CALLBACKS[appName][objectName].push(callback);
    };

    /*
     Отрабатываем все callback`и после инициализации объекта приложения
     */
    var onInitObject = function(appName, objectName) {
        while(OBJECTS_CALLBACKS[appName] && OBJECTS_CALLBACKS[appName][objectName] && OBJECTS_CALLBACKS[appName][objectName].length) {
            OBJECTS_CALLBACKS[appName][objectName].shift()();
        }
    };

    /*
        Добавление callback`а, который срабатывает после инициализации модуля
    */
    var addModuleCallback = function(moduleName, callback) {
        (MODULES_CALLBACKS[moduleName] = MODULES_CALLBACKS[moduleName] || []).push(callback);
    };

    /*
        Отрабатываем все callback`и после инициализации модуля
    */
    var onStartModule = function(moduleName) {
        while(MODULES_CALLBACKS[moduleName] && MODULES_CALLBACKS[moduleName].length) {
            MODULES_CALLBACKS[moduleName].shift()();
        }
    };

    var basedMethodsOfApps = new (function() {

        /*-------------------------------------------------------------------------------------------------
            Вызов метода какого-либо модуля
        */
        this._module = function(moduleName, moduleMethod, moduleMethodParams, callback) {
            modulesStatuses[moduleName] = modulesStatuses[moduleName] || 'ON_START';
            switch (modulesStatuses[moduleName]) {
                case 'ON_START':
                    modulesStatuses[moduleName] = 'ON_LOAD';
                    addModuleCallback(moduleName, function() {
                        callback ?
                            callback(modules[moduleName][moduleMethod](moduleMethodParams)) :
                            modules[moduleName][moduleMethod](moduleMethodParams);
                    });
                    ajax({
                        url: options['modulesPath'].replace('%MODULE_NAME%', moduleName),
                        dataType: 'text',
                        success: function (data) {
                            var moduleFunc = new Function(data);
                            modules[moduleName] = new moduleFunc();
                            modulesStatuses[moduleName] = 'STARTED';
                            onStartModule(moduleName);
                        }
                    });
                    break;
                case 'ON_LOAD':
                    addModuleCallback(moduleName, function() {
                        callback ?
                            callback(modules[moduleName][moduleMethod](moduleMethodParams)) :
                            modules[moduleName][moduleMethod](moduleMethodParams);
                    });
                    break;
                case 'STARTED':
                    callback ?
                        callback(modules[moduleName][moduleMethod](moduleMethodParams)) :
                        modules[moduleName][moduleMethod](moduleMethodParams);
                    break;
            }
        };

        /*-------------------------------------------------------------------------------------------------
            Добавить публичный метод
        */
        this._share = function(methodName, methodFunc) {
            var appName = this._info('slug');
            methods[appName] = methods[appName] || {};
            methods[appName][methodName] = methodFunc;
            onShareMethod(appName, methodName);
        };

        /*-------------------------------------------------------------------------------------------------
            Использовать метод, если есть в списке
        */
        this._use = function(appName, methodName, params) {
            return methods[appName] && methods[appName][methodName] ?
                methods[appName][methodName](this._info('slug'), params) :
                null;
        };

        /*-------------------------------------------------------------------------------------------------
            Загрузить и выполнить приложение, и если в нём есть метод - он выполнится
        */
        this._call = function(appName, appParams, methodName, params, callback) {
            if (this._onCall &&
                !this._onCall({
                    object: appName,
                    subject: this._info('slug')
                })) return;

            if (this._use(appName, methodName, params) === null) {
                methodName ? this._wait(appName, methodName, params, callback) : false;
                MSW.startApplication(appName, appParams);
            }
        };

        /*-------------------------------------------------------------------------------------------------
            Ждать, когда будет загружено и выполнено приложение и метод появится в списке публичных
        */
        this._wait = function(appName, methodName, params, callback) {
            var app = this;
            if (this._use(appName, methodName, params) === null) {
                METHODS_CALLBACKS[appName] = METHODS_CALLBACKS[appName] || [];
                methodName ? addMethodCallback(appName, methodName, function() {
                    callback ?
                        callback(app._use(appName, methodName, params)) :
                        app._use(appName, methodName, params);
                }) : false;
            }
        };

        /*-------------------------------------------------------------------------------------------------
            Вызов плагина приложения и его выполнение
        */
        this._plugin = function(plugName, plugParams) {
            var app = this;
            var slug = app._info('slug');
            pluginsStatuses[slug] = pluginsStatuses[slug] || {};
            pluginsStatuses[slug][plugName] = pluginsStatuses[slug][plugName] || 'ON_START';
            switch(pluginsStatuses[slug][plugName]) {
                case 'ON_START':
                    pluginsStatuses[slug][plugName] = 'ON_LOAD';
                    plugParams = plugParams || false;
                    var plug = (plugins[slug] = plugins[slug] || {});
                    ajax({
                        url: app._info('dir') + options['pluginsPath'].replace('%PLUGIN_NAME%', plugName),
                        dataType: 'text',
                        success: function (data) {
                            var pluginFunc = new Function('_params', data);
                            pluginFunc.prototype = app;
                            plug[plugName] = new pluginFunc(plugParams);
                            onStartPlugin(slug, plugName);
                            pluginsStatuses[slug][plugName] = 'STARTED';
                        }
                    });
                    break;
                case 'ON_LOAD':
                    addPluginCallback(slug, plugName, function() {
                        plugins[slug][plugName]._toggle ? plugins[slug][plugName]._toggle(plugParams) : false;
                    });
                    break;
                case 'STARTED':
                    plugins[slug][plugName]._toggle ? plugins[slug][plugName]._toggle(plugParams) : false;
                    break;
            }
        };

        /*-------------------------------------------------------------------------------------------------
            Вызов и создание объекта приложения
        */
        this._object = function(objectName, params, callback) {
            var app = this;
            var slug = app._info('slug');
            objectsStatuses[slug] = objectsStatuses[slug] || {};
            objectsStatuses[slug][objectName] = objectsStatuses[slug][objectName] || 'ON_START';
            switch(objectsStatuses[slug][objectName]) {
                case 'ON_START':
                    objectsStatuses[slug][objectName] = 'ON_LOAD';
                    ajax({
                        url: app._info('dir') + options['objectsPath'].replace('%OBJECT_NAME%', objectName.replace('.', '/')),
                        dataType: 'text',
                        success: function (data) {
                            objects[slug] = objects[slug] || {};
                            objects[slug][objectName] = new Function('_params', data);
                            onInitObject(slug, objectName);
                            objectsStatuses[slug][objectName] = 'STARTED';
                            callback ?
                                callback(new objects[slug][objectName](params)) :
                                new objects[slug][objectName](params);
                        }
                    });
                    break;
                case 'ON_LOAD':
                    addObjectCallback(slug, objectName, function() {
                        return callback ?
                            callback(new objects[slug][objectName](params)) :
                            new objects[slug][objectName](params);
                    });
                    break;
                case 'STARTED':
                    return callback ?
                        callback(new objects[slug][objectName](params)) :
                        new objects[slug][objectName](params);
                    break;
            }
        };

        /*-------------------------------------------------------------------------------------------------
            Просто ajax
        */
        this._ajax = ajax;

    })();


    /*-------------------------------------------------------------------------------------------------
         Прототип приложения для вызова и сохранения загруженных
         конфигов, изображений, стилей и т.д. (статичных данных)
     */
    var appTypePrototype = function(_data) {
        /*-------------------------------------------------------------------------------------------------
            Возвращает основную информацию приложения
        */
        this._info = function(n) {
            return _data[n] || false;
        };
    };

    /*-------------------------------------------------------------------------------------------------
        Пользовательский прототип с базовыми методами для всех приложений
    */
    var loadPrototype = function() {
        ajax({
            url: options['prototypePath'],
            dataType: 'text',
            success: function (data) {
                var protoFunc = new Function(data);
                protoFunc.prototype = basedMethodsOfApps;
                appTypePrototype.prototype = new protoFunc();
                options['onInit'] ? options['onInit']() : false;
            }
        });
    };


    /*-------------------------------------------------------------------------------------------------
         Прототип для хранения параметров одного экземпляра приложения
         id, id_of_type
    */
    var dataPrototype = function(_data) {
        this._data = function (n) {
            return _data[n] || false;
        };
    };

    /*---------------------- Запуск приложения ----------------------*/

    var appsID = 0;

    /*-------------------------------------------------------------------------------------------------
        Создание нового приложения
    */
    var createNewApplication = function(appName, app, params) {
        appsID++;
        var appData = {
            id: appsID
        };
        dataPrototype.prototype = new appTypePrototype({
            slug: appName,
            dir: options['appsPath'].replace('%APP_NAME%', appName)
        });
        app.prototype = new dataPrototype(appData);
        appsList[appName] = new app(params);
    };

    /*-------------------------------------------------------------------------------------------------
        Загрузка приложения
    */
    var loadApplication = function(appName, params) {
        var appDir = options['appsPath'].replace('%APP_NAME%', appName);
        appsStatuses[appName] = 'ON_LOAD';
        ajax({
            url: appDir + options['jsFile'],
            dataType: 'text',
            success: function (data) {
                createNewApplication(appName, new Function('_params', data), params);
                onStartApplication(appName);
                appsStatuses[appName] = 'STARTED';
            }
        })
    };

    this.startApplication = function(appName, params) {
        appsStatuses[appName] = appsStatuses[appName] || 'ON_START';
        switch (appsStatuses[appName]) {
            case 'ON_START':
                loadApplication(appName, params);
                break;
            case 'ON_LOAD':
                addAppCallback(appName, function() {
                    appsList[appName]._toggle ? appsList[appName]._toggle(params) : false;
                });
                break;
            case 'STARTED':
                appsList[appName]._toggle ? appsList[appName]._toggle(params) : false;
                break;
        }
    };

    loadPrototype();
};

