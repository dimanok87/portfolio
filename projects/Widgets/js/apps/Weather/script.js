this._onInit();
var script = $('script');

var weatherCodes = {
    0: 'tornado',
    1: 'tropical storm',
    2: 'hurricane',
    3: 'severe thunderstorms',
    4: 'thunderstorms',
    5: 'mixed rain and snow',
    6: 'mixed rain and sleet',
    7: 'mixed snow and sleet',
    8: 'freezing drizzle',
    9: 'drizzle',
    10: 'freezing rain',
    11: 'showers',
    12: 'showers',
    13: 'snow flurries',
    14: 'light snow showers',
    15: 'blowing snow',
    16: 'snow',
    17: 'hail',
    18: 'sleet',
    19: 'dust',
    20: 'foggy',
    21: 'haze',
    22: 'smoky',
    23: 'blustery',
    24: 'windy',
    25: 'cold',
    26: 'cloudy',
    27: 'mostly cloudy (night)',
    28: 'mostly cloudy (day)',
    29: 'partly cloudy (night)',
    30: 'partly cloudy (day)',
    31: 'clear (night)',
    32: 'sunny',
    33: 'fair (night)',
    34: 'fair (day)',
    35: 'mixed rain and hail',
    36: 'hot',
    37: 'isolated thunderstorms',
    38: 'scattered thunderstorms',
    39: 'scattered thunderstorms',
    40: 'scattered showers',
    41: 'heavy snow',
    42: 'scattered snow showers',
    43: 'heavy snow',
    44: 'partly cloudy',
    45: 'thundershowers',
    46: 'snow showers',
    47: 'isolated thundershowers',
    3200: 'not available'
};


window.showWeather = function(data) {
    console.log(data);
    var location = data['query']['results']['result']['location'];
    bgImage.attr('src', data['query']['results']['result']['photo']['url_mrs']);
    weatherStatus.attr('src', location['currently']['condition']['imglarge']);
    tempBlock.html(location['currently']['temp'] + '&deg;');
    shortDescription.text(location['currently']['condition']['title']);
    cityName.text(location['city'] + ', ' + location['country']);
//    _params.container.html();
};
window.showWeatherPhoto = function(data) {
    console.log(data);
};



var urlAttr = [
    'format=json',
    'callback=showWeather',
    'env=store://datatables.org/alltableswithkeys'
];

var imgBgContainer = $('<div>').addClass('weather-bg-container');
var weatherInfoContainer = $('<div>').addClass('weather-info-container');
var bgImage = $('<img>');
var weatherInfoBlock = $('<div>').addClass('weather-info-block');
var weatherStatus = $('<img>').addClass('weather-status-img');
var weatherContent = $('<div>').addClass('weather-info-content');

var tempBlock = $('<span>').addClass('weather-temp');
var shortDescription = $('<span>').addClass('weather-code-info');

var cityName = weatherInfoBlock.clone().addClass('city-name')

_params.container.html('').append(
    weatherInfoContainer.append(
        cityName,
        weatherInfoBlock.append(
            weatherContent.append(
                tempBlock,
                shortDescription
            ),
            weatherStatus
        )
    ),
    imgBgContainer.append(bgImage)
);


$.ajax({
    url: 'https://query.yahooapis.com/v1/public/yql?q=select * from yahoo.media.weather where woeid in (select woeid from geo.places(1) where text="Moscow")&' + urlAttr.join('&'),
    dataType: 'jsonp',
    callback: 'showWeather'
});