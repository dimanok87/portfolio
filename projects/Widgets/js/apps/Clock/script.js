this._onInit();
var clock = $('<div>').addClass('clock-container');
var minutes = $('<span>').addClass('clock-one-part');
var hours = $('<span>').addClass('clock-one-part');
var seconds = $('<span>').addClass('clock-one-part seconds-part');
var delimiter = $('<span>').addClass('clock-one-part').text(':');

_params.container.html(
    clock.append(seconds, hours, delimiter, minutes)
);

var updateTime = function() {
    setTime(new Date());
};

var setTime = function(dataTime) {
    var h = dataTime.getHours();
    h = h < 10 ? '0' + h : h;
    var m = dataTime.getMinutes();
    m = m < 10 ? '0' + m : m;
    var s = dataTime.getSeconds();
    s = s < 10 ? '0' + s : s;
    seconds.text(s);
    hours.text(h);
    minutes.text(m);
};
var hideDelimiter = function() {
    delimiter.toggleClass('novisible');
    setTimeout(showDelimiter, 250);
};
var showDelimiter = function() {
    updateTime();
    delimiter.toggleClass('novisible');
    setTimeout(hideDelimiter, 700);
};
updateTime();
setTimeout(hideDelimiter, 700);
