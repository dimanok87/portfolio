var application = this;

this._share('addInitApp', function(app, params) {
    _params.appsList.append(
        '<div class="app-item-name">' + params.name + '</div>'
    );
});

_params.appsBtns.each(function() {
    var _this = $(this);
    var appName = _this.attr(_params.appNameAttr);
    application._wait(appName, 'onInit');
    _this.on('click', function() {
        application._call(
            appName,
            {
                container: _params.appsContainers.filter('[data-app-container="' + appName + '"]')
            }
        );
    });

});

