(function($) {
    $(function() {
        var sections = $('section');
        var showedSection = 0;
        var inAnimate = false;
        var duration = 1000;
        var body = $('body');


        var activeScroll = 0;
        var toSlide = function(n) {
            inAnimate = true;
            var endScroll = 0;
            sections.each(function(i) {
                if (i < n) {
                    endScroll+= $(this).height();
                }
            });
            $({temporary_x: currentScroll}).animate({temporary_x: endScroll}, {
                duration: duration,
                step: function () {
                    $(window).scrollTop(activeScroll = this.temporary_x);
                },
                complete: function() {
                    setTimeout(function() {
                        inAnimate = false;
                    }, 200);

                    currentScroll = $(window).scrollTop();
                }
            });
            showedSection = n;
        };



        var toNextSlide = function() {
            if (inAnimate) return;
            var newShowedSection = Math.min(showedSection + 1, sections.length - 1);
            if (showedSection != newShowedSection) {
                toSlide(newShowedSection);
            }
        };

        var toPrevSlide = function() {
            if (inAnimate) return;
            var newShowedSection = Math.max(showedSection - 1, 0);
            if (showedSection != newShowedSection) {
                toSlide(newShowedSection);
            }
        };

        $('[data-to-next]').on('click', toNextSlide);

        var contentInfoTPL = $('#content-template');
        var contentInfoWindow = contentInfoTPL.get(0).outerHTML;
        contentInfoTPL.remove();

        function initialize() {
            var myLatlng = new google.maps.LatLng(54.1638436,-4.4737673);
            var mapOptions = {
                center: myLatlng,
                zoom: 17,
                scrollwheel: false
            };
            var map = new google.maps.Map(document.getElementById('map'), mapOptions);
            var infowindow = new google.maps.InfoWindow({
                content: contentInfoWindow
            });

            var image = new google.maps.MarkerImage('images/marker.png',
                new google.maps.Size(60, 60),
                new google.maps.Point(0,0),
                new google.maps.Point(30, 40)
            );
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                icon: image
            });
            /*google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map,marker);
            });*/
            infowindow.open(map,marker);
            setTimeout(function() {
                duration = 400;
                var contentTPL = $('#content-template');
                contentTPL.parents('.gm-style-iw:first').prev().remove();
                contentTPL.parents('.gm-style-iw:first').next().remove();
                contentTPL.parent().parent().append(
                    $('<div>').addClass('contacts-content-down-arrow')
                ).css({
                    'vertical-align': 'top',
                    'position': 'relative',
                    'overflow': 'visible',
                    'margin-bottom': '-24px',
                    'margin-left': '11px'
                }).parent().css({
                    'overflow': 'visible'
                });
            }, 1000);

        }

        google.maps.event.addDomListener(window, 'load', initialize);
        var mapMask = $('#map-block-mask').click(function() {
            mapMask.hide();
            setTimeout(function() {
                mapMask.show();
            }, 7000);
        });


        var currentScroll = 0;
        $(window).scrollTop(0);
        $(window).on("scroll", function(e) {
            if (inAnimate) {
                $(this).scrollTop(activeScroll);
                return false;
            }
            var newScroll = $(this).scrollTop();
            newScroll > currentScroll ? toNextSlide() : toPrevSlide();
            return false;
        });
    });


})(jQuery);
