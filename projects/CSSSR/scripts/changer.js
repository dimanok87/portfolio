(function($) {
	var valueParamsPrefix = "val-";
	
	$.fn.changer = function(options) {
		var win = $(window);
		var body = $("body:first");
		var values = options.values;
		var geometry = [0];
		var changerMask = $("<div>").
			addClass("field-changer-mask");
			
		return $(this).each(function() {
			var _this = $(this);
			var selectedValue = _this.val();
			var containerChanger = $("<div>").
				addClass("field-changer");
							
			var cursorPosition, onePixelPercent;
			
			var mouseDownArrow = function(e) {
				e.preventDefault();
				var containerWidth = containerChanger.width();
				onePixelPercent = 100 / containerWidth;
				cursorPosition = e.clientX;
				changerMask.appendTo(body);
				arrowChanger.unbind("mousedown", mouseDownArrow);
				win.bind({
					mousemove: mouseMoveWindow,
					mouseup: mouseUpWindow
				});
			};
			
			var arrowOriginalPosition = 0;
			
			var mouseMoveWindow = function(e) {
				e.preventDefault();
				arrowOriginalPosition = Math.min(
						100, Math.max(
							0, arrowPosition + (e.clientX - cursorPosition) * onePixelPercent
						)
					);
				arrowChanger.css({
					left: arrowOriginalPosition + "%"
				});
			};
			
			var mouseUpWindow = function() {
				arrowChanger.bind("mousedown", mouseDownArrow);
				win.unbind({
					mousemove: mouseMoveWindow,
					mouseup: mouseUpWindow
				});
				changerMask.detach();
				for (var i = 1; i < allValuesParamsArray.length; i++) {
					var param = allValuesParamsArray[i];
					var rightPoint = param.geometry.left + param.geometry.width;
					
					if (arrowOriginalPosition >= param.geometry.left && rightPoint >= arrowOriginalPosition) {
						arrowOriginalPosition - param.geometry.left < rightPoint - arrowOriginalPosition ? 
							setValue(allValuesParamsArray[i - 1].options.value) :
							setValue(param.options.value);
						return;
					}
				}
			};
			
			var arrowChanger = $("<div>").
				addClass("field-changer__arrow-changer").
				
				appendTo(containerChanger).bind({
					mousedown: mouseDownArrow
				});
				
			_this.before(containerChanger);
			
			var arrowPosition = 0;
			var setValue = function(val) {
				_this.val(val);
				var valueParams = allValuesParams[valueParamsPrefix + val];
				valueParams.geometry ? 
					arrowPosition = valueParams.geometry.width + valueParams.geometry.left : 
					arrowPosition = 0;
				arrowChanger.animate({
					left: arrowPosition + "%"
				}, 100, "linear");
			};
			
			var createItemChanger = function(params) {			
				var item = $("<div>").
					addClass("field-changer__item").
					appendTo(containerChanger);
					
				item.css({
					width: (params.geometry ? params.geometry.width : 0) + "%",
					left: (params.geometry ? params.geometry.left : 0) + "%"
				});
				
				var label = $("<div>").
					addClass("field-changer__item__label").
					appendTo(item).
					text(params.options.title).click(function() {
						setValue(params.options.value);
					});
					
				var hiddenLabel = $("<div>").
					addClass("field-changer__item__hidden-label").
					prependTo(containerChanger).click(function() {
						setValue(params.options.value);
					}).css({
						left: (params.geometry ? params.geometry.width + params.geometry.left : 0) + "%"
					});
				hiddenLabel.addClass("field-changer__item__hidden-label_position_" + params.options.position);
				item.addClass("field-changer__item_position_" + params.options.position);
				label.addClass("field-changer__item__label_position_" + params.options.position);
			};
			
			var fullLength = 0;
			var lastLength = 0;
			
			for (var i = 1; i < values.length; i++) {
				lastLength = lastLength ? options["rangeDilimiters"] * lastLength : 4;
				geometry[i] = {
					width: lastLength,
					left: fullLength
				};
				fullLength+= lastLength;
			}
			
			var oneSize = 100 / fullLength;
			var allValuesParams = {};
			var allValuesParamsArray = [];
			for (var k = 0; k < values.length; k++) {			
				var params = {};
				allValuesParams[valueParamsPrefix + values[k].value] = params;
				allValuesParamsArray.push(params);
				if (geometry[k]) {
					var itemGeometry = geometry[k];
					params["geometry"] = {
						width: Math.round(itemGeometry.width * oneSize),
						left: Math.round(itemGeometry.left * oneSize)
					};
				} else {
					params["geometry"] = {
						left: 0,
						width: 0
					};
				}
				params["options"] = {
					title: values[k].title,
					value: values[k].value,
					position: k == values.length - 1 ? "last" : (k == 0 ? "first" : "center") 
				};
				createItemChanger(params);
			}
			setValue(selectedValue || allValuesParamsArray[0].options.value);
		});
	};
})(jQuery);

