(function($) {
	$(function() {
		$('#changer-field').changer({	
			values: [
				{
					value: 0,
					title: 'Не владею'
				}, {
					value: 1,
					title: 'Использую готовые решения'
				}, {
					value: 2,
					title: 'Использую готовые решения и умею их переделывать'
				}, {
					value: 3,
					title: 'Пишу сложные скрипты'
				}
			],
			rangeDilimiters: 1.5
		});
	});
})(jQuery);
