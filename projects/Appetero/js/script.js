(function($) {
    $(function() {
        $('select').select(false, {
            parent: 'fieldset'
        });
        var header = $('header:first');
        $(window).on('scroll', function() {
            $(window).scrollTop() > 20 ?
                header.addClass('scrolled-page') :
                header.removeClass('scrolled-page');
        });
        $('#menu-switcher').on('click', function() {
            $(this).toggleClass('active-menu');
        });

        var body = $('body:first');
        var closeModal = function(e) {
            e.preventDefault();
            modalMask.removeClass('in');
            activeModal.removeClass('in');
            setTimeout(function() {
                activeModal.hide();
                modalMask.detach();
            }, 150);
        };
        $('[data-dismiss="modal"]').on('click', closeModal);
        var modalMask = $('#modal-mask').detach().show();
        var activeModal = false;
        $('[data-toggle="modal"]').each(function() {
            var _this = $(this);
            var modal = $(_this.attr('data-target'));
            _this.on('click', function(e) {
                e.preventDefault();
                modalMask.appendTo(body);
                activeModal = modal.show();
                setTimeout(function() {
                    modalMask.addClass('in');
                    activeModal.addClass('in');
                }, 10);
                return false;
            });
        });

    });
})(jQuery);

