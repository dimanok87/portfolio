(function($) {

    var NO_PROGRESS_BUTTONS = {
        'prev': 'Contact Us',
        'next': 'Start Survey'
    };
    var ON_PROGRESS_BUTTONS = {
        'prev': 'Backward',
        'next': 'Forward'
    };
    $(function() {

        var activeQuestion = 0;

        var goToQuestion = function(index) {
            if (index == activeQuestion) return;
            if (!form.valid() && index > activeQuestion) return;
            if (index <= questions.length) {
                progressIndicator.width(index / (questions.length - 1) * 100 + '%');
                if (index) {
                    stepQuestionText.text('(' + index + '/' + (questions.length - 1) + ')');
                } else {
                    stepQuestionText.text('');
                }
                if (activeQuestion == 0) {
                    formButtons.addClass('on-progress');
                    formButtons.eq(0).text(ON_PROGRESS_BUTTONS['prev']);
                    formButtons.eq(1).text(ON_PROGRESS_BUTTONS['next']);
                } else if (index == 0) {
                    formButtons.removeClass('on-progress');
                    formButtons.eq(0).text(NO_PROGRESS_BUTTONS['prev']);
                    formButtons.eq(1).text(NO_PROGRESS_BUTTONS['next']);
                }
                if (activeQuestion == questions.length - 1) {
                    formButtons.eq(1).addClass('btn-green').removeClass('btn-disabled');
                } else if (index == questions.length - 1) {
                    formButtons.eq(1).removeClass('btn-green').addClass('btn-disabled');
                }
                questions.eq(activeQuestion).addClass('invisible');
                questions.eq(activeQuestion = index).removeClass('invisible');
            }
        };

        var goToPrevQuestion = function() {
            if (activeQuestion) {
                goToQuestion(activeQuestion - 1);
            }
        };

        var goToNextQuestion = function() {
            if (activeQuestion < questions.length - 1) {
                goToQuestion(activeQuestion + 1);
            }
        };

        var form = $('#survey-form');
        var progressIndicator = $('.progress-bar-indicator:first', form);
        var stepQuestionText = $('.step-of-questions:first', form);
        var formButtons = $('.questions-list-footer:first .btn').each(function(i) {
            $(this).on('click', function() {
                i ? goToNextQuestion() : goToPrevQuestion();
            });
        });
        $('#add-ingredient').click(function() {
            var original = $('.for-copy-field', form);
            var clone = original.clone();
            original.after(clone.addClass('for-copy-field').val('')).removeClass('for-copy-field');

        });
        var questions = $('.survey-question', form);
        form.validate();
    });
})(jQuery);