(function($) {
    $.fn.iniHeader = function(options) {

        options = $.extend({
            rows: {
                'min-size': 100,
                'max-size': 200
            },
            cells: {
                'selector': '.header-cell'
            }
        }, options);

        var t = $(this);
        var logotype = $(options['logotype']['selector'], t);
        var rows = $(options['rows_selector'], t);
        var all_cells = $(options['cells']['selector'], rows).hide();

        var animateCells = function() {
            var prew_row = false;
            rows.each(function(i) {
                var r = $(this).css({'visibility': ''});
                var params_row = options['rows'].length ?
                    (options['rows'][i] ?
                        options['rows'][i] :
                        options['rows'][0]) :
                    options['rows'];
                var cells = $(options['cells']['selector'], r);
                var cells_length = cells.length;
                var row_width = r.innerWidth();
                var cells_width = Math.floor(row_width / cells_length);
                var center_count = (cells_length - 1) / 2;

                cells.css({'float': 'left', 'width': cells_width}).show().each(function(z) {
                    var c = $(this).css({
                        'margin-left': z ? (-cells_width + 'px'): (row_width/2 - cells_width/2)
                    });
                    var title = $('.title-item', c).css({'visibility': 'hidden'});
                    var img = $('img:first', c).css({
                        height: params_row['min-size'],
                        margin: (params_row['max-size'] - params_row['min-size']) / 2 + 'px auto 0'
                    });
                    img.css({'position': 'relative', opacity: 0}).animate({
                        height: params_row['max-size'],
                        marginTop: 0,
                        opacity: options['cells']['start-opacity'] || 1
                    }, 1500, 'linear', function() {
                        title.css({'visibility': ''}).hide().fadeIn('slow');
                    });
                    img.mouseleave(function() {
                        img.stop().animate({height: params_row['max-size'], opacity: options['cells']['start-opacity'] || 1, margin: 0}, 'fast');
                    }).mouseenter(function() {
                            var hovermargin = {
                                top: params_row['hover-size'] ? -(params_row['hover-size'] - params_row['max-size'])/2 : 0,
                                left: params_row['hover-size'] ? -img.width() * (params_row['hover-size'] / params_row['max-size'] * 100) : 0
                            };
                            img.stop().animate({height: params_row['hover-size'] || params_row['max-size'], opacity: 1, margin: hovermargin['top'] + 'px ' + hovermargin['left']}, 'fast');
                        });
                });
                r.append($('<div>').css({'clear': 'both', 'height': 0, 'line-height': 0, 'font-size': 0}));
                cells.animate({'margin-left': 0}, 1500, 'linear');
                prew_row = r;
            });
        };

        logotype.css({
            'width': options['logotype']['startsize']
        }).animate({
                'width': options['logotype']['endsize']
            }, 2500, function() {
                animateCells();
                var images = {
                    group_1: [],
                    group_2: []
                };
                var img_logotype = $('img:first', logotype);
                var logo_k = img_logotype.height() / img_logotype.clone().get(0).height;
                if (options['logotype']['endimage']) {
                    var end_logo = new Image();
                    end_logo.onload = function() {
                        img_logotype.get(0).src = end_logo.src;
                    };
                }

                end_logo.src = options['logotype']['endimage'];
                var last_position = false;
                var interval_times,
                    createStar = function(second) {
                        var l, t;
                        var getNewPosition = function(index) {
                            index = index || false;
                            var new_position = index !== false ? index : Math.round((options['star_positions'].length - 1) * Math.random());
                            if (new_position != last_position || index !== false) {
                                var position = options['star_positions'][last_position = new_position];
                                l = Math.round(position[0] * logo_k);
                                t = Math.round(position[1] * logo_k);
                            } else {
                                getNewPosition(false);
                            }
                        };
                        interval_times = 100 + Math.round(1000*Math.random());
                        second = second || false;
                        getNewPosition(second ? last_position : false);

                        var star_img = images['group_' + (!second ? 1 : 2)][Math.round(Math.random() * 3)].clone();
                        var k = (!second ? 1 : 0.5);
                        var w = star_img.get(0).width * k;
                        var h = star_img.get(0).height * k;
                        star_img.animate({'margin': -h / 2 + 'px ' + (-w / 2 + 'px'), height: h, opacity: 1}, '20', function() {
                            !second ? createStar(true) : false;
                            star_img.animate({'margin-left': 0, 'margin-top': 0, height: 0, opacity: 0}, !second ? '100' : '20', function() {
                                star_img.empty().remove();
                            });
                        });
                        !second ? setTimeout(createStar, interval_times) : false;
                        logotype.append(star_img.css({
                            'margin-left': 0,
                            'margin-top': 0,
                            height: 0,
                            opacity: 0,
                            position: 'absolute',
                            'left': l,
                            'top': t
                        }));
                    }, count_images = 8, sch = 0;
                for (var i = 0; i < 8; i++) {
                    var img = new Image();
                    img.onload = function() {
                        sch++;
                        sch == count_images ? createStar() : false;
                    };
                    img.src = 'images/star-' + (i+1) + '.png?t';
                    images['group_' + (i%2 + 1)].push($(img).css({'left': '50%', top: '50%'}));
                }
            });

    };
})(jQuery)