(function($) {
    $(function() {
        $('#anim-head').iniHeader({
            rows: [
                {
                    'min-size': 50,
                    'max-size': 310,
                    'minus': 10,
                    'plus': 0,
                    'hover-size': 330
                }, {
                    'min-size': 25,
                    'max-size': 200,
                    'minus': 10,
                    'plus': 0,
                    'hover-size': 220
                }
            ],
            rows_selector: '.header-row',
            cells: {
                'selector': '.header-cell',
                'start-opacity': 0.7
            },
            logotype: {
                'selector': '.logotype',
                'startsize': 350,
                'endsize': 150,
                'endimage': 'images/logo_2.png'
            },
            star_positions: [
                [184, 7],
                [146, 30],
                [122, 62],
                [68, 56],
                [38, 110],
                [8, 205],
                [60, 258],
                [144, 277],
                [186, 288],
                [288, 320],
                [269, 172],
                [328, 109],
                [260, 100],
                [300, 56],
                [223, 39]
            ]
        });

    })
})(jQuery)