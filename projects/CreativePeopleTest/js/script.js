(function($) {
    var body;
    $.fn.slider = function() {
        return $(this).each(function() {
            var _this = $(this);
            var sliderList = $('.slider-list:first', _this);
            var items = $('li', sliderList);
            var activeSlide = 0;
            var toSlide = function(n) {
                n = n < 0 ? items.length - 1 : n > items.length - 1 ? 0 : n;
                if (n == activeSlide) return;
                items.eq(activeSlide).removeClass('active');
                n != 0 ?
                    _this.addClass('no-first-slide') :
                    (activeSlide != 0 ?
                        _this.removeClass('no-first-slide') :
                        false
                    );
                items.eq(activeSlide = n).addClass('active');
            };
            var toNextSlide = function() {
                toSlide(activeSlide + 1);
            };
            var toPrevSlide = function() {
                toSlide(activeSlide - 1);
            };
            var pagination = $('<div>').addClass('pagination');
            var nextButton = $('<span>').addClass('paging-button next-page').on('click', toNextSlide);
            nextButton.append($('<img>').attr('src', 'images/slider/next_slide.svg'));
            var prevButton = $('<span>').addClass('paging-button prev-page').on('click', toPrevSlide);
            prevButton.append($('<img>').attr('src', 'images/slider/prev_slide.svg'));
            if (items.length > 1) {
                pagination.
                    append(prevButton, nextButton).
                    appendTo(_this);
            }
        });
    };

    var correctPositionPopup = function() {
        popupContainer.css({
            'top': (popupMask.height() - popupContainer.height()) / 2,
            'left': (popupMask.width() - popupContainer.width()) / 2
        });
    };
    var openPopup = function(content) {
        popupBlockContent.html(content);
        body.append(popupContainer, popupMask);
        setTimeout(function() {
            popupContainer.css({opacity: 1});
            popupMask.css({opacity: 1});
            correctPositionPopup();
        }, 10);
    };
    var closePopup = function() {
        popupContainer.css({opacity: 0});
        popupMask.css({opacity: 0});
        setTimeout(function() {
            popupContainer.detach();
            popupBlockContent.html('');
            popupMask.detach();
        }, 300);
    };

    var popupMask = $('<div>').addClass('modal-mask').on('click', closePopup);
    var popupContainer = $('<div>').addClass('modal-container');
    var popupBlockContent = $('<div>').addClass('modal-content').prependTo(popupContainer);
    var closeButton = $('<div>').
        addClass('modal-close').
        on('click', closePopup).
        append($('<img>').attr('src', 'images/icon_close.svg')).
        prependTo(popupContainer);

    $(window).on('resize', correctPositionPopup);

    $.fn.iniPreview = function() {
        return $(this).each(function() {
            var _this = $(this);
            var originalSrc = _this.attr('data-preview');
            var previewType = _this.attr('data-preview-type');
            var previewButton = $('<div>').addClass('preview-button');
            _this.after(previewButton);

            switch(previewType) {
                case 'image':
                    previewButton.append(
                        $('<img>').
                            attr('src', 'images/preview/icon_zoom.svg')
                    );
                    break;
                case 'youtube':
                    previewButton.append(
                        $('<img>').
                            attr('src', 'images/preview/icon_play.svg')
                    );
                    break;
            }

            var showPreviewContent = function() {
                switch(previewType) {
                    case 'image':
                        var _img = new Image();
                        var img = $(_img).
                            addClass('modal-img-preview');
                        _img.onload = function() {
                            openPopup(img);
                        };
                        img.attr('src', originalSrc);
                        break;
                    case 'youtube':
                        var iframe = $('<iframe>').
                            addClass('modal-img-preview');
                        iframe.attr({
                            src: originalSrc + '?rel=0&amp;autoplay=1',
                            frameborder: 0,
                            allowfullscreen: '',
                            width: 740,
                            height: 480
                        });
                        openPopup(iframe);
                        break;
                }
            };
            previewButton.on('click', showPreviewContent);
        });
    };

    $.fn.iniFilter = function(options) {
        return $(this).each(function() {
            var _this = $(this);
            var itemsFilter = $('[' + options['filterItemAttr'] + ']', _this);
            var itemsForFilter = $('[' + options['listItemAttr'] + ']');
            var allItem = options['allItemsValue'] ? itemsFilter.filter('[' + options['filterItemAttr'] + '="' + options['allItemsValue'] + '"]') : false;
            var filteredItems = false;
            var activeFilterItem = false;

            var filterItems = function() {
                var _th = $(this);
                var value = _th.attr(options['filterItemAttr']);
                if (activeFilterItem && value == activeFilterItem.attr(options['filterItemAttr'])) return;
                activeFilterItem ? activeFilterItem.removeClass(options['classForMenuItem']) : false;
                activeFilterItem = _th;
                var items;

                filteredItems ? filteredItems.removeClass(options['classForFiltered']) : false;
                itemsForFilter.not(filteredItems).show();

                switch(value) {
                    case options['allItemsValue']:
                        items = itemsForFilter;
                        break;
                    default:
                        items = itemsForFilter.filter('[' + options['listItemAttr'] + '="' + value + '"]');
                        filteredItems = items.addClass(options['classForFiltered']);
                        itemsForFilter.not(filteredItems).hide();
                        break;
                }
                _th.addClass(options['classForMenuItem']);
                $(window).scroll();
            };
            itemsFilter.on('click', filterItems);
            allItem.each(filterItems);
        });
    };

    $(function() {
        body = $('body:first');
        $('.slider-container').slider();
        $('[data-preview]').iniPreview();

        $('#filter-menu').iniFilter({
            filterItemAttr: 'data-category-filter',
            listItemAttr: 'data-category',
            allItemsValue: 'all',
            classForFiltered: 'filtered-item',
            classForMenuItem: 'active'
        });
        $('img[data-original]').lazyload({
            effect: 'fadeIn',
            threshold: 300
        });
    })
})(jQuery);