(function($) {
    $(function() {
        var menu = $('.main-menu:first');
        var menuLinks = $('a', menu);
        var anchors = $('a.anchor');

        var isClick = false;
        menuLinks.each(function() {
            var _th = $(this);
            var selectorAnchor = _th.attr('href');
            var thAnchor = $(selectorAnchor);
            if (!thAnchor.length) return;
            _th.data('anchor', thAnchor.length ? thAnchor : false);
            _th.on('click', function(e) {
                e.preventDefault();
                if (oldActiveItem && _th.is(oldActiveItem)) return;
                oldActiveItem ? oldActiveItem.removeClass('active') : false;
                oldActiveItem = _th;
                isClick = true;
                _th.addClass('active');
                $({top: $(window).scrollTop()}).animate({top: thAnchor.offset()["top"]}, {
                    duration: 1000,
                    step: function() {
                        $(window).scrollTop(this.top);
                    },
                    complete: function() {
                        isClick = false;
                    }
                });
            });
        });

        var header = $('.main-menu:first');
        var win = $(window);

        var oldActiveItem = false;
        var serchAnchors = function() {
            var winHeight = win.height() / 2;
            var activeItem = false;
            var winScroll = win.scrollTop();
            menuLinks.each(function() {
                var _th = $(this);
                var anch = _th.data('anchor');
                if (!anch) return;
                var offsetTop = anch.offset()['top'];
                if (offsetTop < winScroll && (!activeItem || activeItem['offset'] < offsetTop) || (offsetTop - winScroll < winHeight)) {
                    activeItem = {
                        offset: offsetTop,
                        item: _th
                    };
                }
            });
            oldActiveItem ? oldActiveItem.removeClass('active') : false;
            oldActiveItem = activeItem.item.addClass('active');
        };
        serchAnchors();

        var scrollTimeout = false;
        win.on('scroll', function() {
            if (header.offset()['top'] + header.outerHeight() < win.scrollTop()) {
                menu.addClass('fixed');
            } else {
                menu.removeClass('fixed')
            }
            scrollTimeout ? clearTimeout(scrollTimeout) : false;
            if (isClick) return;
            scrollTimeout = setTimeout(serchAnchors, 100);
        });

        var body = $('body:first');
        var activePopUp = false;
        var closePopup = function(e) {
            var target = e ? $(e.target) : false;
            if (!activePopUp) {
                return;
            } else {
                if (target !== false && !target.is('[data-close-popup]')) {
                    return;
                }
            }
            if (target) {
                mask.fadeOut(function() {
                    activePopUp.detach();
                    activePopUp = false;
                    body.css({
                        overflow: '',
                        marginTop: '',
                        padding: ''
                    });
                    menu.css({
                        right: ''
                    });
                });
            } else {
                mask.hide();
                activePopUp.detach();
                activePopUp = false;
                body.css({
                    overflow: '',
                    marginTop: '',
                    padding: ''
                });
            }
        };

        var openPopup = function(name) {
            closePopup();
            var bodyWidth = body.width();

            body.css({
                overflow: 'hidden',
                marginTop: -win.scrollTop() + win.height
            });
            activePopUp = popups[name].appendTo(mask).show();
            mask.fadeIn();
            var marg = Math.max((mask.height() - activePopUp.height()) / 2, 20);
            activePopUp.css({
                marginTop: marg,
                marginBottom: 20
            });
            body.css({
                'padding-right': body.outerWidth() - bodyWidth
            });
            menu.css({
                right: body.outerWidth() - bodyWidth
            });
        };

        var mask = $('<div>').addClass('popup-mask').attr('data-close-popup', '').on('click', closePopup).hide().appendTo(body);
        var popups = {};
        $('.popup-window').each(function() {
            var _this = $(this);
            var popupName = _this.data('name');
            popups[popupName] = _this;
        });
        $('[data-open-popup]').each(function() {
            var _this = $(this);
            var popupName = _this.data('open-popup');
            _this.on('click', function(e) {
                e.preventDefault();
                openPopup(popupName);
                return false;
            });
        });

        $.ui.autocomplete.filter = function (array, term) {
            var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term), "i");
            return $.grep(array, function (value) {
                return matcher.test(value.label || value.value || value);
            });
        };

        $('select').each(function() {
            var _this = $(this);
            var arr = [];
            var inputAuto = $('<input>').attr({
                'placeholder': 'Ваш город',
                'name': _this.attr('name')
            });
            _this.before(inputAuto).detach();
            $('option', _this).each(function() {
                var t = $(this);
                arr.push(t.attr('value'));
            });
            var parentField = _this.parent().addClass('ui-autocomplete-parent');
            inputAuto.autocomplete({
                appendTo: parentField,
                source: arr,
                change: function(event, ui) {
                    if (!ui.item) {
                        inputAuto.val('');
                    }
                }
            });
        });
        mask.append($('.ui-autocomplete'));


        //--- Валидация формы ---
        var regExps = {
            email: /^[a-zA-Z0-9\-\.\_]+\@[a-zA-Z0-9\-\.\_]+\.[a-zA-Z]{2,4}$/,
            phone: /^\+?[\d]{1,3}\s?\(?[\d]{1,4}\)?\s?[\d\-?]{3,10}$/,
            name: /^[\S\s]{3,120}$/,
            city: /^[\s\S]+$/
        };
        var emptyErrors = {
            email: "Введите e-mail",
            phone: "Введите номер",
            name: "Введите Имя",
            city: 'Выберите город'
        };
        var errorRegexpTexts = {
            email: "Неверный формат e-mail",
            phone: "Проверьте номер",
            name: "Проверьте Имя",
            city: 'Выберите город'
        };
        var validateField = function(inp) {
            var errorBaloon = inp.data("errorBaloon");
            inp.val(inp.val().replace(/[\s]+/g, ' '));
            var name = inp.attr("name");
            var regExp = regExps[name];
            if (!regExp) return;
            var emptyErrorText = emptyErrors[name] || false;
            var formatError = errorRegexpTexts[name] || false;
            var val = inp.val();
            var error = regExp.test(val);
            var errorMsg = false;
            if (val != inp.data("oldValue") || !val.length) {
                if (!val.length) {
                    errorMsg = emptyErrorText;
                } else if (!regExp.test(val)) {
                    errorMsg = formatError;
                }
            }
            if (errorMsg) {
                errorBaloon.text(errorMsg);
                if (!error) {
                    errorBaloon.fadeIn();
                    error = true;
                    inp.data('error', true);
                }
            } else {
                if (error) {
                    errorBaloon.fadeOut();
                    error = false;
                    inp.data('error', false);
                }
            }
            inp.data("oldValue", val);
        };
        var validateFormActivate = function(form) {
            var inputs = $("input, select", form).each(function() {
                var inp = $(this);
                var errorBaloon = $("<div>").addClass("error-field").hide();
                inp.data("errorBaloon", errorBaloon);
                inp.data("oldValue", "");
                inp.after(errorBaloon);
                inp.bind("blur", function(){
                    validateField($(this));
                });
            });


            var sendForm = function() {
                var data = {};
                var errors = false;
                inputs.each(function() {
                    var inp = $(this);
                    validateField(inp);
                    if (inp.data('error')) errors = true;
                    data[inp.attr('name')] = inp.val();
                });
                if (!errors) {
                    $.ajax({
                        url: form.attr('action'),
                        data: form.serialize(),
                        complete: function() {
                            openPopup('form-success');
                        },
                        type: form.attr('method')
                    });
                }
            };
            $('[type="submit"]', form).on('click', sendForm);
        };
        //--- Валидация формы ---
        $('form').on("submit", function(e) {
            e.preventDefault();
            return false;
        }).on('keydown', function(e) {
            if (e.keyCode == 13) return false;
        }).each(function() {
            validateFormActivate($(this));
        });

    })
})(jQuery);